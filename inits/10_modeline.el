;; -*- lexical-binding: t -*-

;; (defun my:mode-line-simplify (&optional dummy-arg)
;;   (setq minor-mode-alist
;;         (list '(flycheck-mode flycheck-mode-line))))

;; (add-hook 'after-init-hook
;;           (lambda ()
;;             (my:mode-line-simplify)
;;             (add-hook 'after-load-functions 'my:mode-line-simplify)))

;; ;; flat theme
;; (face-spec-set 'header-line '((t (:box nil))))
;; (face-spec-set 'mode-line '((t (:box nil))))
;; (face-spec-set 'mode-line-inactive '((t (:box nil))))

;; (setq mode-line--centering-buffer-name-p nil)
;; (require 'my-modeline)
;; (face-bold-p 'mode-line-mode-face)
(window-numbering-mode 1)
;; (defun my/which-func-after-func (&rest _)
;;     (when which-func-mode
;;       (setq header-line-format
;;             '(" " (which-func-mode (:propertize which-func-current face header-line))))))
;; ;; Which func header line
;; (with-eval-after-load "which-func"
;;   (advice-add 'which-func-ff-hook :after #'my/which-func-after-func))

(require 'powerline)

(defun my-modeline-awesome--propertize (glyph)
  (propertize glyph 'face '(:family "FontAwesome" :height 1.0)))

(defpowerline powerline-vc
  (when (and (buffer-file-name (current-buffer)) vc-mode)
    (save-match-data
      (let ((vc-mdl (format-mode-line '(vc-mode vc-mode)))
            (git-char (my-modeline-awesome--propertize " ")))
        (cond ((string-match (rx " Git"
                                 (group (or "-" ":" "@" "!" "?"))
                                 (group (1+ nonl)))
                             vc-mdl)
               (let ((state (match-string 1 vc-mdl))
                     (branch (match-string 2 vc-mdl)))
                 (cond ((string= state ":")
                        (concat
                         (format "%s " git-char)
                         (powerline-raw
                          (format "%s(changed)" branch) 'bold)))
                       ((member state '("@" "!" "?"))
                        (format "%s %s%s" git-char branch
                                state))
                       (t (format "%s %s" git-char branch)))))
              (t vc-mdl))))))


(defun my-modeline--add-prop (s er)
  (propertize s 'face
              `(:family "FontAwesome"
                        :height 1.0
                        :foreground ,(face-foreground er))))

(defpowerline my-flycheck-mode-line
  (when (bound-and-true-p flycheck-mode)
    (let* ((error-type nil)
           (text (pcase flycheck-last-status-change
                   (`not-checked "")
                   (`no-checker "-")
                   (`running "*")
                   (`errored "!")
                   (`finished
                    (let-alist (flycheck-count-errors flycheck-current-errors)
                      (cond (.error
                             (setq error-type 'error)
                             (format "%s/%s" .error (or .warning 0)))
                            (.warning
                             (setq error-type 'warning)
                             (format "%s" .warning))
                            (.info (format "%s" .info))
                            (t "0"))))
                   (`interrupted "-")
                   (`suspicious "?"))))
      (let ((s (my-modeline-awesome--propertize "  ")))
        (powerline-raw
         (concat
          (if (and (eq flycheck-last-status-change 'finished)
                   (memq error-type '(error warning)))
              (my-modeline--add-prop s error-type)
            s)
          text))))))

(defun mode-line--coding-system-string ()
  (let* ((cs buffer-file-coding-system)
         (cs-name-raw (plist-get (coding-system-plist cs) :name))
         (name (->> (cl-case cs-name-raw
                      (undecided nil)
                      (t (symbol-name cs-name-raw)))
                    mode-line--cs-name-convert))
         (eol-type1 (coding-system-eol-type cs))
         (eol-type (and (integerp eol-type1)
                        (cl-case eol-type1
                          (0 nil)
                          (1 "dos")
                          (2 "mac")))))
    (cond ((and eol-type name (not (string= eol-type "")))
           (concat name (format " %s" eol-type)))
          (eol-type (format "%s" eol-type))
          (name name)
          (t ""))))

(defvar mode-line--cs-name-convert-alst
  '(("japanese-iso-8bit" . "euc-jp")
    ("prefer-utf-8" . "")
    ("japanese-shift-jis" . "sjis")))

(defun mode-line--cs-name-convert (cs-name)
  (when (and cs-name
             (string-match (rx (group (1+ (or alnum "-"))) "-with-signature")
                           cs-name))
    (setq cs-name (format "%s(BOM)" (match-string 1 cs-name))))
  (or (assoc-default cs-name mode-line--cs-name-convert-alst) cs-name))

(defpowerline my-mode-line-mule-info
  (mode-line--coding-system-string))

(defpowerline my-mode-line-evil-state
  (cl-case evil-state
    ('insert "I")
    ('normal "N")
    ('visual "V")
    ('motion "M")
    ('emacs "E")
    (t (symbol-name evil-state))))

(load "/home/sho/app/sky-color-clock/sky-color-clock.el")
(sky-color-clock-initialize 34)
(setq sky-color-clock-format "%H:%M")

(defun my-powerline-theme ()
  "Setup the my mode-line.
cf. http://qiita.com/itome0403/items/c0fba8186ee8910bf8ab"
  (interactive)
  (setq-default mode-line-format
                '("%e"
                  (:eval (sky-color-clock))
                  (:eval
                   (let* ((active (powerline-selected-window-active))
                          (mode-line (if active 'mode-line 'mode-line-inactive))
                          ;; (face1 (if active 'powerline-active1 'powerline-inactive1))
                          (lhs (list
                                (if (buffer-file-name)
                                    (powerline-raw " %* ")
                                  " ")
                                (when evil-mode
                                  (my-mode-line-evil-state))
                                (powerline-raw " ")
                                (powerline-buffer-id nil)
                                (powerline-raw "  ")
                                (powerline-vc)
                                (powerline-raw " ")
                                (when (bound-and-true-p flycheck-mode)
                                  (powerline-raw " ")
                                  (my-flycheck-mode-line))
                                ;; (powerline-arrow-fade-left face1 mode-line)
                                ;; (powerline-raw " ")
                                ;; (powerline-raw "%o" nil 'r)
                                (when (and (boundp 'which-func-mode) which-func-mode)
                                  (powerline-raw which-func-format nil 'l))))
                          (rhs (list
                                (powerline-raw "L: %4l C: %3c" nil 'l)
                                (powerline-raw "  ")
                                (powerline-major-mode)
                                (powerline-raw " ")
                                (powerline-process nil 'r)
                                (my-mode-line-mule-info)
                                (powerline-raw " "))))
                     (concat (powerline-render lhs)
                             (powerline-fill nil (powerline-width rhs))
                             (powerline-render rhs)))))))

;; (face-spec-set 'powerline-active1
;;                `((t (:background "#EF8300"
;;                                  :foreground ,(face-background 'mode-line)
;;                                  :weight bold))))

(my-powerline-theme)

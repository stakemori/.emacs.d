(modify-frame-parameters nil '((wait-for-wm . nil)))

;; bytecompileで新しいほう．
(setq load-prefer-newer t)

(setq custom-file (locate-user-emacs-file "custom.el"))
(load custom-file)

(load (locate-user-emacs-file "inits/load-path.el"))

;; package
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))

(let ((file-name-handler-alist nil)
      (inhibit-message t)
      (gc-cons-threshold (* 30 gc-cons-threshold)))
  (package-initialize)
  (ignore-errors (package-install-selected-packages))
  ;; init-loader
  (setq init-loader-show-log-after-init 'error-only)
  (init-loader-load (locate-user-emacs-file "inits")))
(require 'f)
;; byte-recompile
;; (defun my-byte-recompile-rec (dir &optional force)
;;   (let* ((d-fs (directory-files dir t "[a-zA-Z0-9]+.*$"))
;;          (ds (cl-loop for x in d-fs
;;                    if (file-directory-p x)
;;                    collect x))
;;          (fs (cl-loop for x in d-fs
;;                    if (and (not (file-directory-p x))
;;                            (string-match (rx ".el" eol) x))
;;                    collect x)))
;;     (cl-loop for f in fs
;;           do (ignore-errors (if force (byte-compile-file f)
;;                               (byte-recompile-file f))))
;;     (cl-loop for d in ds
;;           do (my-byte-recompile-rec d force))))
;; (my-byte-recompile-rec (locate-user-emacs-file "packages") t)
;; (let ((force t))
;;   (cl-loop for d in `((locate-user-emacs-file "elisp")
;;                       (locate-user-emacs-file "my-elisp")
;;                       (locate-user-emacs-file "skk")
;;                       "~/work/elisp/"
;;                       ,package-user-dir)
;;         do (my-byte-recompile-rec d force)))

;; Local Variables:
;; no-byte-compile: t
;; End:

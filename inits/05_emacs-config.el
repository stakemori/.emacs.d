;; -*- lexical-binding: t -*-
(eval-when-compile (require 'dash))
(setq-default truncate-lines t)

;; text-mode
(my-named-add-hook text-mode-hook
  (flyspell-mode 1)
  (toggle-truncate-lines)
  (toggle-word-wrap))

;; ;; query-replaceの時に大文字小文字を文脈で判断しない
;; (setq case-replace t)
;; replace-historyをfromとtoで別のものをつかう
(setq query-replace-from-history-variable 'my-query-replace-from-history)
(defvar my-query-replace-from-history nil)
(setq query-replace-to-history-variable 'my-query-replace-to-history)
(defvar my-query-replace-to-history nil)

;; isearch
(when (boundp 'migemo-isearch-enable-p)
  (setq migemo-isearch-enable-p nil))
(define-key isearch-mode-map (kbd "C-n") 'isearch-delete-char)

;; 日本語環境
(set-language-environment "Japanese")

;; ドイツ語入力
(setq default-input-method "german-postfix")

;; server-start
(my/lazy-progn
 (require 'server)
 (unless (server-running-p)
   (server-start)))

;; ;; y,n in place of yes no
;; (defalias 'yes-or-no-p-orig (symbol-function 'yes-or-no-p))
;; (defalias 'yes-or-no-p (symbol-function 'y-or-n-p))
(setq dired-deletion-confirmer #'y-or-n-p)

;; log-view-mode
(my-eval-after-load "log-view"
    (define-key log-view-mode-map (kbd "j") 'log-view-msg-next)
    (define-key log-view-mode-map (kbd "k") 'log-view-msg-prev))

;; desktop clear
(autoload 'desktop-clear "desktop")
(my-eval-after-load "desktop"
  (setq desktop-clear-preserve-buffers
        (append desktop-clear-preserve-buffers
                '("\\*Pymacs\\*" "\\*Python\\*" "\\*GNU Emacs\\*")))
  ;; bufferだけを消すように
  (setq desktop-globals-to-clear nil))

;;;indentするとき tab 使わない
(setq-default indent-tabs-mode nil)

;; マウスのドラッグでコピーしない．
(setq mouse-drag-copy-region nil)


;; file名補完入力のとき大文字小文字を区別しない．
(setq read-file-name-completion-ignore-case t)

;; 変数名補完入力のとき大文字小文字を区別しない．
(setq completion-ignore-case t)

(setq make-backup-files nil)
;;; バックアップファイルの保存場所を指定。
(unless (file-exists-p (expand-file-name (locate-user-emacs-file "backups/")))
  (make-directory (expand-file-name (locate-user-emacs-file "backups/"))))
(setq backup-directory-alist
      (cons (cons ".*$" (expand-file-name (locate-user-emacs-file "backups/")))
            backup-directory-alist))
;; (setq version-control t)     ; 複数のバックアップを残します。世代。
;; (setq kept-new-versions 5)   ; 新しいものをいくつ残すか
;; (setq kept-old-versions 5)   ; 古いものをいくつ残すか
;; (setq delete-old-versions t) ; 確認せずに古いものを消す。
;; (setq vc-make-backup-files t) ; バージョン管理下のファイルもバックアップを作る。
;; auto-save
(setq auto-save-file-name-transforms
      `((".*" ,(expand-file-name (locate-user-emacs-file "backups/")) t)))
;; (setq auto-save-interval 50)

;; bookmark
(setq bookmark-save-flag 1)

;; page-ext
(autoload 'pages-directory "page-ext" nil t)
(global-set-key (kbd "C-x C-p C-d") 'pages-directory)
(my-eval-after-load "page-ext"
  (require 'view)
  (setq pages-directory-mode-map (copy-keymap view-mode-map))
  (defadvice pages-directory
    (around widen-pop-to-buffer activate)
    (save-window-excursion
      (widen)
      ad-do-it)
    (pop-to-buffer (concat pages-directory-prefix " " (buffer-name))))
  (defvar my-pages-directory-mode-hook nil)
  (defadvice pages-directory-mode (after my-hook activate)
    (run-hooks 'my-pages-directory-mode-hook))
  (my-define-keys pages-directory-map
    "RET"      'pages-directory-goto
    "q"        'delete-window
    "j"        'next-line
    "k"        'previous-line
    "o"        'pages-directory-goto
    "C-c C-a"  'add-new-page))

;; diff-mode
(my-add-hook diff-mode-hook
  (view-mode 1)
  (setq buffer-read-only nil)
  (define-key view-mode-map (kbd "n") 'diff-hunk-next)
  (define-key view-mode-map (kbd "p") 'diff-hunk-prev)
  (define-key view-mode-map (kbd "q") 'quit-window)
  (define-key view-mode-map (kbd "SPC") 'View-scroll-line-forward)
  (define-key view-mode-map (kbd "b") 'View-scroll-line-backward))

;;括弧自動補完
;;; スケルトンでのペア補完をオン
(setq skeleton-pair 1)
;;; スケルトンでのペア補完定義
(global-set-key "(" 'skeleton-pair-insert-maybe)
(global-set-key "[" 'skeleton-pair-insert-maybe)
(global-set-key "{" 'skeleton-pair-insert-maybe)
(global-set-key "\"" 'skeleton-pair-insert-maybe)
;(global-set-key "'" 'skeleton-pair-insert-maybe)

;; enable to pop `mark-ring' repeatedly like C-u C-SPC C-SPC ...
(setq set-mark-command-repeat-pop t)

;;kill-line で改行を含めてカット
(setq kill-whole-line t)

;;menu非表示
;; (my/lazy-progn :time 0.05
;;  (menu-bar-mode 0)
;;  (tool-bar-mode 0))

;; auto-fill
(setq-default fill-column 80)

;;; 終了時にオートセーブファイルを消す
(setq delete-auto-save-files t)

;; locate
(my-eval-after-load "locate"
  (my-define-keys locate-mode-map
    "M-s" nil
    "M-o" nil))

;; ;; frame maximize
;; (set-frame-parameter nil 'fullscreen 'maximized)

;; ediff
;; one frameにする
(setq ediff-window-setup-function 'ediff-setup-windows-plain)

;; prettify symbol
(defmacro my:prettify-symbols-add (hook &rest alists)
  (declare (indent 1))
  `(my-add-hook0 ,hook "prettify-symbols"
     (setq prettify-symbols-alist
           (append (cl-loop for alst in (list ,@alists) append alst)
                   prettify-symbols-alist))
     (prettify-symbols-mode 1)))

(let ((lt-gt '(("<=" . ?≤) (">=" . ?≥)))
      (lam '(("lambda" . ?λ)))
      (not-eq '(("!=" . ?≠)))
      (lisp-not-eq '(("/=" . ?≠))))
  (my:prettify-symbols-add emacs-lisp-mode-hook
    lt-gt lisp-not-eq)
  (my:prettify-symbols-add lisp-interaction-mode-hook
    lt-gt lisp-not-eq)
  (my:prettify-symbols-add clojure-mode-hook
    lt-gt)
  (my:prettify-symbols-add python-mode-hook
    lt-gt not-eq)
  (my:prettify-symbols-add sage-shell-mode-hook
    lt-gt not-eq)
  (my:prettify-symbols-add rust-mode-hook
    not-eq))

(my/lazy-progn
 (show-paren-mode 1)
 ;; eshellでhilightされないように
 (make-variable-buffer-local 'show-paren-mode)

 ;; for whitespace-mode
 (require 'whitespace)
 ;; see whitespace.el for more details
 (setq whitespace-style '(face
                          tabs tab-mark
                          space-mark))
 (setq whitespace-display-mappings
       (cons (list 'space-mark ?\x3000 [?⛶])
             (--filter (not (eq (car it) 'space-mark))
                       whitespace-display-mappings)))
 (setq whitespace-space-regexp "\\(\u3000+\\)")
 ;; (when (boundp 'show-trailing-whitespace)
 ;;   (setq-default show-trailing-whitespace t))
 (defun my:show-trailing-whitespace ()
   (setq show-trailing-whitespace t))
 (add-hook 'prog-mode-hook #'my:show-trailing-whitespace)
 (add-hook 'LaTeX-mode-hook #'my:show-trailing-whitespace)
 (set-face-foreground 'whitespace-space "#7cfc00")
 (set-face-background 'whitespace-space 'nil)
 (set-face-bold-p 'whitespace-space t)
 (add-hook 'prog-mode-hook 'whitespace-mode)
 (add-hook 'LaTeX-mode-hook 'whitespace-mode)
 (add-hook 'org-mode-hook 'whitespace-mode)
 (add-hook 'text-mode-hook 'whitespace-mode)
 ;; delete-backward-char で複数の whitespace を消す。
 (setq backward-delete-char-untabify-method 'hungry)
 ;; 全角commaのface
 (defface my-zenkaku-comma-face
   '((t (:foreground "tomato")))
   "zenkaku comma face"
   :group 'my-zenkaku-face)
 (defvar my-zenkaku-comma-face 'my-zenkaku-comma-face))



;; minibufferを再帰的に呼びだせるようにする
(setq enable-recursive-minibuffers t)

;; minibuffer map
(define-key minibuffer-local-map (kbd "C-j") 'next-line)

(put 'narrow-to-page 'disabled nil)
(put 'upcase-region 'disabled nil)

(setq user-mail-address "stakemorii@gmail.com")

(global-set-key (kbd "C-M-y") 'clipboard-paste)

;; info path
(my-add-hook Info-mode-hook            ; After Info-mode has started
  (setq Info-additional-directory-list Info-default-directory-list))
(setq Info-default-directory-list
        (cons (expand-file-name (locate-user-emacs-file "info")) Info-default-directory-list))

;; comint keymap
(with-eval-after-load "comint"
  (define-key comint-mode-map (kbd "C-c C-d") nil))


;; hippie-expand
(setq hippie-expand-try-functions-list
      '(try-complete-file-name-partially
        try-complete-file-name))
(global-set-key "\M-/" 'hippie-expand)

(global-set-key (kbd "C-t") 'other-window-or-split)

;; ring bell
(setq ring-bell-function 'ignore)

;; revert-buffer
(add-hook 'after-init-hook #'global-auto-revert-mode)

;; frame title にdefault directory
(setq frame-title-format
      `((:eval
         (let ((emacs (format "- GNU Emacs %s"
                              (--> emacs-version
                                   (split-string it (rx "."))
                                   (butlast it)
                                   (mapconcat 'identity it ".")))))
           (cond ((buffer-file-name)
                  (format "%s - %s %s"
                          (file-name-nondirectory (buffer-file-name))
                          default-directory
                          emacs))
                 (t (format "%s %s"
                            default-directory
                            emacs)))))))

;; kill ring max
(setq kill-ring-max 3000)

;; compilation後にwindowを選択
(setq compilation-finish-functions
      (list
       (lambda (buf status)
         (select-window (get-buffer-window buf)))))
;; (set-face-underline 'warning nil)
;; (set-face-underline 'underline nil)
(setq help-window-select t)

;; eofを分かりやすくする
(my-add-hook find-file-hook
  (setq indicate-buffer-boundaries 'left))

(my/lazy-progn
 (ido-mode 'buffers)
 (blink-cursor-mode -1))


(setq use-dialog-box nil)

;; overwrite mode off
(my-define-keys global-map
  "<insert>" 'ignore
  "<insertchar>" 'ignore)

;; minibufferでsaveするのを防ぐ
(define-key minibuffer-local-map (kbd "C-x C-s") 'ignore)


;; 最大化
(add-hook 'after-init-hook #'toggle-frame-maximized)


(my-define-keys global-map "C-q t" 'ert-run-tests-interactively)

;; eldoc
(with-eval-after-load "eldoc"
  (eldoc-add-command #'insert-paren #'ac-complete
                     #'my-ace-window-cmd
                     #'skeleton-pair-insert-maybe
                     #'delete-backward-char))

;; prefix key を echo area に表示させない
(setq  echo-keystrokes 0)

;;; occur key map
(my-define-keys occur-mode-map
  "n" (lambda () (interactive)
        (call-interactively 'occur-next)
        (call-interactively 'occur-mode-display-occurrence))
  "p" (lambda () (interactive)
        (call-interactively 'occur-prev)
        (call-interactively 'occur-mode-display-occurrence))
  "m" 'occur-mode-goto-occurrence)

(setq case-fold-search nil)

;; saveplace
(my/lazy-progn
 (save-place-mode 1)
 (setq save-place-file (locate-user-emacs-file "cache/.cache_save_place")))

(with-eval-after-load "eww"
  (my-define-keys eww-mode-map
    "j" 'next-line
    "k" 'previous-line))

(setq print-gensym t)

(setq browse-url-browser-function #'browse-url-firefox)
(setq browse-url-chrome-program "firefox")

;; truncate時の左の矢印を消す
(setq-default fringe-indicator-alist
              (cons '(continuation nil right-curly-arrow)
                    fringe-indicator-alist))

;;; 直前のウィンドウ構成に戻す
(my/lazy-progn
 (winner-mode 1))
(global-set-key (kbd "C-M-,") 'winner-undo)
(global-set-key (kbd "C-M-.") 'winner-redo)

(setq comint-input-ring-size 2000)
(my-named-add-hook after-init-hook
  window-config
  (remove-hook 'window-configuration-change-hook
               'window--adjust-process-windows))

(add-hook 'prog-mode-hook #'hl-line-mode)
(setq hl-line-sticky-flag nil)
(setq user-full-name "Sho Takemori <stakemorii@gmail.com>")
(setq doc-view-resolution 150)

(defvar my-m-r-map (make-sparse-keymap))
(global-set-key (kbd "M-r") my-m-r-map)
(bind-keys :prefix "M-r"
           :prefix-map my-m-r-map
           ("y" . (lambda (reg)
                    (interactive
                     (list (register-read-with-preview "Copy to register: ")))
                    (set-register reg (car kill-ring))))
           ("s" . copy-to-register)
           ("M-w" . copy-to-register)
           ("r" . copy-rectangle-to-register)
           ("SPC" . point-to-register)
           ("C-SPC" . point-to-register)
           ("w" . window-configuration-to-register)
           ("j" . jump-to-register)
           ("f" . frameset-to-register)
           ("i" . insert-register)
           ("p" . insert-register)
           ("+" . increment-register)
           ("h" . helm-register))
(define-key minibuffer-inactive-mode-map [mouse-1] nil)

;; shell-command のbuffer消さない． pointの位置が最後のoutputの始まり．
(setq shell-command-dont-erase-buffer 'beg-last-out)
(setq save-some-buffers-default-predicate t)
(setq save-abbrevs 'silently)
;; (my-named-add-hook prog-mode-hook
;;   lines
;;   (setq-local display-line-numbers 'relative))
(setq auto-hscroll-mode 'current-line)

(set-frame-parameter nil 'alpha '95)
(add-hook 'prog-mode-hook 'display-line-numbers-mode)

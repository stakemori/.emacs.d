(my-eval-after-load "org"
  ;; ExportしたときValiditate を表示しない
  (setq org-html-validation-link nil)
  (evil-leader/set-key-for-mode 'org-mode
    "t"  'org-show-todo-tree
    "a"  'org-agenda
    "l"  'evil-org-open-links
    "o"  'evil-org-recompute-clocks)

  (evil-define-key 'normal org-mode-map
    "gn" 'org-next-visible-heading)
  (evil-define-key 'insert evil-org-mode-map
    (kbd "M-e") 'org-edit-special)

  (define-key org-src-mode-map (kbd "M-e") 'org-edit-src-exit)

  (setq org-startup-with-inline-images t)
  (add-hook 'org-babel-after-execute-hook 'org-display-inline-images)
  (setq org-babel-default-header-args:sage '((:session . t)
                                             (:exports . "both")
                                             (:results . "output")
                                             (:latexnewline . "\\\\\n")
                                             (:latexwrap  "\\("  . "\\)")))
  (setq org-export-babel-evaluate nil)
  (my:set-mykey! org-mode-map)
  (defadvice org-export (around no-kill-ring activate)
    (let ((kill-ring nil))
      ad-do-it))
  (setq org-directory "~/Dropbox/memo/")
  (setq org-default-notes-file (expand-file-name "todo.org" org-directory))
  (setq org-log-done 'time)
  (setq org-capture-templates
        '(("t" "Todo" entry (file+headline "~/Dropbox/memo/todo.org" "Tasks")
           "* TODO %? \n  Added: %T")
          ("n" "Note" entry (file+headline "~/Dropbox/memo/notes.org" "Notes")
           "* %? \n  Added: %T")))
  (setq org-use-fast-todo-selection t)
  (setq org-todo-keywords
        '((sequence "TODO(t)" "STARTED(s)" "WAITING(w)" "|"
                    "DONE(x)" "CANCEL(c)")))
  (my-add-hook org-agenda-mode-hook
    (define-key org-agenda-mode-map  (kbd "C-j") 'next-line)
    (define-key org-agenda-mode-map  (kbd "C-k") 'previous-line)
    (define-key org-agenda-mode-map  (kbd "C-p") 'org-agenda-kill))

  ;; アジェンダ表示の対象ファイル
  (setq org-agenda-files (list org-directory))
  (defun org-insert-upheading (arg)
    (interactive "P")
    (org-insert-heading arg)
    (cond ((org-on-heading-p) (org-do-promote))
          ((org-at-item-p) (org-indent-item -1))))
  (defun org-insert-heading-dwim (arg)
    "現在と同じレベルの見出しを入力する．
C-uをつけると1レベル上，C-u C-uをつけると1レベル下 "
    (interactive "p")
    (cl-case arg
      (4 (org-insert-subheading nil))
      (16 (org-insert-upheading nil))
      (t (org-insert-heading nil))))
  (setq org-confirm-babel-evaluate nil)
  (setq org-html-table-default-attributes
        '(:border "2" :cellspacing "0" :cellpadding "6" :rules "all" :frame "border"))
  (my-add-hook org-mode-hook
    (auto-fill-mode -1)
    (org-babel-do-load-languages
     'org-babel-load-languages
     '((python . t)
       (shell . t)))
    (smartrep-define-key org-mode-map "C-c"
      '(("C-p" . 'outline-previous-visible-heading)
        ("C-n" . 'outline-next-visible-heading)))
    (my-define-keys org-mode-map
      "C-v" 'org-return-indent
      "C-j" 'next-line
      "C-p" 'org-kill-line
      "C-k" 'previous-line
      "C-c C-m" 'org-insert-heading-dwim
      "C-c c" 'ob-sagemath-execute-async
      "M-h" nil
      "M-e" 'org-edit-special))

  ;; orgtable挿入，narrow-to-page
  (defadvice orgtbl-ctrl-c-ctrl-c (around orgtbl-ctrl-c-ctrl-c-widen-narrow (arg))
    (save-excursion
      (widen)
      ad-do-it
      (narrow-to-page)))
  (ad-activate 'orgtbl-ctrl-c-ctrl-c))
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c r") 'org-capture)

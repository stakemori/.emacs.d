(eval-when-compile(require 'cl))
(require 'my-utilities)
(require 'dired)

;; buffer-infoはplist, window, heightは total-width, total-heightからの比
(defvar my-win-plist-mems
  '(split-type
    children nil
    height
    width
    point
    selected-window-p
    buffer-info))

(defun my-win-make-win-plist (&rest args)
  args)

(defun my-win-parse-current-win-config (&optional window only-wh)
  (let* ((window (or window (frame-root-window)))
        (swin (selected-window))
        (child (cl-loop for i in (list (cons 'horizontal (window-top-child window))
                                    (cons 'vertical (window-left-child window)))
                     if (cdr i)
                     return i))
        (win-make-fun (if only-wh
                        'my-win-make-win-plist-only-wh
                        'my-win-make-win-plist-with-buf-info)))
    (cond (child
           (let ((split-type (car child))
                 (children
                  (cl-loop for win = (cdr child) then (window-next-sibling win)
                        while win
                        collect (my-win-parse-current-win-config win only-wh))))
             (my-win-make-win-plist
              :split-type split-type
              :width (case split-type
                       ('vertical
                        (cl-loop for i in children
                              sum (plist-get i :width)))
                       ('horizontal
                        (plist-get (car children) :width)))
              :height (case split-type
                        ('horizontal
                         (cl-loop for i in children
                               sum (plist-get i :height)))
                        ('vertical
                         (plist-get (car children) :height)))
              :children children)))
          (t (funcall win-make-fun window swin)))))

(defun my-win-height-rat (window)
  (/ (float (window-total-height window))
     (window-total-height (frame-root-window))))

(defun my-win-width-rat (window)
  (/ (float (window-total-width window))
     (window-total-width (frame-root-window))))

(defun my-win-make-win-plist-only-wh (window swin)
  (my-win-make-win-plist
   :height (my-win-height-rat window)
   :width (my-win-width-rat window)))

(defun my-win-make-win-plist-with-buf-info (window swin)
  "SWIN is selected window"
  (append
   (my-win-make-win-plist-only-wh window swin)
   (my-win-make-win-plist
    :buffer-info (my-win-parse-buffer (window-buffer window))
    :point (save-selected-window (select-window window) (point))
    :selected-window-p (equal window swin))))

(defun my-win-parse-buf-type (buf)
  (cond ((buffer-file-name buf) 'file-buffer)
        ((eq (buffer-local-value 'major-mode buf) 'dired-mode) 'dired)
        ((equal (buffer-name buf) "*eshell*<1>") 'shell-pop)
        (t 'generic)))

(defun my-win-parse-buffer (buf)
  (list :default-directory (buffer-local-value 'default-directory buf)
        :buffer-name (buffer-name buf)
        :buffer-file-name (buffer-file-name buf)
        :type (my-win-parse-buf-type buf)))

(defvar my-win-selected-window nil)
(cl-defun my-win-load
    (win-children &optional (win-load-from-buf-info-fun
                             'my-win-load-from-buf-info))
  (let* ((split-type (plist-get win-children :split-type))
         (split-window-func
          (case split-type
            ('vertical 'split-window-right)
            ('horizontal 'split-window-below)))
         (window-size-func
          (case split-type
            ('vertical (lambda (ws)
                         (floor (* (plist-get ws :width)
                                   (window-total-width (frame-root-window))))))
            ('horizontal
             (lambda (ws)
               (floor (* (plist-get ws :height)
                         (window-total-height (frame-root-window)))))))))
    (cond (split-type (cl-loop for i on (plist-get win-children :children)
                            for j = (car i)
                            with next-win
                            do
                            (when (cdr i)
                              (funcall split-window-func
                                       (funcall window-size-func j))
                              (setq next-win (window-next-sibling)))
                            (my-win-load j)
                            (select-window next-win)))
          (t (funcall win-load-from-buf-info-fun
                      (plist-get win-children :buffer-info)
                      (plist-get win-children :point))
             (when (plist-get win-children :selected-window-p)
               (setq my-win-selected-window (selected-window))))))
  (when my-win-selected-window
    (if (aand (window-child my-win-selected-window)
              (window-live-p it))
      (select-window (window-child my-win-selected-window))
      (when (window-live-p my-win-selected-window)
        (select-window my-win-selected-window)))))

(defun my-win-load-from-buf-info (buf-info point)
  (call-interactively 'beginning-of-line)
  (scroll-right (window-width))
  (let ((type (plist-get buf-info :type))
        (bn (plist-get buf-info :buffer-name))
        (bfn (plist-get buf-info :buffer-file-name))
        (dfd (plist-get buf-info :default-directory)))
    (case type
      ('file-buffer (when (file-exists-p bfn)
                      (find-file bfn)))
      ('dired (dired dfd)
              (dired-next-dirline 0))
      ('shell-pop
       (aif (get-buffer "*eshell*<1>")
           (progn (switch-to-buffer it)
                  (with-no-warnings
                    (eshell-cd-default-directory dfd)))
         (let ((default-directory dfd))
           (switch-to-buffer (get-buffer-create "*eshell*<1>"))
           (eshell-mode)))
       (goto-char (point-max)))
      (t (when (and bn (get-buffer bn))
           (switch-to-buffer bn))))
    (when point (goto-char point))))

(defvar my-win-saved-plist nil)

(defun my-win-save-current-windows (&optional char no-warning silent)
  (interactive)
  (let* ((char (or char (read-char "Input a character :")))
         (sym (intern (format ":%c" char)))
         (message-log-max nil))
    (when (or (not (plist-member my-win-saved-plist sym))
              (or no-warning
                  (y-or-n-p (format "Character %c is already uesed. Overwrite?"
                                    char))))
      (setq my-win-saved-plist
            (plist-put my-win-saved-plist sym (my-win-parse-current-win-config)))
      (or silent
          (message "Saved \"%c\" to the current window configuration." char)))))


(defun my-win-set-win-config ()
  (interactive)
  (let* ((char (read-char "Load from :"))
         (sym (intern (format ":%c" char)))
         (message-log-max nil))
    (or (awhen (plist-get my-win-saved-plist sym)
          (delete-other-windows)
          (my-win-load it)
          (message "Loaded the window configuration from \"%c\"." char))
        (message "\"%c\" has no window configuration." char))))


(defun my-win-buf-info-alike-p (buf-info1 buf-info2)
  (let ((type1 (plist-get buf-info1 :type))
        (type2 (plist-get buf-info2 :type)))
    (when (equal type1 type2)
      (case type1
        ('dired t)
        (t (equal (plist-get buf-info1 :buffer-name)
                  (plist-get buf-info2 :buffer-name)))))))

(defun my-win-alike-p (win-str1 win-str2)
  (and (cl-loop for s in '(:split-type :height :width)
             always (equal (plist-get win-str1 s)
                           (plist-get win-str2 s)))
       (my-win-buf-info-alike-p (plist-get win-str1 :buffer-info)
                                (plist-get win-str2 :buffer-info))
       (equal (length (plist-get win-str1 :children))
              (length (plist-get win-str2 :children)))
       (cl-loop for w1 in (plist-get win-str1 :children)
             for w2 in (plist-get win-str2 :children)
             always (my-win-alike-p w1 w2))))

(defun my-win-save-win-config-automatically ()
  (cl-loop for (sym win) in (group my-win-saved-plist)
        if (my-win-alike-p win (my-win-parse-current-win-config))
        do
        (my-win-save-current-windows
         (string-to-char (substring (format "%S" sym) 1))
         t t)))

(defvar my-win-auto-save-timer nil)
(defvar my-win-auto-save-p nil)
(defvar my-win-timer-period 1)
(when my-win-auto-save-p
  (setq my-win-auto-save-timer
        (run-with-idle-timer
         my-win-timer-period t
         'my-win-save-win-config-automatically)))

(defun my-2screen-dired (dir1 dir2)
  (interactive (list (read-directory-name "Directory1: ")
                     (read-directory-name "Directory2: ")))
  (delete-other-windows)
  (my-win-load
   (let ((child-fun
          (lambda (dir &optional sp)
            (append
             (and sp '(:selected-window-p t))
             (list :width 0.5
                   :buffer-info
                   (list :default-directory dir :type 'dired))))))
     (list :split-type 'horizontal
           :children
           (list (list :split-type 'vertical :height 0.75
                       :children (list (funcall child-fun dir1 t)
                                       (funcall child-fun dir2)))
                 (list :buffer-info (list :default-directory dir1
                                          :type 'shell-pop))))))
  (my-win-save-current-windows ?h t t))

(defvar my-win-cache-file "~/.emacs.d/cache/.my_win_session.el")

(defun my-win-save-win-config-to-file (file)
  (write-region (concat (prin1-to-string my-win-saved-plist)) nil
                file nil 'no-messg))

(defun my-win-save-win-config-to-cache-file ()
  (my-win-save-win-config-to-file my-win-cache-file))

(defun my-win-load-win-config-from-cache-file ()
  (when (file-exists-p my-win-cache-file)
    (with-temp-buffer
      (insert-file-contents-literally my-win-cache-file)
      (setq my-win-saved-plist (read (current-buffer))))))

(provide 'my-windows)

;; gtags
(setq view-read-only t)
(setq gtags-read-only t)

;; grep a lot
(with-eval-after-load "grep"
  (require 'grep-a-lot))

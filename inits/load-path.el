;; load-path
(dolist (drct `(,(locate-user-emacs-file "elisp/")
                ,(locate-user-emacs-file "proofgeneral/")))
  (let ((default-directory  drct))
    (normal-top-level-add-subdirs-to-load-path)))

(setq load-path
      (append
       '("~/work/elisp/sage-shell-mode/")
       load-path
       (list (locate-user-emacs-file "my-elisp")
             (locate-user-emacs-file "elisp")
             (locate-user-emacs-file "env_dep/")
             (locate-user-emacs-file "skk/skk")
             (locate-user-emacs-file "elisp/pari/"))))

;; -*- lexical-binding: t -*-
(with-eval-after-load 'term
  (cl-loop for x in '("C-b" ";" "M-j" "M-k"
                      "M-y" "C-w" "M-w" "C-j" "C-k" "C-t" "M-t" "C-x"
                      "M-:" "M-x" "C-y")
           for k = (kbd x)
           do (define-key term-raw-map k nil))
  (define-key term-raw-map (kbd "C-x C-j")
    (lambda () (interactive)
      (progn
        (term-send-raw-string "")
        (term-send-raw-string "\n"))))
  (define-key term-raw-map (kbd "C-y")
    (lambda () (interactive)
      (execute-kbd-macro (car kill-ring)))))

(add-hook 'term-mode-hook
          (lambda () (setq window-adjust-process-window-size-function #'ignore)))



(defun open-in-terminal ()
  (interactive)
  (call-process "gnome-terminal" nil nil nil
                "TerminalEmulator"))

;; (defmacro mterm-send-raw-meta-command (char)
;;   `(lambda () (interactive)
;;      (mterm-send-raw-meta ,char)))

;; (defmacro mterm-send-raw-command (char)
;;   `(lambda () (interactive)
;;      (term-send-raw-string (string ,char))))

;; (defmacro mterm-insert-pair (str)
;;   `(lambda () (interactive)
;;      (term-send-raw-string ,(char-to-string (aref str 0)))
;;      (term-send-raw-string ,(char-to-string (aref str 1)))
;;      (term-send-left)))

;; (autoload 'term+mux-new "term+mux" nil t)
;; (autoload 'term+mux-other-window "term+mux" nil t)
;; (my-eval-after-load "term"
;;   (defun mterm-send-raw-meta (char)
;;     (let ((char (event-basic-type char)))
;;       (term-send-raw-string (if (and (numberp char)
;;                                      (> char 127)
;;                                      (< char 256))
;;                                 (make-string 1 char)
;;                               (format "\e%c" char)))))

;;   (require 'term+)
;;   (require 'term+mux)
;;   (define-key term-raw-map
;;     (kbd "C-/") (mterm-send-raw-command ?))
;;   (my-define-keys term-raw-map
;;     "C-M-x" (mterm-send-raw-meta-command ?\M-x)
;;     "\"" (mterm-insert-pair "\"\"")
;;     "'"  (mterm-insert-pair "''"))
;;   (my-keychord-define term-raw-map
;;     "fj" (mterm-insert-pair "{}")
;;     "fk" (mterm-insert-pair "()")
;;     "fl" (mterm-insert-pair "fl"))
;; )

;; (defun mterm+mx-other-window (arg)
;;   (interactive "p")
;;   (require 'term+mux)
;;   (let ((dfd default-directory))
;;     (cond ((equal arg 4)
;;            (other-window-or-split)
;;            (cd dfd)
;;            (term+mux-new)
;;            (pop-to-buffer
;;                        (format "term:%s" term+mux-default-host)))
;;           (t (cond ((cl-loop for b in (buffer-list)
;;                           thereis
;;                           (string-equal (buffer-name b)
;;                                         (format "term:%s" term+mux-default-host)))
;;                     (pop-to-buffer (format "term:%s" term+mux-default-host)))
;;                    (t
;;                     (other-window-or-split)
;;                     (cd dfd)
;;                     (term+mux-new)))))))

;; (defvar term+mux-char-prefix "C-q")
;; (global-set-key (kbd "C-c t") 'mterm+mx-other-window)
;; (face-spec-set 'term-color-red
;;  '((((class color) (background dark))
;;     (:foreground "magenta"))))

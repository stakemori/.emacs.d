(defvar my-shell-ignore-file-list)
(setq my-shell-ignore-file-list
      `("\\.profile$" ,(rx "config" eol)))
(my-add-hook sh-mode-hook
  (when (aand (buffer-file-name)
              (file-exists-p it))
    (add-hook 'after-save-hook
              'my-chmod-u+x
              nil t)
    (unless (cl-loop for r in my-shell-ignore-file-list
                  thereis (string-match r (buffer-file-name)))
      (save-excursion
        (save-restriction
          (widen)
          (goto-char (point-min))
          (unless (looking-at "#!.+sh")
            (insert "#!/bin/bash\n")))))))

(defun my-chmod-u+x ()
  (let ((file (buffer-file-name)))
    (unless (or (string-match-p (rx "config" eol) file)
                (file-executable-p file))
      (shell-command
       (format "chmod u+x %s"
               (file-name-nondirectory file))))))
(my-named-add-hook shell-mode-hook
  async
  (when (string-match (rx "*Async") (buffer-name))
    (view-mode 1)))

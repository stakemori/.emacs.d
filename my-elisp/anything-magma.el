(require 'anything)
(require 'magma-mode)

(defvar amc-buffer-name "*Anything Magma*")

(defun amc-init ()
  (unless (get-buffer amc-buffer-name)
    (with-current-buffer (get-buffer-create amc-buffer-name)
      (insert-file-contents-literally "~/.emacs.d/ac-dict/magma-mode")
      (anything-candidate-buffer (current-buffer)))))

(defvar anything-c-source-magma-commands
      '((name . "Magma Commands")
        (init . amc-init)
        (candidates-in-buffer)
        (action . (("Insert" . (lambda (can) (insert can)))
                   ("Help in Browser" . (lambda (can)
                                          (magma-help-word-browser can)))))))

(defun anything-magma ()
  (interactive)
  (anything :sources '(anything-c-source-magma-commands)))

(provide 'anything-magma)

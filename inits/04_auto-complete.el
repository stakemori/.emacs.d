
;;auto-complete
(use-package auto-complete
  :defer 1
  :config
  (progn
    (face-spec-set 'popup-tip-face
                   '((((class color) (background dark))
                      (:background "khaki1" :foreground "black"))))
    (require 'auto-complete)
    (require 'auto-complete-config)
    (require 'pos-tip)
    (require 'help-mode)
    (setq ac-quick-help-prefer-pos-tip t)
    (add-to-list 'ac-dictionary-directories (locate-user-emacs-file "ac-dict"))
    (ac-config-default)
    (global-auto-complete-mode -1)
    (setq ac-dwim              t
          ac-ignore-case       nil
          ac-auto-start        2
          ac-menu-height       5
          ac-use-quick-help    t
          ac-auto-show-menu    0.8
          ac-limit             1000)
    (setq ac-disable-faces
          '(font-lock-comment-face font-lock-doc-face))
    ;; flyspellとの親和性
    (ac-flyspell-workaround)
    (my-define-keys ac-completing-map
      "C-j" 'ac-next
      "C-k" 'ac-previous
      "TAB" 'ac-complete
      "C-s" 'ac-isearch
      "RET" nil)
    ;; ac-source-words-in-same-mode-buffersがnarrowingしていても単語を集めるように
    (defadvice ac-candidate-words-in-buffer (around widening activate)
      (save-restriction (widen) ad-do-it))
    )
)

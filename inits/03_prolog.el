(push `(,(rx (1+ nonl) ".swi" eol) . prolog-mode) auto-mode-alist)
(with-eval-after-load 'prolog
  (my-define-keys prolog-mode-map "<f10>" 'ediprolog-dwim)
  (key-chord-define prolog-mode-map
                    "rj" (lambda () (interactive) (insert "%?- "))))

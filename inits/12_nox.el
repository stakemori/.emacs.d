(unless (window-system)
  (my-define-keys global-map
    ";" (lambda  ()
          (interactive)
          (if (and (eolp)
                   (not (= (line-beginning-position (point)))))
              (insert ";")
            (call-interactively 'recenter-top-bottom))))

  (setq sage-shell:sage-root "~/app/sage")
  (defvar m1-map (make-sparse-keymap))
  (global-set-key (kbd "M-1") m1-map)
  (my-define-keys global-map
   "M-1 f" 'describe-function
   "M-1 v" 'describe-variable
   "M-1 k" 'describe-key
   "M-1 a" 'helm-apropos
   "M-9" 'vr/replace
   "M-7" 'ag))

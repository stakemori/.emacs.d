;; ruby-mode
(my-add-hook ruby-mode-hook
  (require 'ruby-electric)
  (require 'ruby-block)
  (ruby-electric-mode 1)
  (ruby-block-mode t)
  (ansetq ac-sources (append '(ac-source-yasnippet) it))
  (setq ruby-indent-level 2))

(my-eval-after-load "ruby-electric"
  ;; 関数のかきかえ
  (defvar ruby-electric-mode-map (make-keymap))
  (define-minor-mode ruby-electric-mode
    "Toggle Ruby Electric minor mode.
With no argument, this command toggles the mode.  Non-null prefix
argument turns on the mode.  Null prefix argument turns off the
mode.

When Ruby Electric mode is enabled, an indented 'end' is
heuristicaly inserted whenever typing a word like 'module',
'class', 'def', 'if', 'unless', 'case', 'until', 'for', 'begin',
'do'. Simple, double and back quotes as well as braces are paired
auto-magically. Expansion does not occur inside comments and
strings. Note that you must have Font Lock enabled."
    ;; initial value.
    nil
    ;;indicator for the mode line.
    " REl"
    ;;keymap
    ruby-electric-mode-map
    (ruby-electric-setup-keymap)))

(defvar my-ruby-electric-mode-hook nil)
(defadvice ruby-electric-mode (after make-my-hook activate)
  (run-hooks 'my-ruby-electric-mode-hook))
(my-add-hook my-ruby-electric-mode-hook
  (define-key ruby-mode-map (kbd "C-j") 'next-line)
  (define-key ruby-mode-map (kbd "C-v") 'ruby-electric-return)
  (my-define-keys ruby-mode-map
      "C-j"      'next-line
      "C-v"      'ruby-electric-return
      "C-M-h"    'ruby-backward-sexp
      "C-M-l"    'ruby-forward-sexp
      "C-c o"    'ruby-switch-to-inf
      "\""       'skeleton-pair-insert-maybe
      "("        'skeleton-pair-insert-maybe
      "["        'skeleton-pair-insert-maybe
      "{"        'skeleton-pair-insert-maybe
      "C-c C-c"  'my-ruby-load-file-and-go)
  (my-keychord-define ruby-mode-map
    "vm" 'insert-bars))

(my-eval-after-load "inf-ruby"
  (defun my-ruby-load-file-and-go ()
    (interactive)
    (awhen (buffer-file-name)
      (ruby-load-file it)
      (ruby-switch-to-inf t))))

(require 'align)
(add-elements 'align-rules-list
    '(ruby-equal (regexp . "\\( +\\)\\(=\\)\\( +\\)")  (modes . '(ruby-mode))))


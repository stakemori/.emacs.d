(let ((dfd (format "~/.emacs.d/.cask/%s/elpa"
                   emacs-version)))
  (mapc (lambda (d) (setq load-path (cons (expand-file-name d dfd)
                                          load-path)))
        (cddr (directory-files dfd))))
(load "~/.emacs.d/inits/load-path.el")

;; C-q をprefixに
(defvar ctl-q-map (make-keymap))
(my-define-keys global-map
  "C-q"      ctl-q-map
  "<f5>"     'kmacro-insert-counter
  "C-q i"    'kmacro-insert-counter
  "C-q a"    'kmacro-add-counter
  "C-q C-i"  'quoted-insert
  "C-c q"    'query-replace
  "M-p"      (lambda () (interactive)
               (delete-indentation)
               (delete-trailing-whitespace))
  "M-i"      'indent-region
  "C-l"      'forward-char
  "C-h"      'backward-char
  "M-l"      'forward-word
  "M-h"      'backward-word
  "C-j"      'next-line
  "C-k"      'previous-line
  "C-n"      'delete-backward-char
  "M-n"      'backward-kill-word
  "C-v"      'newline-and-indent
  "C-M-v"    'comment-indent-new-line
  "<f6>"     'bookmark-set
  "C-M-f"    'forward-sexp
  "C-x C-k"  (lambda () (interactive) (kill-buffer))
  "C-x g"    'goto-line
  "C-M-l"    'forward-sexp
  "C-M-h"    'backward-sexp
  "C-M-j"    'forward-paragraph
  "C-M-k"    'backward-paragraph
  "C-x C-9"  'kill-buffer-and-window
  "C-;"      'recenter-top-bottom
  "C-p"      'kill-line
  "C-q p"    'kill-sexp
  "C-q d"    'backward-kill-sexp
  "M-w"      'copy-region-as-kill
  "C-z"      nil
  "C-S-Z"    nil
  "C-x C-n"  nil
  "C-c C-d"  'my-desktop-clear
  "<f9>"     'vr/replace
  "S-<f9>"   'query-replace-regexp
  "M-j"      'scroll-up
  "M-k"      'scroll-down
  "C-M-n"    'scroll-other-window
  "C-M-p"    'scroll-other-window-down
  "<f8>"     'replace-string
  "S-<f8>"   'query-replace
  "C-\\"     'ignore
  "M-o"      'occur
  "C-c w"    (lambda () (interactive)
               (let ((fname (buffer-file-name (current-buffer))))
                 (kill-new fname)
                 (message (format "Copied %s to kill-ring." fname))))
  "C-c n n"  'narrow-to-region
  "C-c n w"  'widen
  "C-c n d"  'narrow-to-defun
  "C-c n p"  'narrow-to-page
  "M-\\"     'shell-command-on-region
  "C-c C-f"  'find-file-at-point
  "H-t"      'open-in-terminal
  "C-`"      'helm-resume
  "M-`"      'helm-resume
  "C-M-w"    'mark-defun)

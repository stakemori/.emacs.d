;; -*- lexical-binding: t -*-

;; Sage
(add-to-list 'load-path "~/work/elisp/sage-shell-mode")
(add-to-list 'load-path "~/work/elisp/auto-complete-sage")
(add-to-list 'load-path "~/work/elisp/ob-sagemath")
(add-to-list 'load-path "~/work/elisp/company-sage")

(add-to-list 'auto-mode-alist
             (cons (rx bow "sage" eow (0+ nonl) ".py" eol) 'sage-mode))
(defalias 'sage-mode 'sage-shell:sage-mode)
(my/lazy-progn
 (sage-shell:define-alias))

(setq sage-shell:input-history-cache-file "~/.sage/.sage_shell_history"
      ac-sage-complete-on-dot t
      ac-sage-show-quick-help nil
      sage-shell-sagetex:auctex-command-name "latexmk -pdfdvi")

(add-hook 'sage-shell-mode-hook #'eldoc-mode)
(add-hook 'sage-shell:sage-mode-hook #'eldoc-mode)
(my-add-hook sage-shell-mode-hook
  (require 'company-sage)
  (setq-local company-backends '((company-sage company-dabbrev-code)))
  (setq-local eldoc-idle-delay 0.1)
  (my-define-keys sage-shell-mode-map
    "C-M-l" nil
    "'" 'skeleton-pair-insert-maybe
    "C-c C-b" 'my-make-sage-scratch
    "C-c C-d" 'my-sage-pop-to-help-buffer
    "<end>"   'sage-shell-pdb:input-next
    ;; helm-sage
    "C-c C-i" 'helm-sage-shell
    "C-c C-h" 'helm-sage-shell-describe-object-at-point
    "M-r" 'helm-sage-command-history
    "C-c o" 'helm-sage-output-history)
  (smartrep-define-key sage-shell-mode-map (kbd "C-c")
    '(("n" . 'sage-shell-pdb:input-next)
      ("s" . 'sage-shell-pdb:input-step)
      ("c" . 'sage-shell-pdb:input-continue)
      ("u" . 'sage-shell-pdb:input-up)
      ("d" . 'sage-shell-pdb:input-down))))

(my-add-hook sage-shell:sage-mode-hook
  (setq-local company-backends (list (cons 'company-sage (car company-backends))))
  (my-define-keys sage-shell:sage-mode-map
    "C-c C-j" 'sage-shell-edit:send-line*
    "RET" 'newline
    "'" 'skeleton-pair-insert-maybe))

;;; ac-sage
;; (remove-hook 'sage-shell:sage-mode-hook 'ac-sage-setup)
(with-eval-after-load 'auto-complete-sage
  (setq sage-shell:completion-function 'completion-at-point))

(my/lazy-progn
 (with-eval-after-load "latex"
   (my-define-keys LaTeX-mode-map
     "C-c s c" 'sage-shell-sagetex:compile-current-file
     "C-c s C" 'sage-shell-sagetex:compile-file
     "C-c s r" 'sage-shell-sagetex:run-latex-and-load-current-file
     "C-c s R" 'sage-shell-sagetex:run-latex-and-load-file
     "C-c s l" 'sage-shell-sagetex:load-current-file
     "C-c s L" 'sage-shell-sagetex:load-file
     "C-c C-z" 'sage-shell-edit:pop-to-process-buffer))
 (defun my-sage-pop-to-help-buffer ()
   (interactive)
   (awhen (get-buffer sage-shell-help:help-buffer-name)
     (pop-to-buffer it)))

 (cl-defun my-make-sage-scratch (&optional (bfn "*Sage scratch*"))
   (interactive)
   (with-current-buffer (pop-to-buffer (get-buffer-create bfn))
     (sage-shell:sage-mode))))

(my-named-add-hook sage-shell:sage-mode-hook
  test-compile-cmd
  (let ((bfn (buffer-file-name))
        (sage-exec (executable-find "sage")))
    (when (and bfn sage-exec (with-syntax-table (standard-syntax-table)
                               (string-match (rx bow "test" eow) bfn)))
      (when-let (root (ignore-errors (projectile-project-root)))
        (let ((mod-name
               (->>
                (s-split
                 (f-path-separator)
                 (f-no-ext (f-relative (buffer-file-name) root)))
                (cons (f-filename root))
                (s-join "."))))
          (setq-local compile-command
                      (format "%s -c 'sys.path.append(\"%s\"); import %s'"
                              sage-exec
                              (f-parent root)
                              mod-name)))))))

(my-named-add-hook cython-mode-hook
  disable
  (flycheck-mode -1)
  (which-function-mode -1)
  (anaconda-mode -1))
;; (setq sage-shell:redirect-long-output-p t)
(setq sage-shell:sage-root "~/sage/")

(with-eval-after-load 'sage-shell-mode
  (push '(?n . sage-shell:-report-cursor-pos)
        sage-shell:-ansi-escpace-handler-alist))
(setq sage-shell:check-ipython-version-on-startup nil)
(setq sage-shell-blocks:delimiter "#-")
(setq sage-shell:ask-command-options nil)

;; -*- lexical-binding: t -*-
(my/lazy-progn (global-company-mode 1))
;; (setq company-echo-delay 0.8)
(setq company-idle-delay 0.5)
(setq company-dabbrev-ignore-case nil)
(setq company-minimum-prefix-length 3)
(my-eval-after-load "company"
  (setq company-dabbrev-ignore-buffers (rx bol (1+ space)))
  (setq company-dabbrev-char-regexp (rx (or (syntax symbol) word)))
  (my-define-keys company-active-map
    "C-j" 'company-select-next
    "C-k" 'company-select-previous
    "C-M-i" 'company-complete-common
    "C-i" 'company-complete-selection))
(my-named-add-hook text-mode-hook "company"
                   (setq-local company-backends '(company-dabbrev)))
(my-define-keys global-map
  "C-M-i" 'company-complete
  "M-SPC" 'company-complete)
(my-named-add-hook gud-mode-hook
  company-off
  (company-mode -1))
(my-named-add-hook shell-mode-hook
  company-off
  (company-mode -1))

;; 環境依存な設定
(cl-case system-type
  (gnu/linux (load (locate-user-emacs-file "env_dep/ubuntu"))))

(cl-loop for f in (directory-files (locate-user-emacs-file "env_dep"))
         with sname = (system-name)
         if (string= (file-name-nondirectory (file-name-sans-extension f)) sname)
         do (load f))

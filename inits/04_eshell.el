;; eshell
(require 'bind-key)
(face-spec-set 'eshell-prompt
 `((t (:foreground "#599eff"))))
;; eshell auto-complete
(setq eshell-aliases-file (locate-user-emacs-file ".eshell/eshell_alias"))
;; eshell complete
(defvar ac-source-eshell-git nil)
(setq ac-source-eshell-git
      '((candidates . my-eshell-ac-git-candidates)
        (prefix . my-eshell-ac-git-prefix)
        (cache)))
(defun my-eshell-ac-git-prefix ()
  (let ((chars "a-zA-Z0-9-_./"))
    (when (looking-back (format "git +[%s]*" chars))
      (save-excursion
        (+ (point) (skip-chars-backward "a-zA-Z0-9-_./"))))))
(defun my-eshell-ac-git-candidates ()
  '("init" "clone" "fsck" "gc" "status" "diff"
    "add" "commit" "log" "reset" "revert" "branch"
    "checkout" "show-branch" "merge" "tag" "stash"
    "rebase" "pull" "git" "push"))

(defvar ac-source-eshell-shell-commands nil)
(setq  ac-source-eshell-shell-commands
       '((candidates . my-eshell-complete-candidates)
         (prefix . my-eshell-complete-prefix)
         (cache)))

(defun my-eshell-complete-prefix ()
  (let ((chars "a-zA-Z0-9-_./"))
    (when (and (not (looking-back (format "git +[%s]*" chars)))
               (looking-back (format "[%s]" chars)))
      (save-excursion
        (+ (point) (skip-chars-backward "a-zA-Z0-9-_./"))))))

(defvar my-eshell-complete-candidates nil)
(defun my-eshell-complete-candidates ()
  (aif my-eshell-complete-candidates it
    (setq my-eshell-complete-candidates
          (cl-loop for dir in (split-string (getenv "PATH") ":")
                append (awhen (ignore-errors (directory-files dir))
                         (cl-loop for cmd in it
                               unless (string-match "^\\." cmd)
                               collect cmd))))))

(defvar my-eshell-ac-functions-cache nil)
(defun my-eshell-ac-function-candidates ()
  (or my-eshell-ac-functions-cache
      (setq my-eshell-ac-functions-cache
            (cl-loop for x being the symbols
                  if (fboundp x)
                  collect
                  (if (string-match "^eshell/\\(.+\\)"(symbol-name x))
                      (match-string 1 (symbol-name x))
                    (symbol-name x))))))

(defvar ac-source-my-eshell-functions nil)
(setq ac-source-my-eshell-functions
  '((candidates . my-eshell-ac-function-candidates)
    ;; (document . ac-symbol-documentation)
    (symbol . "f")
    (prefix . my-eshell-complete-prefix)
    (cache)))
(defvar ac-source-my-eshell-variables nil)
(setq ac-source-my-eshell-variables
      '((candidates . ac-variable-candidates)
    ;;    (document . ac-symbol-documentation)
        (prefix . my-eshell-complete-prefix)
        (symbol . "v")
        (cache)))
(defvar ac-source-eshell-current-directory-filename nil)
(setq ac-source-eshell-current-directory-filename
      '((candidates . (lambda () (directory-files default-directory)))
        (prefix . my-eshell-complete-prefix)
        (cache)))
(defun my-ac-eshell-mode ()
  (setq ac-sources
        '(ac-source-yasnippet
          ac-source-filename
          ac-source-eshell-current-directory-filename
          ac-source-eshell-git
          ac-source-eshell-shell-commands
          ac-source-my-eshell-functions
          ac-source-my-eshell-variables
          ac-source-words-in-same-mode-buffers))
  (show-paren-mode -1)
  (my-define-keys (current-local-map)
    "C-j"      'next-line
    "M-r"      'helm-eshell-history
    "C-c C-e"  'eshell-expand-list))

(add-hook 'eshell-mode-hook 'my-ac-eshell-mode)
(defun eshell-pop (arg)
  (interactive "p")
  (if (equal arg 1)
      (if (get-buffer "*eshell*<1>")
          (pop-to-buffer "*eshell*<1>")
        (eshell 1))
    (let ((dir default-directory))
      (if (get-buffer "*eshell*<1>")
          (pop-to-buffer "*eshell*<1>")
        (eshell 1))
      (unless (equal dir default-directory)
        (eshell-cd-default-directory dir)))))

(defun eshell-new ()
  (interactive)
  (let ((n (1+ (->> (buffer-list)
                 (-map #'buffer-name)
                 (-map
                  (lambda (x)
                    (when (string-match (rx "*eshell*"
                                            "<"
                                            (group (1+ num))
                                            ">") x)
                      (string-to-number (match-string 1 x)))))
                 (-filter #'identity)
                 (apply #'max))))
        (dir default-directory))
    (eshell n)
    (unless (string= dir default-directory)
      (eshell-cd-default-directory dir))))

(defun eshell-cd-default-directory (dir)
  (cd dir)
  (eshell-interactive-print (concat "cd " dir "\n"))
  (eshell-emit-prompt)
  (eshell/clear-last-line))

(defun eshell/clear-last-line ()
  (save-excursion
    (let ((inhibit-read-only t) beg end)
      (forward-line -1)
      (setq beg (line-beginning-position)
            end (line-end-position))
      (delete-region beg end) (delete-char 1))))

(defun eshell/clear ()
 "Clear the current buffer, leaving one prompt at the top."
 (interactive)
 (let ((inhibit-read-only t))
   (erase-buffer)))

(defun eshell/get-buffer-and-display (&optional bufn)
  (let ((bufn (or bufn "*eshell output*")))
    (cond ((get-buffer bufn)
           (let ((inhibit-read-only t)
                 (view-read-only nil))
             (with-current-buffer bufn
               (erase-buffer)
               (view-mode -1))))
          (t (get-buffer-create bufn)))
    (display-buffer bufn)
    (get-buffer bufn)))

(defun eshell-expand-list ()
  (interactive)
  (let ((lst (symbol-value (preceding-sexp)))
        (kill-ring nil))
    (save-excursion
      (backward-char)
      (kill-sexp*))
    (insert (mapconcat (lambda (x) (format "%s" x)) lst " "))))

(defun eshell/add-elments (lst-name &rest elmts)
  (unless (ignore-errors (eval (intern lst-name)) t)
    (set (intern lst-name) nil))
  (let ((lst-var (intern lst-name))
        (elmts elmts))
    (cl-loop for i in (nreverse elmts)
          do (add-to-list lst-var i))))

(defun eshell/byte-recompile-if-needed (file)
  (unless (and (file-exists-p (concat file "c"))
               (file-newer-than-file-p (concat file "c")
                                       file))
    (byte-compile-file file)))

(defun eshell/less (&rest args)
 "Invoke `view-file' on the file.
\"less +42 foo\" also goes to line 42 in the buffer."
 (while args
   (if (string-match "\\`\\+\\([0-9]+\\)\\'" (car args))
       (let* ((line (string-to-number (match-string 1 (pop args))))
              (file (pop args)))
         (view-file file)
         (goto-char (point-min)) (forward-line (1- line)))
     (view-file (pop args)))))


(defun my/shell-new ()
  (interactive)
  (pop-to-buffer
   (shell (format
           "*shell-%s*"
           (get-buffer-create default-directory)))))

(bind-key* "C-c ;" 'my/shell-new)

(setq eshell-ask-to-save-history (quote always))
(setq eshell-history-size 1000)
(setq eshell-ls-exclude-regexp "~\\'")
(setq eshell-ls-initial-args "-h")
(setq eshell-prefer-to-shell t)
(setq eshell-stringify-t nil)
(setq eshell-term-name "ansi")
(setq eshell-visual-commands (quote ("vi" "top" "screen" "less" "lynx"
                                     "ssh" "rlogin" "telnet")))
(my-named-add-hook eshell-mode-hook
  "config"
  (company-mode -1))
(with-eval-after-load 'eshell
  (require 'pcmpl-args))
(with-eval-after-load 'shell
  (require 'pcmpl-args))

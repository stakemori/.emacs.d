(require 'anything-match-plugin)
(defun agp-candidates-lang-c (&optional filter)
  "Normal version of grep-candidates candidates function with LANG=C.
Grep is run by asynchronous process."
  (start-process-shell-command
   "anything-grep-candidates" nil
   (agp-command-line-2-lang-c filter (anything-attr-defined 'search-from-end))))
(defun agp-command-line-lang-c (query files &optional limit filter search-from-end)
  (let ((result (agp-command-line query files limit filter search-from-end)))
  (while (string-match "\\(^\\|| \\)\\(grep -ih\\)" result)
    (setq result (replace-match "LANG=C \\2" t nil result 2)))
  result))
(defun agp-command-line-2-lang-c (filter &optional search-from-end)
  "Build command line used by grep-candidates from FILTER and current source."
  (agp-command-line-lang-c
   anything-pattern
   (anything-mklist (anything-interpret-value (anything-attr 'grep-candidates-lang-c)))
   (anything-candidate-number-limit (anything-get-current-source))
   filter search-from-end))
(defun anything-compile-source--grep-candidates-lang-c (source)
  (anything-aif (assoc-default 'grep-candidates-lang-c source)
      (append
       source
       (cond ((not (anything-interpret-value it)) nil)
             (t
              (anything-log "normal version lang = c")
              '((candidates . agp-candidates-lang-c)
                (delayed)))))
    source))
(add-to-list 'anything-compile-source-functions 'anything-compile-source--grep-candidates-lang-c)
(provide 'anything-grep-lang-c)





(set-fontset-font nil 'unicode (font-spec :family "DejaVu Sans Mono" :size 14) nil)
(set-fontset-font nil 'japanese-jisx0208 "TakaoPMincho" nil 'append)
(let ((font "-PfEd-DejaVu Sans Mono-normal-normal-normal-*-14-*-*-*-m-0-iso10646-1"))
  (set-face-font 'variable-pitch font))

;; (add-to-list 'load-path "~/app/lsp-mode")
(add-to-list 'exec-path "~/.cargo/bin")
;; (setenv "RUST_SRC_PATH"
;;         (expand-file-name
;;          "lib/rustlib/src/rust/src"
;;          (expand-file-name
;;           "~/.rustup/toolchains/nightly-x86_64-unknown-linux-gnu")))
(add-hook 'rust-mode-hook #'racer-mode)
(add-hook 'racer-mode-hook #'eldoc-mode)
(with-eval-after-load "rust-mode"
  (my-define-keys rust-mode-map
    "C-c C-c" #'compile
    "C-c c" (lambda () (interactive)
              (shell-command
               (read-shell-command
                "Shell command: "
                (format "../target/debug/%s"
                        (projectile-project-name))
                'shell-command-history))))
  (my-keychord-define rust-mode-map
    "rj" (lambda () (interactive) (insert "=> ")))
  (with-eval-after-load 'flycheck
    (push "-Zno-trans" flycheck-rust-args)
    (flycheck-def-args-var flycheck-rust-args (rust-cargo rust)
      :package-version '(flycheck . "0.24")
      :risky nil)
    (flycheck-def-args-var flycheck-cargo-rustc-args (rust-cargo)
      :package-version '(flycheck . "30")
      :risky nil)))
(setq company-tooltip-align-annotations t)
(setq rust-match-angle-brackets nil)
(my-add-hook rust-mode-hook
  (flycheck-rust-setup)
  (flycheck-mode 1)
  (company-mode 1)
  (setq-local compile-command
              (acond ((projectile-project-p)
                      "cargo build")
                     ((buffer-file-name) (format "cargo script %s" (f-relative it)))))
  (setq-local company-backends
              '((company-racer company-dabbrev-code company-gtags company-etags company-keywords)))
  (local-set-key (kbd "TAB") #'company-indent-or-complete-common))

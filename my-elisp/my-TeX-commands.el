(eval-when-compile (require 'cl))
(require 'dired-x)
(require 'tex-buf)
(require 'tex)
(require 'latex)
(require 'my-utilities)
(require 'texmathp)

;;;###autoload
(defun tex-looking-back-command ()
  (looking-back (rx "\\" (1+ letter)) (line-beginning-position) t))

;; ;;;###autoload
;; (defun tex-looking-back-math-var ()
;;   (save-match-data
;;     (and (not (tex-looking-back-command))
;;          (save-excursion
;;            (forward-char -1)
;;            (and (ac-math-latex-math-face-p)
;;                 (not (looking-back
;;                       "\\\\math[a-zA-Z]+{[a-zA-Z]*")))))))
;; (defmacro when-looking-back-math-var (&rest then-forms)
;;   "if (1- (point)) is in tex math mode and not looking back tex command"
;;   (declare (indent 0))
;;   `(when (tex-looking-back-math-var) ,@then-forms))


;; style fileを読み込むユーザー用のdirectory
(defvar my-TeX-texmf-user-dir nil)



;; tex auto-complete 子ファイル
(defun TeX-default-dir-file-list ()
  (mapcar 'file-name-sans-extension
        (directory-files default-directory nil "tex$" nil)))
(defun TeX-default-dir-file-list-input-or-include ()
  (interactive)
  (save-excursion
    (beginning-of-line)
    (when (looking-at-p "\\\\\\(input\\|include\\)")
      (re-search-forward "\\\\\\(input\\|include\\){")))
  )

(defvar ac-source-tex-default-directory-file-list
  '((candidates . TeX-default-dir-file-list)
    (prefix . TeX-default-dir-file-list-input-or-include)
    (cache)
    ))


;; auto detect master file
;;;###autoload
(defun guess-TeX-master (filename)
  "Guess the master file for FILENAME from currently open .tex files."
  (when (and (stringp filename))
    (let ((candidate t)
          (filename-non (file-name-nondirectory filename)))
      (save-excursion
        (when (save-restriction (widen) (goto-char (point-min))
                                (or (not (search-forward "\\begin{document}" nil t))
                                    (and (goto-char (point-min))
                                         (search-forward "\\begin{document}" nil t)
                                         (TeX-in-comment))))
          (dolist (tex-file (TeX-default-dir-file-list))
            (with-temp-buffer
              (when (string-match-p "^\\([mM]ain\\|[mM]aster\\)" tex-file)
                (insert-file-contents (concat default-directory tex-file ".tex"))
                (let ()
                  (goto-char (point-min))
                  (when (re-search-forward
                         (concat "\\\\\\(input\\|include\\){"
                                 (file-name-sans-extension filename-non)
                                 "\\(\\.tex\\)*}") nil t)
                    (setq candidate tex-file) (return))))
              ))))
      (cond
       ((stringp candidate) candidate)
       (t t))
      )))

;;;###autoload
(defun my-TeX-open-master-file ()
  (interactive)
  (let (file)
    (cond
     ((stringp TeX-master)
      (progn
        (setq file (concat default-directory TeX-master ".tex"))
        (when (file-exists-p file) (find-file file))))
     (t
      (message "Visited file is a master file"))
     )))

;; (defun set-TeX-master (&optional filename)
;;   (interactive)
;;   (unless filename (setq filename (buffer-file-name)))
;;   (setq TeX-master (guess-TeX-master filename)))


;; tex-auto-parse

;; (defun my-TeX-readable-file-list ()
;;   (append
;;    (directory-files default-directory nil "\\(tex$\\|sty$\\)" nil)
;;    (when my-TeX-texmf-user-dir
;;      (directory-files my-TeX-texmf-user-dir nil "\\(tex$\\|sty$\\)" nil)
;;      )))
(defun my-TeX-in-comment ()
  (save-excursion (beginning-of-line)
                  (looking-at-p "%")))

;;;###autoload
(defun my-TeX-parse-package (file)
  (let* (lst end)
    (goto-char (point-min))
    (cond
     ((string-match "sty$" file) (setq end (point-max)))
     ((and (save-excursion (re-search-forward "\\\\begin{document}" nil t)
                           (not (my-TeX-in-comment))
                           ))
      (setq end (match-end 0)))
     (t (setq end (point-min)))
     )
    (while (and
            (< (point) end)
            (re-search-forward
             "\\\\\\(usepackage\\|documentclass\\)\\(\\[[^]]+\\]\\)?{\\([^}]+\\)}" nil t)
            )
      (unless (my-TeX-in-comment)
        (push (buffer-substring-no-properties (match-beginning 3)
                                              (match-end 3))
              lst)))
    (when lst
      (cl-loop for str in lst
            append (split-string str ",")))))

;;;###autoload
(defun my-TeX-parse-environment (file)
  (let* (lst end)
    (goto-char (point-min))
    (cond
     ((string-match "sty$" file) (setq end (point-max)))
     ((and (save-excursion (re-search-forward "\\\\begin{document}" nil t)
                           (not (my-TeX-in-comment))))
      (setq end (match-end 0)))
     (t (setq end (point-min)))
     )
    (while (and
            (< (point) end)
            (re-search-forward
             "\\\\\\(newtheorem\\|newenvironment\\)\\(\\[[^]]+\\]\\)?{\\([^}]+\\)}" nil t))
      (unless (my-TeX-in-comment)
        (push (buffer-substring-no-properties (match-beginning 3)
                                              (match-end 3))
              lst)))
    lst))

(cl-defun my-tex-required-files (&optional dir (max-pt 5000))
  (let ((master-file
         (cond
          ((eq TeX-master t)
           (buffer-file-name))
          ((stringp TeX-master)
           (expand-file-name (concat TeX-master ".tex")))))
        (scommands '("usepackage" "RequirePackage"))
        (files nil))
    (when master-file
      (with-temp-buffer
        (insert-file-contents master-file)
        (setq files
              (cl-loop initially
                    (goto-char (point-min))
                    while  (re-search-forward
                            (rx "\\" (group (or "usepackage" "RequirePackage"
                                                "input" "include"))
                                "{" (group (1+ (not (any "}")))) "}")
                            nil t)
                    append (let ((f (if (member (match-string 1) scommands)
                                        (concat (match-string 2) ".sty")
                                      (concat (match-string 2) ".tex"))))
                             (cond (dir (list (expand-file-name f)
                                              (expand-file-name f dir)))
                                   (t (list (expand-file-name f)))))))
        (cons master-file
              (cl-loop  for f in files
                     if (and f (file-exists-p f))
                     collect f))))))
;;;###autoload
(defun my-TeX-auto-parse ()
  (interactive)
  (let* (lstpa lste x)
    (dolist (file (my-tex-required-files my-TeX-texmf-user-dir))
      (with-temp-buffer
        (insert-file-contents file)
        (setq lstpa (my-TeX-parse-package file))
        (setq lste (my-TeX-parse-environment file)))
      (when lstpa
        (dolist (x lstpa)
          (TeX-run-style-hooks x)
          ))
      (when lste
        (dolist (x lste)
          (LaTeX-add-environments x))))))


;; math modeでないところのコンマを全角コンマにかえる
;;;###autoload
(defun my-TeX-replace-hankaku-commas-in-this-buffer (beg end)
  (interactive "r")
  (let (b e)
    (save-excursion
      (save-restriction
        (widen)
        (when (region-active-p) (setq b beg e end))
        (my-TeX-replace-hankaku-commas (buffer-file-name) b e)))))

;; diredでmarkされたfileについてmy-TeX-replace-hankaku-commasを実行
;;;###autoload
(defun my-TeX-replace-hankaku-commas-dired ()
  (interactive)
  (dolist (x (dired-get-marked-files))
    (my-TeX-replace-hankaku-commas x nil nil t)
    ))

;;;###autoload
(defun my-TeX-replace-hankaku-commas (file &optional b e silent)
  (widen)
  (let* ((n 0))
    (set-buffer (find-file-noselect file))
    (unless (and b e)
      (goto-char (point-min))
      (when (re-search-forward
             "\\(\\\\begin{document}\\)" (point-max) t)
        (setq b (match-beginning 1)))
      (when (re-search-forward
             "\\(\\\\end{document}\\|\\\\begin{thebibliography}\\)"
             (point-max) t)
        (setq e (match-beginning 1))))
    (goto-char b)
    (while (re-search-forward "\\(,\\|\\.\\)\\( \\|　\\)*" e t)
      (let (s (data (match-data)))
        (unless (or (save-excursion (forward-char -1) (texmathp))
                    (my-TeX-inside-label-or-ref-p)
                    (my-TeX-inside-dai-kakko-p)
                    )
          (set-match-data data)
          (setq s (buffer-substring-no-properties
                   (match-beginning 1) (1+ (match-beginning 1))))
          (cond
           ((string= s ",") (replace-match "，"))
           ((string= s ".") (replace-match "．"))
           )
          (incf n))))
    ;; 全角コンマ+空白を全角コンマに置換
    (goto-char b)
    (while (re-search-forward "\\(，\\|．\\)\\( \\|　\\)+" nil t)
      (let (s)
        (setq s (buffer-substring-no-properties
                 (match-beginning 1) (1+ (match-beginning 1))))
        (cond
         ((string= s "，") (replace-match "，"))
         ((string= s "．") (replace-match "．"))
         )
        (incf n)))
    (unless silent
      (cond
       ((= n 1) (message (format "Replaced %d hankaku comma" n)))
       (t (message (format "Replaced %d hankaku commas" n)))))))

;;;###autoload
(defun my-TeX-inside-label-or-ref-p ()
  (interactive)
  (save-excursion
    (let* (a b (p (point)))
      (when (re-search-backward "\\(label\\|ref\\)\\({\\)" nil t)
       (setq a (match-beginning 2))
       (goto-char a)
       (when (re-search-forward "\\(}\\)" nil t)
         (setq b (match-beginning 1))
         (and (< a p) (< p b)))
       ))))
;;;###autoload
(defun my-TeX-inside-dai-kakko-p ()
  (interactive)
  (save-excursion
    (let* (a b (p (point)))
      (when (and
             (re-search-backward "\\[" nil t)
             (not (texmathp)))
        (setq a (point))
        (forward-char -1)
        (when (and (re-search-forward "\\[[^]]*\\]" nil t)
                   (not (texmathp)))
          (and (< a p) (< p (point)))
          )))))


;; preambleとthebibliographyをcomment-outする．
;;;###autoload
(defun my-TeX-comment-out-preamble (&optional buf-f-n)
  (interactive)
  (let (b e)
    (when buf-f-n
        (set-buffer (find-file-noselect buf-f-n)))
    (save-excursion
      (save-restriction
        (widen)
        (goto-char (point-min))
        (when (re-search-forward "\\(\\\\begin{document}\\)" (point-max) t 1)
          (comment-region 1 (match-end 1))
          (when (re-search-forward "\\(\\\\end{document}\\)" (point-max) t 1)
            (comment-region (match-beginning 1) (match-end 1))
            ))
        (goto-char (point-min))
        (when (re-search-forward
               "\\(\\\\begin{thebibliography}\\)" (point-max) t 1)
          (setq b (match-beginning 1))
          (when (re-search-forward
                 "\\(\\\\end{thebibliography}\\)" (point-max) t 1)
            (setq e (match-end 1))
            (comment-region b e)
            ))))))
;; diredでmarkしたfileにたいして上を実行
;;;###autoload
(defun my-TeX-comment-out-preamble-dired-marked-files ()
  (interactive)
  (mapcar 'my-TeX-comment-out-preamble (dired-get-marked-files))
  )


;; multiply defined labelsを調べる
;;;###autoload
(defun my-TeX-investigate-multiply-defined-labels-write ()
  (interactive)
  (save-excursion
    (let (l num)
      (setq l (reverse
             (my-TeX-investigate-multiply-defined-labels-list-funct)))
      (dolist (lab l)
        (insert (format
                 "LaTeX Warning: Label `%s' multiply defined.\n " lab))
        (dolist (flnm (reverse (remove "main" (TeX-default-dir-file-list))))
          (setq num (my-TeX-investigate-multiply-defined-labels-in-a-file
            lab flnm))
          (when num
             (insert (format "%sの中に%dつ， "
                             flnm num)))
          )
        (delete-char (- 2))
        (insert ".\n")
        )
      )))
;;;###autoload
(defun my-TeX-investigate-multiply-defined-labels-list-funct ()
  (let* (l)
    (with-temp-buffer
      (when (file-exists-p (concat default-directory "main.log"))
        (insert-file-contents
         (concat default-directory "main.log")))
      (goto-char (point-min))
      (while (re-search-forward
              "^LaTeX Warning: Label `\\(.+\\)' multiply defined\\."
              (point-max) t 1)
        (setq l (cons (buffer-substring-no-properties (match-beginning 1)
                                                      (match-end 1)) l)))
      l)))
;;;###autoload
(defun my-TeX-investigate-multiply-defined-labels-in-a-file
  (label &optional bufflnm)
  (let* ((s nil))
    (with-temp-buffer
      (when (and bufflnm
                 (file-exists-p (concat default-directory bufflnm ".tex")))
        (insert-file-contents (concat default-directory bufflnm ".tex")))
      (goto-char (point-min))
      (while (search-forward
              (format "\\label{%s}" label) (point-max) t 1)
        (setq s (cons 1 s)))
      (when s
        (length s)))))


;; カタカナ語の検索
(defvar my-TeX-katakana-string
      (let* ((l "\\(ァ"))
        (cl-loop for i from 12450 to 12531
              do
              (setq l (concat l "\\|" (char-to-string i))))
        (concat l "\\|ー\\)+")
        ))
;;;###autoload
(defun my-TeX-katakana-search ()
  (interactive)
  (let* (l s kat)
    (save-excursion
      (goto-char (point-min))
      (while  (re-search-forward my-TeX-katakana-string nil t)
        (when (not (TeX-in-comment))
          (setq kat (buffer-substring-no-properties (match-beginning 0)
                                                    (match-end 0)))
          (when (looking-at-p (concat "\\( \\|\n\\)+"
                                      my-TeX-katakana-string))
            (setq kat (concat kat
                              (progn
                                (re-search-forward
                                 my-TeX-katakana-string)
                                (buffer-substring-no-properties
                                 (match-beginning 0)
                                 (match-end 0))))))
          (when (> (length kat) 1) (add-to-list 'l kat)))
        )
      )
    (when l
      (setq l (reverse l))
      (dolist (x l)
        (setq s (concat s " " x))
        )
      (with-current-buffer "*scratch*"
        (erase-buffer)
        (insert s)))
    ))

;; (defun my-TeX-katakana-bara-search ()
;;   (interactive)
;;   (let* (l s kat)
;;     (save-excursion
;;       (goto-char (point-min))
;;       (while (re-search-forward my-TeX-katakana-string nil t)
;;         (setq kat (buffer-substring-no-properties (match-beginning 0)
;;                                                   (match-end 0)))
;;         (add-to-list 'l kat)
;;         ))
;;     (setq l (reverse l))
;;     (dolist (x l)
;;       (setq s (concat s " " x))
;;       )
;;     (with-current-buffer "*scratch*"
;;       (erase-buffer)
;;       (insert s))
;;     ))

;;; my-tex-clean
;;;###autoload
(defun my-TeX-clean (&optional arg)
  "my-TeX-clean:
Delete generated files associated with current master and region files.
If prefix ARG is non-nil, not only remove intermediate but also
output files."
  (interactive "P")
  (let* ((mode-prefix (TeX-mode-prefix))
         (suffixes (append (symbol-value
                            (intern (concat mode-prefix
                                            "-clean-intermediate-suffixes")))
                           (when arg
                             (symbol-value
                              (intern (concat mode-prefix
                                              "-clean-output-suffixes"))))))
         (master (TeX-active-master))
         (master-dir (file-name-directory master))
         (regexp (concat "\\("
                         (file-name-nondirectory master) "\\|"
                         (TeX-region-file nil t)
                         "\\)"
                         "\\("
                         (mapconcat 'identity suffixes "\\|")
                         "\\)\\'"
                         "\\|" (TeX-region-file t t)))
         (files (when regexp
                  (directory-files (or master-dir ".") nil regexp))))
    (when files
        (dolist (file files)
          (delete-file (concat master-dir file)))
      )))

;; 参照されていないlabel
(defun mt:collect-matching-string (regexp)
  (save-restriction
    (save-excursion
      (cl-loop initially
            (widen)
            (goto-char (point-min))
            with res = nil
            while (re-search-forward regexp nil t)
            do  (add-to-list 'res (match-string-no-properties 1))
            finally (return res)))))

;;;###autoload
(defun mt:check-label-referred ()
  (interactive)
  (aif (cl-loop for lb in (mt:collect-matching-string
                        (rx "\\label{"
                            (group (1+ (not (any "}" " " "\n"))))
                            "}"))
             with refs = (mt:collect-matching-string
                          (rx "\\" (0+ alpha)
                              "ref{"
                              (group (1+ (not (any "}" " " "\n"))))
                              "}"))
             unless (member lb refs)
             collect lb)
      (message (let ((one? (eq (length it) 1)))
                 (concat "Label"
                         (if one?  " " "s ")
                         (mapconcat 'identity it ", ")
                         (if one? " is" " are")
                         " not referred.")))
    (message "All labels are referred.")))

(provide 'my-TeX-commands)

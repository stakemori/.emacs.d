;; -*- lexical-binding: t -*-
(defvar xmodmap-mode-syntax-table
  (let ((table (make-syntax-table)))
    (modify-syntax-entry ?! "<" table)
    (modify-syntax-entry ?\n ">" table)
    table))

(define-derived-mode xmodmap-mode nil
  "Xmodmap" "Mode for editing .Xmodmap."
  (set-syntax-table xmodmap-mode-syntax-table)
  (setq comment-start "!"))

(font-lock-add-keywords 'xmodmap-mode
                          `(,(cons (rx bow (or "add" "clear" "remove" "keycode") eow)
                                   'font-lock-keyword-face)))


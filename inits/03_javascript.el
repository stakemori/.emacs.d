(add-hook 'js2-mode-hook #'skewer-mode)
(add-hook 'css-mode-hook #'skewer-css-mode)
(add-hook 'html-mode-hook #'skewer-html-mode)
(add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))

(my-named-add-hook js2-mode-hook
  (my-define-keys js2-mode-map
    "C-c C-l" #'skewer-load-buffer))

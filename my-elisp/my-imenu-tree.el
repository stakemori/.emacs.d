(require 'view)
(require 'imenu)
(require 'cl-lib)
(require 'anaphora)
(defvar imtr:button-keymap (make-sparse-keymap))

(defmacro imtr:define-keys (keymap &rest defs)
  (declare (indent 1))
  (append (list 'progn)
          (cl-loop for i from 0 to (1- (/ (length defs) 2))
                   collect
                   `(define-key
                      ,keymap
                      (kbd ,(nth (* 2 i) defs))
                      ,(nth (1+ (* 2 i)) defs)))))

(define-key imtr:button-keymap (kbd "<down-mouse-1>") 'push-button)
(define-key imtr:button-keymap (kbd "RET") 'push-button)

(defvar imtr:offset 2)

(defvar imtr:imenu-source-buffer nil)
(defvar imtr:imenu-buffer-name " *imenu-tree*")

;; (defvar imtr:closed-str "[+]")
;; (defvar imtr:open-str "[-]")
(defvar imtr:node-str "* ")

(make-face 'imtr:face)

(face-spec-set 'imtr:face
 '((((class color) (background dark))
    (:weight semibold))))

(defun imtr:insert-button (key val)
  (let ((btn-str (if (and (listp val) val)
                     (concat imtr:node-str key)
                   key))
        (start (point)))
    (insert btn-str)
    (make-text-button start (point)
                      'action #'imtr:button-action
                      'keymap imtr:button-keymap
                      'value val
                      'closed t
                      'name key
                      'depth (- start (line-beginning-position))
                      'face (aif (get-text-property 0 'face key)
                                it
                              'imtr:face))))

(run-hooks 'view-mode-hook)
(defvar imtr:minor-mode-map (copy-keymap view-mode-map))
(define-minor-mode imtr:minor-mode "" nil nil imtr:minor-mode-map)

(defun imtr:visit-other-window ()
  (interactive)
  (let ((win (selected-window)))
    (beginning-of-line)
    (re-search-forward "[^ ]" nil t)
    (push-button)
    (delete-window win)))

(defun imtr:display-other-window ()
  (interactive)
  (save-excursion
    (beginning-of-line)
    (re-search-forward "[^ ]" nil t)
    (let* ((btn (button-at (point)))
           (val (button-get btn 'value)))
      (when (integer-or-marker-p val)
        (with-selected-window (display-buffer imtr:imenu-source-buffer)
          (goto-char val)
          (recenter))))))

(imtr:define-keys imtr:minor-mode-map
                  "h"    'imtr:close-all
                  "n"    'imtr:next-top-level
                  "p"    'imtr:previous-top-level
                  "K"    'imtr:scroll-down-other-window
                  "J"    'imtr:scroll-up-other-window
                  "SPC"  'imtr:display-other-window
                  "o"    'imtr:display-other-window
                  "RET"  'imtr:visit-other-window)

(defun imtr:close-all ()
  (interactive)
  (let ((inhibit-read-only t))
    (erase-buffer)
    (imtr:insert-buttons (imtr:imenu-alist) 0)))

(defun imtr:button-action (btn)
  (let ((val (button-get btn 'value))
        (win (selected-window)))
    (cond ((integer-or-marker-p val)
           (select-window (display-buffer imtr:imenu-source-buffer))
           (goto-char val)
           (recenter)
           (delete-window win))
          ((button-get btn 'closed)
           (imtr:open-button btn))
          (t (imtr:close-button btn)))))

(defun imtr:next-top-level ()
  (interactive)
  (forward-line 1)
  (re-search-forward "^[^ ]" nil t)
  (beginning-of-line))

(defun imtr:previous-top-level ()
  (interactive)
  (forward-line -1)
  (re-search-backward "^[^ ]" nil t)
  (beginning-of-line))

(defun imtr:scroll-down-other-window ()
  (interactive)
  (with-selected-window (display-buffer imtr:imenu-source-buffer)
    (scroll-down)))

(defun imtr:scroll-up-other-window ()
  (interactive)
  (with-selected-window (display-buffer imtr:imenu-source-buffer)
    (scroll-up)))

(defun imtr:close-button (btn)
  (save-excursion
    (let* ((inhibit-read-only t)
           (btn-st (button-start btn))
           (val (button-get btn 'value))
           (name (button-get btn 'name))
           (depth (button-get btn 'depth))
           (rgxp (concat "^" (make-string depth ? ) "[^ ]")))
      (goto-char btn-st)
      (forward-line 1)
      ;; delete children
      (delete-region btn-st
                     (if (re-search-forward rgxp nil t)
                         (1+
                          (save-excursion
                            (forward-line -1)
                            (line-end-position)))
                       (point-max)))
      ;; insert the same button
      (beginning-of-line)
      (newline)
      (forward-line -1)
      (imtr:insert-button name val))))

(defun imtr:open-button (btn)
  (save-excursion
    (let* ((inhibit-read-only t)
           (btn-st (button-start btn))
           (val (button-get btn 'value))
           (depth (button-get btn 'depth)))
      (goto-char btn-st)
      (end-of-line)
      (newline)
      (imtr:insert-buttons val (+ imtr:offset depth))
      (button-put btn 'closed nil))))

(defun imtr:insert-buttons (tree offset)
  (save-excursion
    (let ((fill (make-string offset ? )))
      (cl-loop for cs on tree
               for (k . v) = (car cs)
               initially (insert fill)
               do
               (imtr:insert-button k v)
               if (cdr cs)
               do
               (newline)
               (insert fill)))))

(cl-defun imtr:imenu-alist (&optional (buf imtr:imenu-source-buffer))
  (with-current-buffer buf
    (save-excursion
      (imenu--make-index-alist)
      (funcall imenu-create-index-function))))

(defun imtr:imenu-tree ()
  (interactive)
  (let ((tree (imtr:imenu-alist (current-buffer))))
    (setq imtr:imenu-source-buffer (current-buffer))
    (when (get-buffer imtr:imenu-buffer-name)
      (kill-buffer imtr:imenu-buffer-name))
    (with-current-buffer (get-buffer-create imtr:imenu-buffer-name)
      ;; setup
      (progn
        (buffer-disable-undo)
        (setq mode-line-format '("%e")))
      (imtr:insert-buttons tree 0)
      (imtr:minor-mode 1)
      (setq buffer-read-only t))
    (pop-to-buffer imtr:imenu-buffer-name)))

(provide 'my-imenu-tree)

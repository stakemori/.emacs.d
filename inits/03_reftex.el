;; reftex bib path
;; (setq reftex-default-bibliography
;;       (list "/home/sho/Documents/TeX/texmf/bibtex/bib/texref.bib"
;;             (expand-file-name "~/texmf/bibtex/bib/texref.bib")))

;;Reftex
(my-add-hook reftex-select-label-mode-hook
  (define-key reftex-select-label-map (kbd "j") 'reftex-select-next)
  (define-key reftex-select-label-map (kbd "k") 'reftex-select-previous))
(my-add-hook reftex-select-bib-mode-hook
  (define-key reftex-select-bib-map (kbd "j") 'reftex-select-next)
  (define-key reftex-select-bib-map (kbd "k") 'reftex-select-previous))

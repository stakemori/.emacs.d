(require 'ispell)
(eval-when-compile (require 'cl))
(require 'my-utilities)

(defun my-ispell-word-internal (&optional following quietly continue region)
  (let ((cursor-location (point))       ; retain cursor location
        (word (ispell-get-word following))
        start end poss new-word replace ispell-filter message-log-max)
    ;; De-structure return word info list.
    (setq start (car (cdr word))
          end (car (cdr (cdr word)))
          word (car word))

    ;; At this point it used to ignore 2-letter words.
    ;; But that is silly; if the user asks for it, we should do it. - rms.
    (or quietly
        (message "Checking spelling of %s..."
                 (funcall ispell-format-word-function word)))
    (ispell-send-string "%\n")          ; put in verbose mode
    (ispell-send-string (concat "^" word "\n"))
    ;; wait until ispell has processed word
    (while (progn
             (ispell-accept-output)
             (not (string= "" (car ispell-filter)))))
    ;;(ispell-send-string "!\n") ;back to terse mode.
    (setq ispell-filter (cdr ispell-filter)) ; remove extra \n
    (if (and ispell-filter (listp ispell-filter))
        (if (> (length ispell-filter) 1)
            (error "Ispell and its process have different character maps")
          (setq poss (ispell-parse-output (car ispell-filter)))))
    (unless (or (eq poss t) (stringp poss) (null poss) ispell-check-only)
            (save-window-excursion
              (setq replace (ispell-command-loop
                             (car (cdr (cdr poss)))
                             (car (cdr (cdr (cdr poss))))
                             (car poss) start end)))
            (cond ((equal 0 replace)
                   (ispell-add-per-file-word-list (car poss)))
                  (replace
                   (setq new-word (if (atom replace) replace (car replace))
                         cursor-location (+ (- (length word) (- end start))
                                            cursor-location))
                   (if (not (equal new-word (car poss)))
                       (progn
                         (goto-char start)
                         ;; Insert first and then delete,
                         ;; to avoid collapsing markers before and after
                         ;; into a single place.
                         (insert new-word)
                         (delete-region (point) end)
                         ;; It is meaningless to preserve the cursor position
                         ;; inside a word that has changed.
                         (setq cursor-location (point))
                         (setq end (point))))
                   (if (not (atom replace)) ;recheck spelling of replacement
                       (progn
                         (if (car (cdr replace)) ; query replace requested
                             (save-window-excursion
                               (query-replace word new-word t)))
                         (goto-char start))))))))

(defun my-ispell-buffer ()
  (interactive)
  (push-mark)
  (unwind-protect
      (let (ispell-quit mark-ring)
        (cl-loop while (and (not ispell-quit)
                         (re-search-forward (rx (group (>= 2 alpha))) nil t))
              initially
              (goto-char (point-min))
              (save-excursion
                ;; Initialize variables and dicts alists
                (ispell-set-spellchecker-params)
                ;; use the correct dictionary
                (ispell-accept-buffer-local-defs))
              unless (my-ispell-skip-p)
              do
              (my-ispell-word-internal nil t)))
    (my-ispell-save-dict)))

(defun my-ispell-save-dict ()
  ;; keep if rechecking word and we keep choices win.
  (if (get-buffer ispell-choices-buffer)
      (kill-buffer ispell-choices-buffer))
  (ispell-pdict-save ispell-silently-savep)
  (message "Spell checking done."))

(defvar my-ispell-skip-alist
  '((face . ("font-lock-constant-face"
             "font-lock-function-name-face"
             "font-lock-keyword-face"
             "flyspell-duplicate"
             "font-latex-math-face"
             "my-latex-variable-face")))
  "car of each element is face, back, at, or func.
See the definition of `my-ispell-skip-p'")
(make-variable-buffer-local 'my-ispell-skip-alist)

(defun my-ispell-skip-p ()
  (save-match-data
    (awhen my-ispell-skip-alist
      (let ((face-name
             (save-excursion (forward-char -1) (symbol-name (face-at-point)))))
        (labels ((f (type e)
                    (case type
                      (face (string-match e face-name))
                      (back (looking-back e))
                      (at (looking-at e))
                      (func (funcall e)))))
          (cl-loop for (type . list) in it
                thereis
                (cl-loop for e in list thereis (f type e))))))))

(provide 'my-ispell-buffer)

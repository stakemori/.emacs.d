;; Copyright (C) 2012, 2013 Sho Takemori
;; Author: Sho Takemori
;; Keywords: python imenu
;; Created: 2013

;;; License
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(require 'anaphora)
(require 'cl-lib)

(defvar py-imenu-var-regexp (rx (any "_a-zA-Z")
                                (0+ (any "_a-zA-Z0-9"))))

(defun py-imenu:rx-to-string (form)
  (rx-to-string (cons 'and form) t))

(defun py-imenu-global-objs ()
  (cl-loop with res = nil
        initially (goto-char (point-min))
        while (re-search-forward
               (py-imenu:rx-to-string
                `(bol
                  (or (and (group (or "class" "def"))
                           (1+ space)
                           (group (regexp ,py-imenu-var-regexp)))
                      (and (group (regexp ,py-imenu-var-regexp))
                           (1+ space) "="
                           (not (any "="))))))
               nil t)
        do
        (setq res
              (cons
               (append
                (list :pos (line-beginning-position))
                (acond ((match-string-no-properties 1) ;def or class
                        (if (string= it "class")
                            (list :name (match-string-no-properties 2)
                                  :type 'class)
                          (list :name (match-string-no-properties 2)
                                :type 'def)))
                       ((match-string-no-properties 3)
                        (list :name it :type 'module-variable))))
               res))
        finally (return (nreverse res))))

(defvar py-imenu-show-methods-at-toplevel t)

(defun py-imenu-simple-create-index-function ()
  (if py-imenu-show-methods-at-toplevel
      (py-imenu-create-index-show-methods)
    (py-imenu-create-index-donot-show)))

(defun py-imenu-create-index-show-methods ()
  (cl-loop initially (goto-char (point-min))
           with classes = nil
           with mvars = nil
           with funs = nil
           with pigo = (py-imenu-global-objs)
           for pls on pigo
           for pl = (car pls)
           for type = (plist-get pl :type)
           for name = (plist-get pl :name)
           for pos  = (plist-get pl :pos)
           do
           (case type
             (class
              (goto-char pos)
              (let ((mthd-att (py-imenu--mthd-att
                               (aif (cdr pls)
                                   (plist-get (car it) :pos)))))
                (setq classes
                      (cons (cons name
                                  ;; Classes のなかにmethodを表示させない．
                                  ;; (cond (mthd-att
                                  ;;        (mapcar 'py-imenu--morph mthd-att))
                                  ;;       (t pos))
                                  pos)
                            classes))
                (setq funs (cons (cons (propertize name
                                                   'face
                                                   'mpy-class-name-face)
                                       pos) funs))
                (cl-loop for a in mthd-att
                         do (setq funs (cons (cons (concat
                                                    name " "
                                                    (plist-get a :name))
                                                   (plist-get a :pos))
                                             funs)))))
             (module-variable
              (push (cons name pos) mvars))
             (t (push (cons name pos) funs)))
           finally
           (return
            (let* ((mvars (nreverse mvars))
                   (classes (nreverse classes))
                   (l (delete
                       nil
                       (list (when mvars
                               (cons (propertize "ModuleVariable"
                                                 'face
                                                 'mpy-class-name-face)
                                     mvars))
                             (when classes
                               (cons (propertize "Class"
                                                 'face
                                                 'mpy-class-name-face)
                                     classes))))))
              (append l (nreverse funs))))))

(defface mpy-class-name-face
  '((t
     (:height 1.2 :inherit variable-pitch)))
  "Face for class name"
  :group 'mpy)

(defun py-imenu-create-index-donot-show ()
  (cl-loop initially (goto-char (point-min))
           with mvars = nil
           with funs = nil
           with pigo = (py-imenu-global-objs)
           for pls on pigo
           for pl = (car pls)
           for type = (plist-get pl :type)
           for name = (plist-get pl :name)
           for pos  = (plist-get pl :pos)
           do
           (case type
             (class
              (goto-char pos)
              (let ((mthd-att (py-imenu--mthd-att
                               (aif (cdr pls)
                                   (plist-get (car it) :pos)))))
                (setq funs
                      (cons (cons (propertize name
                                              'face 'mpy-class-name-face)
                                  (cond (mthd-att
                                         (mapcar 'py-imenu--morph mthd-att))
                                        (t pos)))
                            funs))))
             (module-variable
              (push (cons name pos) mvars))
             (t (push (cons name pos) funs)))
           finally
           (return (let* ((mvars (nreverse mvars))
                          (funs (nreverse funs)))
                     (append
                      (delete nil (list (when mvars
                                          (cons "ModuleVariable" mvars))))
                      funs)))))

(defun py-imenu--morph (plst)
  (cons (plist-get plst :name) (plist-get plst :pos)))

(defun py-imenu--mthd-att (bd)
  (cl-loop with res = nil
           with pos = nil
           with varrg =
           (py-imenu:rx-to-string
            `((group (or (and "def"
                              (1+ space)
                              (group (regexp ,py-imenu-var-regexp)))
                         (and (group (regexp ,py-imenu-var-regexp))
                              (1+ space) "=" (not (any "=")))))))
           with offset =
           (or (and (boundp 'python-indent-offset)
                    (integerp python-indent-offset)
                    python-indent-offset)
               4)
           with regexp =
           (rx-to-string `(and bol (repeat ,offset whitespace)
                               (regexp ,varrg)))
           while (re-search-forward regexp bd t)
           do
           (setq pos (line-beginning-position)
                 res (cons (acond
                            ((match-string-no-properties 2)
                             (list :pos pos
                                   :name it
                                   :type 'method))
                            ((match-string-no-properties 3)
                             (list :pos pos
                                   :name it
                                   :type 'attribute))) res))
           finally (return (nreverse res))))

(provide 'py-imenu)

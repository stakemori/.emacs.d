(require 'helm)
(require 'cl-lib)
(require 'dash)
(require 'f)
(require 's)
(require 'deferred)

(defvar hpm-base-direcory "~/Dropbox/Documents")
(defvar hpm-manscript-dirs '("notes" "ronbun"))
(defvar hpm-pdf-viewer "evince")

(defun hpm-pdf-manuscripts-in-dir (dir &optional relative?)
  (let ((ls (->> (shell-command-to-string (format "find %s -name \"*.pdf\"" dir))
              (s-split "\n")
              butlast)))
    (if relative?
        (-map (lambda (x) (f-relative x hpm-base-direcory)) ls)
      ls)))

(defun hpm-open-pdf (f)
  (call-process
   "/bin/sh" nil 0 nil "-c"
   (format "%s %s"
           hpm-pdf-viewer
           (shell-quote-argument
            (f-join hpm-base-direcory f)))))

(defvar hpm-data-cached nil)

(defun hpm-data ()
  (or hpm-data-cached
      (setq hpm-data-cached
            (sort (cl-loop for d in hpm-manscript-dirs
                           for pt = (f-join hpm-base-direcory d)
                           append (hpm-pdf-manuscripts-in-dir pt t))
                  'string<))))

(defvar helm-c-source-pdf-manuscripts
  (helm-build-in-buffer-source "Pdf Manuscripts"
    :data 'hpm-data
    :action
    '(("Open" . hpm-open-pdf)
      ("Copy file name" . (lambda (can)
                            (kill-new
                             (->> can
                                  (s-replace " " "\\ ")
                                  (f-join hpm-base-direcory))))))))

(defun helm-pdf-manuscripts-clear-cache ()
  (interactive)
  (setq hpm-data-cached nil))


(defun helm-pdf-manuscripts ()
  (interactive)
  (helm-pdf-manuscripts-clear-cache)
  (helm :sources '(helm-c-source-pdf-manuscripts)))

(provide 'helm-pdf-manuscripts)

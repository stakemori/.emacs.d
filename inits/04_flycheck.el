;; -*- lexical-binding: t -*-
(eval-when-compile (require 'flycheck))

(defmacro my:flycheck-set-checker (mode checker)
  `(my-add-hook0 ,(intern (concat (symbol-name mode) "-hook")) "flycheck"
     (setq flycheck-checker ,checker)))
(my-named-add-hook emacs-lisp-mode-hook
  "flycheck-elisp"
  (when (file-exists-p "Cask")
    (flycheck-mode 1)
    (my:flycheck-set-checker emacs-lisp-mode 'emacs-lisp-cask)))

(with-eval-after-load "flycheck"
  ;; (flycheck-add-next-checker 'python-pylint 'python-flake8)
  ;; (setq flycheck-flake8-maximum-line-length 100)
  (dolist (ckr '(python-pylint python-flake8))
    (flycheck-add-mode ckr 'sage-shell:sage-mode))
  (my:flycheck-set-checker emacs-lisp-mode 'emacs-lisp)
  ;; Disable checkdoc
  (put 'emacs-lisp 'flycheck-next-checkers nil)
  (setq-default flycheck-check-syntax-automatically '(save mode-enabled))

  (require 'flycheck-package)
  (flycheck-package-setup)
  (setq package-lint--sane-prefixes
        (rx
         string-start
         (or
          "org-dblock-write:"
          "org-babel-execute:"
          "org-babel-default-header-args:"
          "org-babel-header-args:"
          "sage-shell:"
          "sage-shell-" (1+ alnum) ":")))
  (defun package-lint--check-keywords-list ()
    nil)
  (flycheck-define-checker emacs-lisp-cask
    "Checker for Cask"
    :command ("~/.cask/bin/cask" "build")
    :error-patterns
    ((error line-start (file-name) ":" line ":" column ":Error:"
            (message (zero-or-more not-newline)
                     (zero-or-more "\n    " (zero-or-more not-newline)))
            line-end)
     (warning line-start (file-name) ":" line ":" column ":Warning:"
              (message (zero-or-more not-newline)
                       (zero-or-more "\n    " (zero-or-more not-newline)))
              line-end))
    :error-filter
    (lambda (errors)
      (flycheck-collapse-error-message-whitespace
       (flycheck-sanitize-errors errors)))
    :modes (emacs-lisp-mode)
    :predicate
    (lambda ()
      (and
       (buffer-file-name)
       (file-exists-p "Cask")
       (not (bound-and-true-p no-byte-compile))
       (not (flycheck-autoloads-file-p))))
    :next-checkers (emacs-lisp-package)))

(smartrep-define-key global-map (kbd "C-q")
  '(("C-n" . flycheck-next-error)
    ("C-p" . flycheck-previous-error)))

;; (my:flycheck-set-checker python-mode 'python-pylint)

;; (defun sage-shell:flycheck-turn-on ()
;;   "Enable flycheck-mode only in a file ended with py."
;;   (when (let ((bfn (buffer-file-name)))
;;           (and bfn (string-match (rx ".py" eol) bfn)
;;                (not (string-match (rx "/sage/") bfn))))
;;     (flycheck-mode 1)))
;; (add-hook 'python-mode-hook 'sage-shell:flycheck-turn-on)

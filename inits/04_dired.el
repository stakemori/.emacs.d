;; dired関連
(global-set-key (kbd "C-x C-j") 'skk-mode)
(global-set-key (kbd "C-x C-d") 'dired-jump)

(setq dired-dwim-target t)
(setq dired-auto-revert-buffer t)
(setq dired-omit-verbose nil)
(setq dired-omit-files (rx (or (and bol "." (not (any "."))
                                    (0+ (not (any "/")))
                                    eol))))
(require 'dired)
(require 'dired-x)
;; (defun dired-default-directory-in-side-window ()
;;   "Display `default-directory' in side window on left, hiding details."
;;   (interactive)
;;   (when (and (not (window-minibuffer-p))
;;              (not (eq major-mode 'dired-mode))
;;              (not (string-match-p "magit" (symbol-name major-mode)))
;;              (not (string-match-p (rx "COMMIT_EDITMSG")
;;                                   (buffer-name))))
;;     (let* ((parameters '(window-parameters . ((no-other-window . t) (no-delete-other-window . t))))
;;            (recentf-list nil)
;;            (buffer (dired-noselect default-directory)))
;;       (display-buffer-in-side-window
;;        buffer `((side . right) (slot . 0)
;;                 (window-width . 25)
;;                 (preserve-size . (t . nil)) ,parameters)))))
;; (my-named-add-hook kill-emacs-hook
;;   dired-kill
;;   (dolist (b (buffer-list))
;;     (when (eq (buffer-local-value 'major-mode b) 'dired-mode)
;;       (kill-buffer b))))
;; (setq my-dired-timer
;;       (run-with-idle-timer 1 t #'dired-default-directory-in-side-window))

(with-eval-after-load "dired-x"
  (setq dired-omit-extensions
        (append dired-tex-unclean-extensions dired-omit-extensions)))

(my-add-hook dired-mode-hook
  (setq-local mode-line-format
              '("  %e " mode-line-buffer-identification))
  ;; (require 'dired-filter)
  ;; (dired-filter-group-mode 1)
  (dired-hide-details-mode)
  (define-key dired-mode-map (kbd "K") 'dired-k)
  (define-key dired-mode-map (kbd "i") 'dired-up-directory)
  ;; (require 'dired-x)

  ;; (require 'dired-aux)
  ;; ;; dired-omit-modeでドットファイルなどの表示をトグル
  ;; Wdired
  (require 'wdired)
  (define-key dired-mode-map "r" 'wdired-change-to-wdired-mode)
  (my-define-keys wdired-mode-map
    "C-j" 'wdired-next-line
    "C-p" 'kill-line
    "C-k" 'wdired-previous-line
    "C-n" 'delete-backward-char)
  (my-define-keys dired-mode-map
    "C-t"  'other-window-or-split
    "O"    'dired-do-moccur
    "T"    'dired-do-convert-coding-system
    "V"    'my-dired-view-marked-files
    "F"    'my-dired-find-marked-files
    "h"    'dired-omit-mode
    "j"    'dired-next-line
    "k"    'dired-previous-line
    "n"    'dired-next-line
    "p"    'dired-previous-line
    "C-p"  'dired-kill-line
    "M-s"  nil
    "M-o"  nil
    "W"    'browse-url-of-dired-file
    )
  ;; (dired-collapse-mode 1)
  )
(add-hook 'dired-load-hook (lambda () (load "dired-x")))

(setq dired-filter-group-saved-groups '(("default"
                                         ("Directory"
                                          (directory))
                                         ("Py"
                                          (extension "py" "sage" "pyx"))
                                         ("C"
                                          (extension "c" "h"))
                                         ("LaTeX"
                                          (extension "tex" "bib" "sty"))
                                         ("Markdown"
                                          (extension  "org" "md"))
                                         ("PDF"
                                          (extension . "pdf"))
                                         ("html"
                                          (extension . "html"))
                                         ("Image"
                                          (extension "png" "jpg" "gif"))
                                         ("elisp"
                                          (extension . "el"))
                                         ("Archives"
                                          (extension "zip" "rar" "gz" "bz2" "tar"))
                                         ("binary"
                                          (extension "o" "sobj"))
                                         ("Dot directory"
                                          (dot-dir))
                                         ("Dot files"
                                          (dot-files-only)))))
(setq-default dired-filter-stack nil)

(with-eval-after-load "dired-filter"
  (dired-filter-define dot-files-only "dot files only"
    (:description "dot-files-only")
    (and
     (string-match-p "^\\." file-name)
     (looking-at "^[* ] -")))

  (dired-filter-define dot-dir "dot dir"
    (:description "dot-dir")
    (and
     (string-match-p "^\\." file-name)
     (looking-at dired-re-dir))))

;; (my-named-add-hook dired-mode-hook
;;   all-the-icon
;;   (font-lock-mode -1)
;;   (all-the-icons-dired-mode 1))

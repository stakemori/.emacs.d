;; ;; tree-undo
;; (require 'undo-tree)
;; (global-set-key (kbd "C-x u") 'undo-tree-visualize)
;; (define-key undo-tree-visualizer-mode-map "k"
;;    'undo-tree-visualize-undo)
;;  (define-key undo-tree-visualizer-mode-map "\C-k"
;;    'undo-tree-visualize-undo)
;;  (define-key undo-tree-visualizer-mode-map [down]
;;    'undo-tree-visualize-redo)
;;  (define-key undo-tree-visualizer-mode-map "j"
;;    'undo-tree-visualize-redo)
;;  (define-key undo-tree-visualizer-mode-map "\C-j"
;;    'undo-tree-visualize-redo)
;;  ;; horizontal motion keys switch branch
;;  (define-key undo-tree-visualizer-mode-map [remap forward-char]
;;    'undo-tree-visualize-switch-branch-right)
;;  (define-key undo-tree-visualizer-mode-map [remap backward-char]
;;    'undo-tree-visualize-switch-branch-left)
;;  (define-key undo-tree-visualizer-mode-map [right]
;;    'undo-tree-visualize-switch-branch-right)
;;  (define-key undo-tree-visualizer-mode-map "l"
;;    'undo-tree-visualize-switch-branch-right)
;;  (define-key undo-tree-visualizer-mode-map "\C-l"
;;    'undo-tree-visualize-switch-branch-right)
;;  (define-key undo-tree-visualizer-mode-map [left]
;;    'undo-tree-visualize-switch-branch-left)
;;  (define-key undo-tree-visualizer-mode-map "h"
;;     'undo-tree-visualize-switch-branch-left)

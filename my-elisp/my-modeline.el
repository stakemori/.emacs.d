;; Based on a bolg post: http://amitp.blogspot.jp/2011/08/emacs-custom-mode-line.html

(require 'dash)
(eval-when-compile (require 'cl))
(require 'anaphora)
(require 'pcase)
(require 'let-alist)

(defmacro mode-line--memoize (sym &rest hooks)
  (let ((memo-var (intern (concat (symbol-name sym) "-memo-var")))
        (unmemo-fn (intern (concat (symbol-name sym) "-unmemo")))
        (set-func (intern (concat (symbol-name sym) "-setfunc")))
        (memo-p (intern (concat (symbol-name sym) "-memo-p"))))
    (append
     '(progn)
     `((defvar ,unmemo-fn (symbol-function (quote ,sym)))
       (defvar ,memo-var nil)
       (defvar ,memo-p t)
       (make-variable-buffer-local (quote ,memo-p))
       (make-variable-buffer-local (quote ,memo-var))
       (defun ,set-func ()
         (setq ,memo-var (funcall ,unmemo-fn)))
       (,set-func)
       (with-no-warnings
         (defun ,sym ()
           (if ,memo-p
               (or ,memo-var (funcall ,unmemo-fn))
             (funcall ,unmemo-fn)))))
     (-map (lambda (s) `(add-hook ',s ',set-func)) hooks))))

(setq-default
 mode-line-position
 '("  "
   (:propertize "L: %l" face mode-line-position-face)
   ;; (:propertize ", " face mode-line-delim-face-1)
   (:propertize "/" face mode-line-delim-face-1)
   (:eval
    (format "%s, "
            (number-to-string (count-lines (point-min) (point-max)))))
   (:eval (propertize "C: %c" 'face 'mode-line-position-face))))

(defvar mode-line--right-items-plists
  '((:sexp (:propertize mode-name face mode-line-mode-face))
    (:sexp (:eval (if (or mode-line-process
                          (not (string= "" (mode-line--coding-system-string))))
                      " "
                    "")))
    (:sexp (:propertize mode-line-process face mode-line-process-face))
    (:sexp (:eval (if mode-line-process " ")))
    (:sexp (:eval (mode-line--coding-system-string)))))

(defun mode-line--ro-bfm ()
  (cond (buffer-read-only
         (concat " %%%% "))
        ((string-match-p (rx bol "*") (buffer-name)) "    ")
        (t " %*%* ")))

(defvar mode-line--centering-buffer-name-p t)

(defun my:flycheck-mode-line-status-text (&optional status)
  (when (and (featurep 'flycheck)
             (bound-and-true-p flycheck-mode))
    (let ((text (pcase (or status (bound-and-true-p flycheck-last-status-change))
                  (`not-checked "")
                  (`no-checker "-")
                  (`running "*")
                  (`errored "!")
                  (`finished
                   (if (bound-and-true-p flycheck-current-errors)
                       (let-alist (flycheck-count-errors flycheck-current-errors)
                         (format "%s/%s" (or .error 0) (or .warning
                                                           0)))
                     ""))
                  (`interrupted "-")
                  (`suspicious "?"))))
      text)))

(setq-default mode-line-format
              `("%e"
                (eldoc-mode-line-string
                 (" " eldoc-mode-line-string " "))
                (:eval (mode-line--ro-bfm))
                (:eval (my:flycheck-mode-line-status-text))
                " "
                ;; "%["
                ;; (:propertize "%b" face mode-line-filename-face)
                ;; "%]"
                mode-line-buffer-identification
                (:eval (or my/mode-line--vc-mode-name-cache
                           (mode-line--vc-mode-name)))
                (:eval (format "[%s]" (projectile-project-name)))
                "%n"
                ;; (global-mode-string global-mode-string)
                ;; (:eval (mode-line--shorten-directory))
                ;; (vc-mode vc-mode)
                ;; (:propertize mode-line-process face mode-line-process-face)
                ;; "  "
                ;; ;; center
                ,(when mode-line--centering-buffer-name-p '(:eval (mode-line--center-fill)))
                mode-line-position
                " "
                ;; mode-line-misc-info
                ;; fill to right
                (:eval (mode-line--make-fill-to-right))
                ,@(-map (lambda (pl) (plist-get pl :sexp))
                        mode-line--right-items-plists)))

(defvar my/mode-line--vc-mode-name-cache nil)
(make-variable-buffer-local 'my/mode-line--vc-mode-name-cache)

(defun mode-line--vc-mode-name ()
  (setq my/mode-line--vc-mode-name-cache
        (cond ((and vc-mode (string-match "^ Git" vc-mode))
               (concat " "
                       (if (fboundp 'magit-get-current-branch)
                           (magit-get-current-branch)
                         (substring vc-mode 5))))
              (t vc-mode))))

(defun mode-line--vc-mode-name-update ()
  (unless (and (fboundp 'helm-alive-p)
               (helm-alive-p))
    (walk-windows
     (lambda (win)
       (with-current-buffer (window-buffer win)
         (when (derived-mode-p major-mode 'prog-mode)
           (mode-line--vc-mode-name)))))))

(add-hook 'window-configuration-change-hook
          'mode-line--vc-mode-name)

(defun mode-line--center-fill ()
  (propertize
   " "
   'display
   `((space :align-to (- center ,(/ (length (buffer-name)) 2))))))

(when mode-line--centering-buffer-name-p
  (with-no-warnings
    (mode-line--memoize mode-line--center-fill
                        after-save-hook
                        window-configuration-change-hook)))

(defun mode-line--coding-system-string ()
  (let* ((cs buffer-file-coding-system)
         (name (->> (acase (plist-get (coding-system-plist cs) :name)
                      ('undecided nil)
                      (t (format "%S" it)))
                 mode-line--cs-name-convert))
         (eol-type1 (coding-system-eol-type cs))
         (eol-type (awhen (and (integerp eol-type1)
                               (case eol-type1
                                 (0 nil)
                                 (1 "dos")
                                 (2 "mac")))
                     (format "%s" it))))
    (cond ((and eol-type name (not (string= eol-type "")))
           (concat name (format " %s" eol-type)))
          (eol-type (format "%s" eol-type))
          (name name)
          (t ""))))

(defvar mode-line--cs-name-convert-alst
  '(("japanese-iso-8bit" . "euc-jp")
    ("prefer-utf-8" . "")
    ("japanese-shift-jis" . "sjis")))

(defun mode-line--cs-name-convert (cs-name)
  (or (assoc-default cs-name mode-line--cs-name-convert-alst) cs-name))

(defun mode-line--make-fill-to-right ()
  (if (eq 'right (get-scroll-bar-mode))
      (propertize " " 'display
                  `((space :align-to (- right-fringe
                                        ,(-
                                          (->>
                                           (-map (lambda (pl) (plist-get pl :sexp))
                                                 mode-line--right-items-plists)
                                           (--map (length (format-mode-line it)))
                                           -sum)
                                          2)))))
    (propertize " " 'display
                `((space :align-to (- right-fringe ,(->>
                                                     (-map (lambda (pl) (plist-get pl :sexp))
                                                           mode-line--right-items-plists)
                                                     (--map (length (format-mode-line it)))
                                                     -sum)))))))

(make-face 'mode-line-small-face)
(set-face-attribute 'mode-line-small-face nil :height 0.9)

(cl-defun mode-line--shorten-directory (&optional (dir default-directory) (max-length 30))
  "Show up to `max-length' characters of a directory name `dir'."
  (propertize
   (let ((path (reverse (split-string (abbreviate-file-name dir) "/")))
         (output ""))
     (when (and path (equal "" (car path)))
       (setq path (cdr path)))
     (while (and path (< (length output) (- max-length 4)))
       (setq output (concat (car path) "/" output))
       (setq path (cdr path)))
     (when path
       (setq output (concat ".../" output)))
     output)
   'face 'mode-line-small-face))

;; (mode-line--memoize mode-line--shorten-directory
;;                    after-save-hook
;;                    window-configuration-change-hook)

(mode-line--memoize mode-line--coding-system-string
                    after-save-hook
                    window-configuration-change-hook)

;; re-calculate right fill length after changing coding system.
(defadvice set-buffer-file-coding-system (around calc-right-fill activate)
  ad-do-it
  (mode-line--coding-system-string-setfunc))

;; (set-face-attribute 'mode-line nil
;;                     :foreground "gray80" :background "gray10"
;;                     :inverse-video nil
;;                     :weight 'normal
;;                     ;; :height 120
;;                     ;; :box '(:line-width 2 :color "gray10" :style nil)
;;                     :box nil)

;; (set-face-attribute 'mode-line-inactive nil
;;                     :foreground "gray80" :background "gray30"
;;                     :inverse-video nil
;;                     :weight 'extra-light
;;                     ;; :height 120
;;                     ;; :box '(:line-width 2 :color "gray30" :style nil)
;;                     :box nil)


;; Extra mode line faces
(make-face 'mode-line-modified-face)
(make-face 'mode-line-folder-face)
(make-face 'mode-line-filename-face)
(make-face 'mode-line-position-face)
(make-face 'mode-line-mode-face)
(make-face 'mode-line-process-face)
(make-face 'mode-line-delim-face-1)


(set-face-attribute 'mode-line-position-face nil
                    :inherit 'mode-line-face)
(set-face-attribute 'mode-line-mode-face nil
                    :inherit 'mode-line-face
                    :foreground nil)

(face-spec-set 'mode-line-delim-face-1
 '((t (:inherit mode-line-face))))

(set-face-attribute 'mode-line-filename-face nil :inherit 'mode-line-face)


(provide 'my-modeline)

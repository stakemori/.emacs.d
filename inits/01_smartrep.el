;; -*- lexical-binding: t -*-

;; キーの連続入力を簡単にする．
(require 'smartrep)
(global-unset-key (kbd "C-x C-p"))
(require 'page-ext)
(smartrep-define-key global-map (kbd "C-x C-p")
    '(("k" . 'previous-page)
      ("j" . 'next-page)))

(my/lazy-progn
 ;;最後の変更箇所にジャンプ
 (require 'goto-chg)
 (smartrep-define-key global-map "C-q"
   '(("C-u" . 'goto-last-change)
     ("C-M-u" . 'goto-last-change-reverse)))
 (setq glc-default-span 2))

;;; PythonDotElGallina, PythonDotEl or PythonModeDotEl
;; (autoload 'python-mode (locate-user-emacs-file "elisp/python.el") nil t)
;; (defvar my-python-mode 'PythonModeDotEl)
(require 'cl-lib)
(defvar my-python-mode 'PythonDotEl)
(setq python-indent-guess-indent-offset nil)
(cl-case my-python-mode
  ('PythonDotElGallina
   (autoload 'python-mode "python-gallina" nil t))
  ('PythonModeDotEl
   (autoload 'python-mode "python-mode" nil t)))

(setq auto-mode-alist (cons '("\\.py$" . python-mode) auto-mode-alist))

(my-eval-after-load "python-mode"
  ;; inferior-python-modeで同フォルダのモジュールをよみこめるようにする
  (setq python-remove-cwd-from-path nil)
  (setq interpreter-mode-alist (cons '("python" . python-mode)
                                     interpreter-mode-alist))

  ;; python debug
  (defun my-gud-query-cmdline (minor-mode)
    (let* ((hist-sym (gud-symbol 'history nil minor-mode))
           (cmd-name (gud-val 'command-name minor-mode)))
      (unless (boundp hist-sym) (set hist-sym nil))
      (read-from-minibuffer
       (format "Run %s (like this): " minor-mode)
       (concat (or cmd-name (symbol-name minor-mode))
               " "
               (when (buffer-file-name)
                 (file-name-nondirectory (buffer-file-name))))
       gud-minibuffer-local-map nil
       hist-sym)))
  (defadvice pdb (before gud-query-cmdline activate)
    "Provide a better default command line when called interactively."
    (interactive
     (list (my-gud-query-cmdline 'pdb))))


  (defun ac-py-candidates ()
    (let* ((pattern (py-symbol-near-point))
           (imports (py-find-global-imports)))
      (pycomplete-get-all-completions pattern imports)))

  ;; ipython
  (defun ipython (&optional argprompt)
    "Start an interactive Ipython interpreter in another window.
   With optional \\[universal-argument] user is prompted
    for options to pass to the Ipython interpreter. "
    (interactive)
    (let ((py-shell-name "ipython")
          (py-python-command-args (append py-python-command-args
                                          (list "-noautoindent"))))
      (py-shell argprompt)))
  ;; (setq py-python-command "python")
  ;; (require 'ipython)
  ;; (defun py-execute-buffer-ipython ()
  ;;   (interactive)
  ;;   (py-execute-region-ipython (point-max) (point-min)))
  ;; (setq ipython-indenting-buffer-name
  ;;       (concat " " ipython-indenting-buffer-name))
  )

(defun python-mode-initialize ()
  (setq fill-column 86)
  (setq tab-width 4)
  (font-lock-add-keywords
   nil
   `((,(rx "^")
      (0 font-lock-keyword-face t))))
  ;; (require 'py-imenu)
  (setq forward-sexp-function nil)
  ;; (setq imenu-create-index-function 'py-imenu-simple-create-index-function)
  ;; python check
  (cond
   ((string-match "test" (buffer-name))
    (set (make-local-variable 'python-check-command) "pyflakes")))

  (my-python-key-define)
  (electric-indent-local-mode -1)

  (setq flycheck-python-flake8-executable "python3")
  (setq flycheck-flake8rc "~/.config/flake8")
  (setq flycheck-checker 'python-flake8)
  (flycheck-mode 1)

  ;; (unless (eq major-mode 'cython-mode)
  ;;   (py-autopep8-enable-on-save))
  ;; ;; kill-ringについてのひどいbug
  ;; (defun my-py-autopep8-kill-ring (fun &rest _args)
  ;;   (let ((kill-ring nil))
  ;;     (funcall fun)))
  ;; (advice-add 'py-autopep8-buffer :around 'my-py-autopep8-kill-ring)
  ;; (advice-add 'py-isort-buffer :around 'my-py-autopep8-kill-ring)
  ;; (setq py-autopep8-options '("--max-line-length=100"))
  (auto-highlight-symbol-mode 1))

(defun my-python-key-define ()
  (define-key python-mode-map (kbd "C-j") 'next-line)
  (my-define-keys python-mode-map
    "C-j" 'next-line
    "'" 'skeleton-pair-insert-maybe
    "C-c C-c" 'python-shell-send-buffer
    "C-c C-v" 'python-check)
  (cl-case my-python-mode
    (PythonModeDotEl
     (my-define-keys python-mode-map
       "C-v"      'py-newline-and-indent
       "'"        'skeleton-pair-insert-maybe
       "C-c C-m"  'py-mark-block
       "'"        'skeleton-pair-insert-maybe
       "RET"      'newline-and-indent
       "C-c C-v"  'py-pychecker-run
       "C-M-h"    nil)
     (smartrep-define-key python-mode-map (kbd "C-q")
       '(("C-h" . 'py-shift-left)
         ("C-l" . 'py-shift-right)
         ("C-j" . 'py-forward-block-or-clause)
         ("C-k" . 'py-previous-block))))
    (t
     (smartrep-define-key python-mode-map (kbd "C-q")
       '(("C-h" . 'python-indent-shift-left)
         ("C-l" . 'python-indent-shift-right)
         ("C-j" . 'python-next-statement)
         ("C-k" . 'python-previous-statement)))
     (my-define-keys python-mode-map
       "C-c C-m"  'python-mark-block
       "RET"      'newline
       "C-n"      (lookup-key python-mode-map (kbd "DEL")))))

  (defun mpy-skip-triple-quotes ()
    (interactive)
    (when (re-search-forward (rx (or "'''" "\"\"\"")) nil t)
      (push-mark)
      (let ((mark-ring nil))
        (re-search-forward (match-string 0) nil t))))
  (defun mpy-skip-triple-quotes-backword ()
    (interactive)
    (when (re-search-backward (rx (or "'''" "\"\"\"")) nil t)
      (push-mark)
      (let ((mark-ring nil))
        (re-search-backward (match-string 0) nil t))))

  (defun mpy-beginning-of-class ()
    (interactive)
    (push-mark)
    (re-search-backward "^class\\> ") nil t)

  (define-key python-mode-map (kbd "C-c j") 'mpy-skip-triple-quotes)
  (define-key python-mode-map (kbd "C-c k") 'mpy-skip-triple-quotes-backword)
  (define-key python-mode-map (kbd "C-c C-a") 'mpy-beginning-of-class))

(add-hook 'python-mode-hook 'python-mode-initialize t)
;; (my-eval-after-load "python"
;;   (my-python-key-define)
;;   (add-hook 'python-mode-hook 'python-mode-initialize t))
;; (my-eval-after-load "python-mode"
;;   (my-python-key-define)
;;   (add-hook 'python-mode-hook 'python-mode-initialize t))
;; python checker
(setq python-check-command "flake8")
(advice-add 'python-check :after
            (lambda (cmd) (setq python-check-custom-command nil)))

;; (setq py-pychecker-command "pylint")
;; (setq py-pychecker-command-args
;;       '("-rn" "--msg-template='{path},{line:4d},{column:2d}: [{msg_id} ({symbol})] {msg}'"))

(setq my-pylint-compilation-alst
      (let ((base (concat
                   (rx bol
                       (group (1+ nonl))
                       ","
                       (0+ space)
                       (group (1+ num))
                       ","
                       (0+ space)
                       (group (1+ num))
                       ": [") "%s")))
        `((,(format base "[FE]") 1 2 3)
          (,(format base "[RWC]") 1 2 3 1))))

(with-eval-after-load "python"
  (setq compilation-error-regexp-alist
        (append my-pylint-compilation-alst
                compilation-error-regexp-alist))
  (pyvenv-tracking-mode 1))

;;; cython
(add-to-list 'auto-mode-alist '("\\.pyx\\'" . cython-mode))
(add-to-list 'auto-mode-alist '("\\.pxd\\'" . cython-mode))
(add-to-list 'auto-mode-alist '("\\.pxi\\'" . cython-mode))
(autoload 'cython-mode "cython-mode" nil t)
(my-eval-after-load "cython-mode"
  (define-key cython-mode-map (kbd "C-j") 'next-line))
;; (my-add-hook cython-mode-hook (delete 'ac-source-ropemacs ac-sources))

(setq elpy-eldoc-show-current-function nil)

(my-named-add-hook python-mode-hook "company"
                   ;; errorがおきるので，anaconda_mode.pyのprocess_definitionsの
                   ;; definition.docstring()をNoneにした．
                   (anaconda-mode 1)
                   (setq-local company-backends
                               '((company-anaconda :with company-dabbrev-code)))
                   (eldoc-mode -1))
(with-eval-after-load 'company-anaconda
  (fset 'company-anaconda-prefix #'company-grab-symbol))
(setq python-shell-interpreter "python3")

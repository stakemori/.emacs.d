;; ========================複数動作=============================
(require 'sequential-command)
;; ポイント前の単語を大文字->キャピタライズ->小文字
(define-sequential-command seq-kyr-backward-word
  seq-upcase-backward-word2
  seq-upcase-backward-word1
  seq-upcase-backward-word3)
(defun seq-upcase-backward-word1 ()
  (interactive)
  (upcase-word (- 1)))
(defun seq-upcase-backward-word2 ()
  (interactive)
  (capitalize-word (- 1)))
(defun seq-upcase-backward-word3 ()
  (interactive)
  (downcase-word (- 1)))
(global-set-key (kbd "M-u") 'seq-kyr-backward-word)
;; 行頭 = >インデントの行頭
(define-sequential-command seq-home-or-back-to-indentation
 back-to-indentation beginning-of-line)
(global-set-key (kbd "C-a") 'seq-home-or-back-to-indentation)

(defun my:end-of-line-no-comment ()
  (interactive)
  (end-of-line)
  (when (and (nth 4 (syntax-ppss))
             comment-start)
    (beginning-of-line)
    (re-search-forward (substring comment-start 0 1) (line-end-position) t)
    (forward-char -1)
    (skip-syntax-backward "-")))

(define-sequential-command my:end-of-line
  end-of-line
  my:end-of-line-no-comment)

(global-set-key (kbd "C-e") 'my:end-of-line)

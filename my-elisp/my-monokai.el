;; (setq monokai-distinct-fringe-background t)

(defface my:font-lock-def-face
  '((t (:foreground
        "#66D9EF"
        :italic nil))) "")
(defvar my:font-lock-def-face 'my:font-lock-def-face)

(defvar my:font-lock-def-alst
  '((python-mode . ("def"))
    (sage-shell:sage-mode . ("def"))
    (emacs-lisp-mode . ("defun" "defvar" "defmacro" "defun*" "cl-defun"
                        "defconst" "defadvice"))
    (clojure-mode . ("defn" "defn-" "def" "defonce"
                     "defmulti" "defmethod" "defmacro"
                     "defstruct" "deftype" "defprotocol"
                     "defrecord" "deftest" "def\\[a-z\\]"))))

;; class functionname for python-mode.
(let ((spec `(,(rx symbol-start (group "class") (1+ space)
                   (group (1+ (or word ?_))))
              (1 my:font-lock-def-face) (2 font-lock-function-name-face))))
  (dolist (mode '(python-mode sage-shell:sage-mode))
    (font-lock-add-keywords mode (list spec))))

(cl-loop for (mode . lst) in my:font-lock-def-alst
      do
      (font-lock-add-keywords
       mode
       `((,(concat "\\_<" (regexp-opt lst) "\\_>")
          (0 my:font-lock-def-face)))))

(load-theme 'monokai t)

(defun my-monokai-override ()
  (face-spec-set 'font-lock-builtin-face
                 '((t (:foreground "#66D9EF"))))
  (face-spec-set 'font-lock-doc-face
                 '((t (:foreground "#E6DB74"))))
  (face-spec-set 'font-lock-preprocessor-face
                 '((t (:foreground "#C2A1FF"))))
  (face-spec-set 'flymake-errline
                 '((t (:underline "PaleVioletRed1" :background nil))))
  (face-spec-set 'flyspell-incorrect
                 '((((class color) (background dark))
                    (:foreground "white" :bold t :underline t))
                   (t (:foreground "tomato2" :bold t :underline t))))
  (face-spec-set 'font-latex-math-face
                 '((((class color) (background dark))
                    ( :foreground "wheat"))
                   (t ( :foreground "saddle brown"))))
  (setq font-latex-fontify-script t)
  (setq font-latex-script-display '(nil . nil))
  (face-spec-set 'font-latex-subscript-face
                 '((((class color) (background dark))
                    (:background "dark slate gray" :height 1.0))
                   (t (:background "light cyan" :height 1.0))))
  (face-spec-set 'font-latex-superscript-face
                 '((((class color) (background dark))
                    (:background "MediumPurple4" :height 1.0))
                   (t (:background "seashell1" :height 1.0))))
  (face-spec-set 'font-latex-verbatim-face
                 '((t (:inherit nil))))
  ;; (face-spec-set 'font-latex-sectioning-5-face
  ;;                `((((class color) (background dark))
  ;;                   (:foreground "azure2" :box nil :font ,ricty-font))))
  ;; (dolist (f '(rainbow-delimiters-depth-2-face
  ;;              rainbow-delimiters-depth-6-face
  ;;              rainbow-delimiters-depth-10-face))
  ;;   (face-spec-set f '((t (:foreground "#C2A1FF")))))
  )

(my-monokai-override)

(defadvice create-monokai-theme (after override activate)
  (my-monokai-override))

(provide 'my-monokai)
;; Local Variables:
;; no-byte-compile: t
;; End:

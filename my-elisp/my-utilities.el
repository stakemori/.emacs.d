(require 'package)
(require 'epl)
(require 'dash)
(require 'dash-functional)
(require 's)
(require 'shell)
(require 'deferred)
(eval-and-compile (require 'cl-lib))
(defvar *my-gensym-counter* 0)
(defun my-gensym (&optional prefix)
  "Generate a new uninterned symbol.
The name is made by appending a number to PREFIX, default \"my-gen\"."
  (let ((pfix (if (stringp prefix) prefix "my-gen"))
        (num (if (integerp prefix) prefix
               (prog1 *my-gensym-counter*
                 (setq *my-gensym-counter* (1+ *my-gensym-counter*))))))
    (make-symbol (format "%s%d" pfix num))))

(defun my-gentemp (&optional prefix)
  "Generate a new interned symbol with a unique name.
The name is made by appending a number to PREFIX, default \"my-gen\"."
  (let ((pfix (if (stringp prefix) prefix "my-gen"))
        name)
    (while (intern-soft (setq name (format "%s%d" pfix *my-gensym-counter*)))
      (setq *my-gensym-counter* (1+ *my-gensym-counter*)))
    (intern name)))

;;;###autoload
(defmacro append-elements (origseq  &rest elmts)
  (declare (indent 1))
  "Add ELMTS to the tail of ORIGSEQ and set ORIGSEQ the result. "
  `(set ,origseq (append (symbol-value ,origseq) (list ,@elmts))))

;;;###autoload
(defmacro macexpand (expr)
  `(let ((print-quoted t))
     (cl-prettyprint (macroexpand ',expr))))


;;;###autoload
(defmacro ansetq (&rest rest)
  "Anaphoric setq. REST is a list of sym val sym1 val1... `it' is the value of last sym"
  (cond ((eq (length rest) 2) `(let ((it ,(car rest)))
                                 (setq ,(nth 0 rest) ,(nth 1 rest))))
        ((> (length rest) 3) `(let ((it ,(car rest)))
                                (setq ,(nth 0 rest) ,(nth 1 rest))
                                (ansetq ,@(nthcdr 2 rest))))))
;;;###autoload
(defun group (l &optional n)
  (let* ((n (or n 2))
         (m (/ (length l) n)))
    (cond
     ((equal m 0) (list l))
     (t (append
         (cl-loop for i from 0 to (1- m)
               collect
               (cl-loop for j from 0 to (1- n)
                     collect
                     (nth (+ (* i n) j) l)))
         (if (nthcdr (* m n) l)
             (list (nthcdr (* m n) l))))))))

;; (defmacro carring (funct &rest args)
;;   "カリー化をする．lambda式を返し_は通常の変数，__は&restの変数"
;;   (let ((arglst (cl-gensym))
;;         (count (cl-gensym))
;;         (lam-arglst (cl-gensym))
;;         (lam-funct-body-lst (cl-gensym)))
;;     ;; lam-arglst，lam-funct-body-lstはlambda式の引数のリストと実行部分の
;;     ;; リスト
;;     `(let* ((_ (quote ,(cl-gensym)))
;;             (__ (quote ,(cl-gensym)))
;;             (,arglst (mapcar (lambda (e)
;;                                (cond ((or (eq _ e) (eq __ e)) e)
;;                                         ;シンボルなどの評価を抑制
;;                                      (t (list 'quote e))))
;;                              (list ,@args)))
;;             (,lam-arglst (cl-loop repeat (count _ ,arglst)
;;                                collect (cl-gensym)))
;;             (,count 0)
;;             (,lam-funct-body-lst
;;              (mapcar (lambda (e)
;;                        (cond ((eq _ e)
;;                               (prog1
;;                                   (nth ,count ,lam-arglst)
;;                                 (incf ,count)))
;;                              (t e))) ,arglst)))
;;        (unless (functionp  ,funct)
;;          (error "The first argument must be a function object."))
;;        (cond
;;         ((memq __ ,arglst)
;;          (setq ,lam-funct-body-lst (remove-if (lambda (elt) (eq elt __))
;;                                               ,lam-funct-body-lst)
;;                ,lam-funct-body-lst (append '(list) ,lam-funct-body-lst))
;;          (setq ,lam-funct-body-lst
;;                (list 'apply ',funct (list 'append ,lam-funct-body-lst __))
;;                ,lam-arglst
;;                (append ,lam-arglst (list '&rest __))))
;;         (t
;;          (setq ,lam-funct-body-lst
;;                (append (list ,funct) ,lam-funct-body-lst))))
;;        (list 'lambda ,lam-arglst ,lam-funct-body-lst))))


;;;###autoload
(defmacro my-composite-function (&rest funcs)
  "When funcs is '('f1 'f2 ... 'fn), returns a composite function x |-> fn \. fn-1 \. ... \. f1(x)."
  (let ((len (length funcs))
        (funcs (nreverse funcs)))
    (case len
      (0 (lambda (x) x))
      (1 `(lambda (&rest args) (apply ,(car funcs) args)))
      (t `(lambda (&rest args) (funcall ,(car funcs)
                                    (apply
                                     (my-composite-function
                                      ,@(nreverse (cdr funcs))) args)))))))

;;;###autoload
(defmacro my-add-to-list (list &rest elements)
  (declare (indent 1))
  (append '(progn)
          (cl-loop for i in elements
                collect
                `(add-to-list ,list ,i))))
;;;###autoload
(defmacro case* (expr &rest clauses)
  "Like case, but compared by `eqaul'"
  (declare (indent 1))
  (let ((a (my-gensym)))
    `(let ((,a ,expr))
       (cond
        ,@(cl-loop for (a1 a2) in clauses
                collect (list (if (or (eq a1 't)
                                      (eq a1 'otherwise))
                                  t
                                  (list 'equal a a1)) a2))))))

(defsubst list-prod2 (l1 l2)
  (cl-loop for i in l1
        append
        (cl-loop for j in l2
              collect (cons i j))))

;;;###autoload
(defsubst list-prod (&rest lists)
  (cl-reduce 'list-prod2 lists :from-end t :initial-value '(nil)))

;;;###autoload
(defsubst list-prod-n (list n)
  (let ((lists (make-list n list)))
    (apply 'list-prod lists)))

;;;###autoload
(defun get-value (func-or-val &rest args)
  (if (functionp func-or-val)
      (apply func-or-val args)
    func-or-val))

;;;###autoload
(defun file:write (cts file)
  (let ((coding-system-for-write 'utf-8))
    (write-region (prin1-to-string cts) nil file nil 'no-messg)))

;;;###autoload
(defun file:read (file)
  (with-temp-buffer
    (let ((coding-system-for-read 'utf-8))
      (when (ignore-errors (insert-file-contents file))
        (read (current-buffer))))))


;;;###autoload
(defun my:not-required-packages ()
  (with-current-buffer (get-buffer-create "*not-required-packages*")
    (erase-buffer)
    (let ((ls (cl-loop for epl-p in (epl-installed-packages)
                    for p = (epl-package-name epl-p)
                    unless (member p (cl-loop for epl-p in (epl-installed-packages)
                                           for p = (epl-package-name epl-p)
                                           for reqs =
                                           (->> (cdr (assoc p package-alist))
                                                package-desc-reqs
                                                (-map 'car))
                                           append reqs))
                    collect p)))
      (dolist (s (nreverse ls))
        (insert (symbol-name s))
        (newline))))
  (pop-to-buffer "*not-required-packages*"))

;;;###autoload
(defun cart-prod (&rest args)
  (cond ((null (cdr args)) (-map #'list (car args)))
        (t (-mapcat
            (lambda (c) (--map (cons it c) (car args)))
            (apply #'cart-prod (cdr args))))))

;;;###autoload
(defmacro some->> (x form &rest forms)
  (if forms
      `(some->> (some->> ,x ,form) ,@forms)
    (if (sequencep form)
        (let ((my-var (cl-gensym)))
          `(let ((,my-var ,x))
             (if ,my-var (,(car form) ,@(cdr form) ,my-var))))
      (let ((my-var (cl-gensym)))
        `(let ((,my-var ,x))
           (if ,my-var (,form ,my-var)))))))

;;;###autoload
(defmacro my:lazy-do (&rest body)
  (declare (indent 0))
  `(deferred:$
     (deferred:next
       (sleep-for 0.1))
     (deferred:nextc it
       (lambda () ,@body))))

;;;###autoload
(defun my:mouse-keys-non-prefix ()
  (let ((modifiers '(nil "C" "S" "s" "M" "H")))
    (->> (cart-prod
          modifiers modifiers
          '(nil "drag" "double" "triple" "down")
          (--map (format "mouse-%s" it) (number-sequence 1 10)))
         (--map (s-join "-" (-keep #'identity it)))
         (--map (vector (intern it))))))

;;;###autoload
(defun my:mouse-keys ()
  (->> (cart-prod
        (cons nil (-map #'vector (list ?\C-c ?\C-x)))
        (my:mouse-keys-non-prefix))
       (-map (-compose (-applify #'vconcat)
                       (-partial #'-keep #'identity)))))


;;;###autoload
(defun my:shell-aysnc (name cmd output-buf)
  (with-current-buffer (get-buffer-create output-buf)
    (erase-buffer)
    (let ((proc (start-process name (current-buffer)
                               shell-file-name
                               shell-command-switch
                               cmd)))
      (shell-mode)
      (set-process-sentinel proc 'shell-command-sentinel)
      (set-process-filter proc 'comint-output-filter))))

(provide 'my-utilities)

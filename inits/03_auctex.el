;; -*- lexical-binding: t -*-
(setq TeX-parse-self t)
;; (require 'auctex-autoloads)
(autoload 'my-ispell-buffer "my-ispell-buffer" nil t)

(with-eval-after-load "bibtex"
  (my-define-keys global-map "C-j" 'next-line bibtex-mode-map)
  (define-key bibtex-mode-map (kbd "TAB") nil)
  (define-key bibtex-mode-map (kbd "C-j") nil))

;; reftex
(setq reftex-plug-into-AUCTeX t)
(with-eval-after-load 'reftex-ref
  (defun my/reftex-label-before (&optional _environment _no-insert &rest _args)
    (reftex-parse-all))
  (advice-add 'reftex-label :before #'my/reftex-label-before))

(defun my-latex-cdg-sys ()
  (let* ((cds (symbol-name buffer-file-coding-system)))
    (cond ((string-match "euc" cds) "euc")
          ((string-match "shift.jis" cds) "sjis")
          ((string-match "japanese.+iso\\|iso.+j" cds) "jis")
          ((string-match "utf" cds) "utf8")
          (t nil))))

(defun my-platex-buffer-coding-system ()
  (aif (my-latex-cdg-sys)
      (format "-kanji=%s" it)
    ""))

(cl-defun my-latexmk-command-str (&key tex bib
                                       (pdflatex (format "%%(my-pdflatex) -synctex=1"))
                                       (idx "mendex -U")
                                       (dvipdf "dvipdfmx")
                                       (opt nil))
  (let* ((cds (my-platex-buffer-coding-system))
         (tex (or tex (format "platex %s -src-specials" cds)))
         (bib (or bib (format  "pbibtex %s" cds)))
         (quote-string (case system-type
                         (windows-nt "\"")
                         (t "'"))))
    (format "latexmk %s %s %%t"
            (mapconcat
             (lambda (s)
               (format "-e %s%s%s" quote-string s quote-string))
             (list (format "$latex=q/%s %%S %%(mode)/" tex)
                   (format "$bibtex=q/%s/" bib)
                   (format "$pdflatex=q/%s %%S %%(mode)/" pdflatex)
                   (format "$makeindex=q/%s/" idx)
                   ;; takao.mapを埋め込み
                   (format "$dvipdf=q/%s -f takao %%%%O -o %%%%D %%%%S/" dvipdf))
             " ")
            (if opt
                (mapconcat (lambda (x) (concat "-" x)) opt " ")
              ""))))

(defun my-auctex-command-lst-elmt (name cmd &optional type)
  "type is 'bib or 'other"
  (cond
    ((eq type 'bib) (list name cmd 'TeX-run-BibTeX nil t))
    ((eq type 'other) (list name cmd 'TeX-run-discard-or-function t t))
    (t (list name cmd 'TeX-run-TeX nil '(latex-mode)))))

(defun my-add-tex-command (list)
  (setq-local TeX-command-list
              (append (--map (apply #'my-auctex-command-lst-elmt it) list)
                      TeX-command-list)))

(my-add-hook LaTeX-mode-hook
  (toggle-truncate-lines -1)
  (toggle-word-wrap)
  (local-set-key (kbd "C-x n") nil)
  ;; LaTeX-commandを正しく設定する
  (let ((cds (my-platex-buffer-coding-system)))
    (if (equal cds "")
        (set (make-local-variable 'LaTeX-command) "latex")
      (set (make-local-variable 'LaTeX-command)
           (format "platex %s" cds))))

  (setq LaTeX-default-style "amsart")
  (setq japanese-TeX-mode nil)          ;output logの文字化け防止
  (flyspell-mode t)
  ;; previewの画像の倍率
  (setq preview-scale-function 1.2
        preview-image-type 'dvipng)
  ;; (setq preview-scale-function 1.5)
  ;; (outline-minor-mode t)
  ;; (tex-fold-mode)
  ;; (turn-off-auto-fill)
  ;; (orgtbl-mode 1)
  (when (boundp 'orgtbl-mode-map)
    (define-key orgtbl-mode-map (kbd "C-c C-t") 'orgtbl-ctrl-c-ctrl-c)
    (define-key orgtbl-mode-map (kbd "C-c C-c") nil))
  (my-add-tex-command
   `(("latexmk" ,(my-latexmk-command-str))
     ;; ("pdflatex" "pdflatex -synctex=1 %t")
     ("pdflatex latexmk" ,(my-latexmk-command-str :opt '("pdf")))
     ("pvc-latexmk pdflatex" ,(my-latexmk-command-str :opt '("pdf" "pvc")))
     ;; ("pvc-latexmk" (lambda () (my-latexmk-command-str :opt '("pvc"))))
     ("latexmk -pdfdvi"
      ,(my-latexmk-command-str :opt '("pdfdvi" "dvi")))
     ("pvc-latexmk -pdfdvi"
      ,(my-latexmk-command-str :opt '("pdfdvi" "dvi" "pvc")))
     ("dvipdfmx" "dvipdfmx -f takao.map %s.dvi" other)
     ("pLaTeX"
      ,(format "platex %s -src-specials -interaction=nonstopmode %%t"
               (my-platex-buffer-coding-system)))
     ("pBibTeX"
      ,(format "pbibtex %s %%s" (my-platex-buffer-coding-system)) bib)))

  (let ((cds (my-latex-cdg-sys)))
    (unless cds
      (tex-pdf-mode 1))
    (setq TeX-command-default
          (case* cds
            ('nil "pdflatex latexmk")
            (otherwise "latexmk -pdfdvi"))))

  ;; (when (stringp (guess-TeX-master (buffer-file-name)))
  ;;   (setq TeX-master (guess-TeX-master (buffer-file-name))))
  ;; (when (stringp TeX-master)
  ;;   (setf (buffer-name)
  ;;         (format "%s(%s)"
  ;;                 (file-name-nondirectory (buffer-file-name)) TeX-master)))
  ;; reftex environment
  (setq reftex-label-alist
        '(
          ("thm" ?h "thm:" "~\\ref{%s}" t ("theorem" "定理")
           nil)
          ("Thm" ?h "thm:" "~\\ref{%s}" t ("theorem" "定理")
           nil)
          ("dfn" ?d "dfn:" "~\\ref{%s}" t ("definition" "定義")
           nil)
          ("Def" ?d "dfn:" "~\\ref{%s}" t ("definition" "定義")
           nil)
          ("rem" ?r "rem:" "~\\ref{%s}" t ("remark" "注意")
           nil)
          ("Rem" ?r "Rem:" "~\\ref{%s}" t ("remark" "注意")
           nil)
          ("cor" ?c "cor:" "~\\ref{%s}" t ("corollary" "系")
           nil)
          ("Cor" ?c "Cor:" "~\\ref{%s}" t ("corollary" "系")
           nil)
          ("lem" ?l "lem:" "~\\ref{%s}" t ("lemma" "補題")
           nil)
          ("Lem" ?l "Lem:" "~\\ref{%s}" t ("lemma" "補題")
           nil)
          ("prop" ?p "prop:" "~\\ref{%s}" t ("proposition" "命題")
           nil)
          ("Prop" ?p "Prop:" "~\\ref{%s}" t ("proposition" "命題")
           nil)
          ("exa" ?x "exa:" "~\\ref{%s}" t ("example" "例")
           nil)
          ("conj" ?j "conj:" "~\\ref{%s}" t ("conjecture" "予想")
           nil)
          ("fact" ?a "fact:" "~\\ref{%s}" t ("fact" "事実")
           nil)
          ("dmath" ?e nil nil t)))
  ;; For eqref.
  (push 'AMSTeX reftex-label-alist)
  (reftex-mode 1)
  (set (make-local-variable 'yas/key-syntaxes)
       '("w" "w_" "w_." "w_.()" "^ "))
  (setq fill-column 78)
  (setq-local company-backends
              (append '(company-math-symbols-latex company-latex-commands)
                      company-backends))
  (add-to-list 'TeX-expand-list
               (list "%(my-pdflatex)" (lambda ()
                                        (cl-case TeX-engine
                                          (luatex "lualatex")
                                          (xetex "xelatex")
                                          (t "pdflatex"))))))


;; TeX-cleanで消す拡張子のリスト
(setq LaTeX-clean-intermediate-suffixes
      (append (list "\\.vrb"
                    "\\.fls"
                    "\\.fmt"
                    "\\.cache\\.tex"
                    "\\.fdb_latexmk"
                    "\\.sagetex\\.sage"
                    "\\.sagetex\\.py"
                    "\\.sagetex\\.scmd\\.tmp"
                    "\\.sagetex\\.sout\\.tmp")
              (cl-loop for a in '("acn" "acr" "alg" "aux" "bbl" "blg"
                                  "glg" "glo" "gls" "idx" "ilg" "ind" "ist"
                                  "lof" "log" "lot" "maf" "mtc" "mtc0" "nav"
                                  "nlo" "out" "pdfsync" "snm" "synctex\\.gz"
                                  "toc" "vrb" "xdy" "tdo" "dvi")
                       collect (concat "\\." a))))

;; dired clean tex extensions
(with-eval-after-load "dired-x"
  (dolist (x LaTeX-clean-intermediate-suffixes)
    (add-to-list 'dired-tex-unclean-extensions
                 (replace-regexp-in-string "\\\\" "" x)))
  (add-to-list 'dired-tex-unclean-extensions ".prv"))


(with-eval-after-load "font-latex"
  (my-keychord-define LaTeX-mode-map
    "jk" "\\"
    "ev" 'LaTeX-mark-environment
    "av" 'my-mark-tex-math-inner
    "ak" 'my-kill-tex-math
    "ay" 'my-kill-ring-save-tex-math-inner
    "rh" 'insert-bigm|
    "rk" 'insert-left-right-paren
    "rj" 'insert-left-right-braces
    "vm" 'insert-norm
    "gk" 'insert-hat
    "gj" 'insert-_
    "vu" 'insert-=
    "vo" 'insert-and
    "vi" 'insert-plus
    "vk" 'insert-minus
    "vj" 'insert-star
    "vl" 'insert-backslash
    "vh" 'insert-prime
    "vn" 'insert-|
    "sj" 'insert-doll
    "sk" 'insert-equation*
    "dj" 'insert-set
    "dk" 'insert-get
    "dh" 'insert-%--)
  (my-define-keys LaTeX-mode-map
    "RET"       nil
    "C-c i"     nil
    "C-c C-m"   'my-TeX-error-message-create-buffer
    "C-c m"     'my-tex-error-message-warning-create-buffer
    "C-c u"     'my-TeX-open-master-file
    "C-c C-j"   'TeX-view
    "C-c C-v"   'reftex-toc
    "C-c l"     'reftex-label
    "C-c e"     'reftex-reference
    "C-c j"     'LaTeX-find-matching-end
    "C-c k"     'LaTeX-find-matching-begin
    "C-j"       'next-line
    "C-v"       'reindent-then-newline-and-indent
    "C-c C-d"   'my-desktop-clear
    "C-c C-a"   'reftex-parse-all
    "C-c C-h"   'reftex-change-label
    "M-<f9>"    'my-TeX-math-replace-regexp
    "M-S-<f9>"  'my-TeX-math-query-replace-regexp
    "C-q C-f"   'preview-at-point
    "C-q C-d"   'preview-clearout-at-point
    "C-q C-b"   'preview-bufferk
    "C-q C-e"   'preview-environment
    "C-q C-s"   'preview-section)
  (define-key LaTeX-mode-map (kbd "C-c C-c") (my:helm-cmd TeX-command-master))

  (setq LaTeX-indent-level 2)
  (setq LaTeX-item-indent 0)

  (setq LaTeX-verbatim-environments
        (append '("comment" "sageblock" "sageexample" "lstlisting"
                  "sagesilent" "sagecommandline") LaTeX-verbatim-environments))
  ;; ispell
  (eval-after-load "ispell"
    '(add-to-list 'ispell-skip-region-alist '("[^\000-\377]+")))
  ;; ispell tex-skip-alists
  (setq ispell-tex-skip-alists
        (list
         (append
          (car ispell-tex-skip-alists)  ;tell ispell to ignore content of this:
          '(
            ;; ("\\\\cite"            ispell-tex-arg-end)
            ("\\\\nocite"           ispell-tex-arg-end)
            ("\\\\includegraphics"  ispell-tex-arg-end)
            ("\\\\bibliography"     ispell-tex-arg-end)
            ("\\\\ref"              ispell-tex-arg-end)
            ("\\\\eqref"            ispell-tex-arg-end)
            ("\\\\label"            ispell-tex-arg-end)
            ("\\\\usenamespace"            ispell-tex-arg-end)
            ;; ("\\[[a-zA-Z|]+")
            ("%" . "$")
            ("\\(\\$\\)+" . "\\(\\$\\)+")
            ("\\\\\\[" . "\\\\\\]")
            ))
         (append
          (cadr ispell-tex-skip-alists)
          '(
            ("gather\\*?"
             . "\\\\end[ \t\n]*{[ \t\n]*gather\\*?[ \t\n]*}")
            ("declare"
             . "\\\\end[ \t\n]*{[ \t\n]*declare[ \t\n]*}")
            ("eqnarray\\*?"
             . "\\\\end[ \t\n]*{[ \t\n]*eqnarray\\*?[ \t\n]*}")
            ("multline\\*?"
             . "\\\\end[ \t\n]*{[ \t\n]*multline\\*?[ \t\n]*}")
            ("equation\\*?"
             . "\\\\end[ \t\n]*{[ \t\n]*equation\\*?[ \t\n]*}")
            ("align\\*?" . "\\\\end{align\\*?}")
            ("\\(display\\)?math" . "end{\\(display\\)?math}")
            ))))
  (add-elements 'LaTeX-clean-output-suffixes
    "\\.sagetex\\.scmd" "\\.sagetex\\.sout")
  (font-lock-add-keywords
   'latex-mode
   `((,(format "\\(\\\\%s\\){\\([^}]*\\)}" (regexp-opt my-latex-text-commands))
      (1 font-lock-keyword-face t)
      (2 my-latex-text-face t))
     (,(rx "\\" (or "left" "right") eow)
      (0 my-latex-left-right-face t))
     (,(rx symbol-start "a" symbol-end (1+ whitespace)
           (group (or (or "a" "i" "e" "o")
                      (and "u" (not (any "n"))))
                  (0+ alnum)))
      (1 font-lock-warning-face)))))

(my-eval-after-load "font-latex"
  (setq font-latex-fontify-sectioning 1.1)

  ;; 色付される数式環境のリスト
  (add-elements 'font-latex-math-environments "math" "declare"
                "dmath" "dmath*")
  (face-spec-set
   'my-latex-text-face
   `((((class color) (background dark))
      (:background nil :foreground
                   ,(face-foreground 'default))))))

(defface my-latex-text-face
  '((t (:foreground nil)))
  "latex text face"
  :group 'my-latex-faces)
(defvar my-latex-text-face
  'my-latex-text-face)
(defvar my-latex-text-commands
  (list "text" "intertext"))
(defface my-latex-variable-face
  '((((class color) (background dark))
     (:foreground "PaleTurquoise1" :bold t))
    (t (:foreground "red2" :bold t)))
  "latex variable face"
  :group 'my-latex-faces)
(defface my-latex-left-right-face
  '((t (:foreground "grey30")))
  "latex variable face"
  :group 'my-latex-faces)
(defvar my-latex-left-right-face 'my-latex-left-right-face)
(defvar my-latex-variable-face 'my-latex-variable-face)
(defvar font-latex-math-face 'font-latex-math-face)

;; my-ispell-buffer
(defun my-add-ispell-skip-alist ()
  (require 'my-ispell-buffer)
  (add-to-list 'my-ispell-skip-alist
               `(back . ("\\\\[a-zA-Z]+"
                         ,(rx (or "cite" "ref" "label" "usenamespace")
                              (zero-or-one "[" (0+ nonl) "]")
                              (regexp "{[^}]*"))
                         "\\\\\\(set\\|get\\)\\(\\[[1-9]\\]\\)*{[a-z]*")))
  (add-to-list 'my-ispell-skip-alist
               '(func . ((lambda () (forward-char -1) (texmathp))))))
(add-hook 'LaTeX-mode-hook 'my-add-ispell-skip-alist)

(setq my-TeX-texmf-user-dir "~/texmf/tex/latex")
(my-eval-after-load "tex"
  (setq TeX-view-program-selection '((output-dvi "xdvi")
                                     (output-pdf "Evince")))

  (let ((dvi-cmd (concat "xdvi"
                         " -s 5 -nofork -watchfile 1 -editor"
                         " \"emacsclient +%%l %%f\" %d -sourceposition %n:%b")))
    (setq TeX-view-program-list (append `(("xdvi" ,dvi-cmd)
                                          (output-pdf "Evince"))
                                        TeX-view-program-list))

    (my-add-tex-command
     `(("Evince" "evince %s.pdf" other)
       ("Qpdfview" "qpdfview %s.pdf" other)
       ("ViewDvi" ,dvi-cmd other))))
  (add-hook 'LaTeX-mode-hook 'TeX-source-correlate-mode))

(require 'anything-exuberant-ctags)
(eval-when-compile (require 'cl))
(require 'cl-lib)
(defvar anything-c-source-exuberant-ctags-select-grep)
(defun anything-exuberant-ctags-select-lang=c (&optional symbol-name)
  (interactive)
  (let* ((tagfile (anything-exuberant-ctags-get-tag-file))
         (initial-pattern
          (if symbol-name
              (concat "\\_<" (regexp-quote symbol-name) "\\_>"
                      (if (featurep 'anything-match-plugin) " ")))))
    (setq anything-c-source-exuberant-ctags-select-grep
          `((name . "Exuberant ctags")
            (grep-candidates-lang-c ,tagfile)
            (get-line . anything-exuberant-ctags-get-line)
            (action ("Goto the location" .
                     (lambda (can) (let (linenum filename)
                                 (string-match "^[^\t ]+\t\\([^\t ]+\\)" can)
                                 (setq filename (expand-file-name
                                                 (match-string 1 can)
                                                 anything-exuberant-ctags-tag-file-dir))
                                 (string-match "\tline:\\([0-9]+\\)\t" can)
                                 (setq linenum (string-to-number (match-string 1 can)))
                                 (if  (and filename
                                           (file-exists-p filename))
                                     (progn
                                       (find-file filename)
                                       (goto-line linenum))
                                   (message "Can't find target file: %s" filename))))))
            (requires-pattern . 4)
            (delayed 0.8)
            (candidate-number-limit . 200)
            (candidate-transformer .
                                   (lambda (candidates)
                                     (anything-exuberant-ctags-transformer-grep-lang-c candidates)))))
    (anything '(anything-c-source-exuberant-ctags-select-grep)
              ;; Initialize input with current symbol
              initial-pattern "Find Tag: " nil)))

(defun anything-exuberant-ctags-transformer-grep-lang-c (tags)
  (cl-flet ((format-func
          (a b c)
          (funcall 'anything-exuberant-ctags-line-format a b c)))
    (let* (list
           (name-max-len 0)
           (path-max-len 0)
           (entries
            (mapcar
             (lambda (tag)
               (let (entry name path kind class line)
                 (if (string-match "^\\([^\t ]+\\)\t\\([^\t ]+\\)\t" tag)
                     (progn
                       (setq name (match-string 1 tag))
                       (setq path (match-string 2 tag))
                       (put-text-property 0 (length name) 'face 'bold name))
                   (setq name "")
                   (setq path ""))
                 (if (string-match "\tline:\\([0-9]+\\)\t" tag)
                     (setq line (match-string 1 tag))
                   (setq line ""))
                 (if (string-match "\tkind:\\([^\t]+\\)\t" tag)
                     (setq kind (match-string 1 tag))
                   (setq kind ""))
                 (if (string-match "\t\\(class:[^\t]+\\)" tag)
                     (setq class (concat "[" (match-string 1 tag) "]"))
                   (setq class ""))
                 (setq name-max-len (max name-max-len (length path)))
                 (setq path-max-len (max path-max-len (length path)))
                 (setq entry (list path name kind class tag))
                 )) tags)))
      (when (< anything-exuberant-ctags-max-length path-max-len)
        (setq path-max-len anything-exuberant-ctags-max-length))
      (cl-loop for entry in entries
            do
            (push
             (cons
              (format-func entry path-max-len name-max-len)
              (nth 4 entry))
             list)
            finally (return (nreverse list))))))

(defun anything-exuberant-ctags-select-from-here-lang=c ()
  "Tag jump with current symbol using `anything'."
  (interactive)
  (anything-exuberant-ctags-select-lang=c (thing-at-point 'symbol)))
(provide 'anything-exuberant-ctags-lang-c)

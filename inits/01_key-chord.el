;; -*- lexical-binding: t -*-

;; key 同時押し
(my/lazy-progn
 (require 'key-chord)
 (key-chord-mode 1)
 (key-chord-define-global "kl" 'view-mode)
 (setq key-chord-two-keys-delay 0.052)
 (setq key-chord-one-key-delay 0.2)
 (my-keychord-define global-map
 ;;   "fk" 'insert-paren
   "fj" 'evil-normal-state
 ;;   "fl" 'insert-brackets
 ;;   "jk" 'insert-yen
 ;;   "cg" 'comment-current-line
 ;;   "df" 'kill-sexp
 ;;   "yf" 'copy-sexp
 ;;   "vf" 'mark-sexp
 ;;   "dr" 'kill-whitespace
 ;;   "yq" 'copy-sentence
 ;;   "vq" 'mark-sentence
 ;;   "dw" 'kill-word
   "wi" 'ispell-word
 ;;   "yw" 'copy-word
 ;;   "vw" 'mark-word
   ;; "ds" 'kill-sexp
 ;;   "cd" 'kill-sentence
 ;;   "cy" 'copy-sentence
 ;;   "cv" 'mark-sentence
 ;;   "ys" nil
   ;; "vs" 'mark-sexp
 ;;   "vo" 'insert-and
 ;;   "gj" (lambda () (interactive) (insert "_"))
   ;; "vn" 'insert-|
   )
 )

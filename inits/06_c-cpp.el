(my-add-hook c++-mode-hook
  (local-set-key (kbd "C-c j") 'executable-interpret))

(defun my-c-hook-func ()
  (add-to-list 'flycheck-gcc-include-path "/usr/include/flint")
  (add-to-list 'flycheck-gcc-include-path "/home/sho/app/flint2")
  (setq flycheck-clang-include-path flycheck-gcc-include-path)
  (local-set-key (kbd "C-c C-c") 'compile)
  (local-set-key (kbd "C-c c") 'recompile)
  (local-set-key (kbd "C-c j") 'executable-interpret)
  (local-set-key (kbd "{") 'skeleton-pair-insert-maybe)
  (local-set-key (kbd "C-M-h") 'backward-list)
  ;; (local-set-key (kbd "C-M-spc") 'c-mark-function)
  (flycheck-select-checker 'c/c++-gcc)
  (setq company-clang-arguments (--map (concat "-I" it) flycheck-gcc-include-path))
  (setq-local company-backends '((company-clang company-dabbrev-code company-gtags company-etags company-keywords)
                                 company-semantic company-files))
  (setq company-clang-insert-arguments nil))
(add-hook 'c-mode-hook #'my-c-hook-func)
(add-hook 'c++-mode-hook #'my-c-hook-func)

(setq-default flycheck-gcc-language-standard "c11")

(with-eval-after-load "gdb-mi"
  (gdb-many-windows 1))

(my-named-add-hook c-flycheck
  (when (and (buffer-file-name)
             (not (string= (f-ext (buffer-file-name)) "txt")))
    (flycheck-mode 1)))

;; c-mode
(my-add-hook c-mode-hook
  (key-chord-define c-mode-map "vo" 'insert-and)
  (key-chord-define c-mode-map "vu"  'insert-=)
  (define-key c-mode-map (kbd "C-c m") 'c-mark-function)
  (define-key c-mode-map (kbd "C-c C-f") #'clang-format-buffer))
(add-to-list 'auto-mode-alist '("\\.pr[io]$" . makefile-mode))

;; ;;; auto-complete-c-headers
;; (defun my:ac-c-headers-init ()
;;   (require 'auto-complete-c-headers)
;;   (add-to-list 'ac-sources 'ac-source-c-headers))
;; (add-hook 'c++-mode-hook 'my:ac-c-headers-init)
;; (add-hook 'c-mode-hook 'my:ac-c-headers-init)


;; ;;; emacs-clang-complete-async
;; (defun ac-cc-mode-setup ()
;;   (add-to-list 'load-path
;;                (locate-user-emacs-file "elisp/emacs-clang-complete-async/"))
;;   (require 'auto-complete-clang-async)
;;   (setq ac-clang-complete-executable
;;         (locate-user-emacs-file "elisp/emacs-clang-complete-async/clang-complete"))
;;   (add-to-list 'ac-sources 'ac-source-clang-async)
;;   (ac-clang-launch-completion-process))
;; (add-hook 'c-mode-common-hook 'ac-cc-mode-setup)

(defun my-generate-c-tags ()
  (interactive)
  (async-shell-command "find . -regex \".*\\.[cChH]\\(pp\\|C\\|xx\\|c\\)?\" -print | etags -"))

(defun my-generate-c-tags-only-header ()
  (interactive)
  (async-shell-command "find . -regex \".*\\.[hH]\" -print | etags -"))

(add-hook 'c-mode-hook #'c-turn-on-eldoc-mode)

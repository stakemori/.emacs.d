;; -*- lexical-binding: t -*-

;; (require 'magit-autoloads)
(global-set-key (kbd "C-x v s") 'magit-status)
(setq magit-last-seen-setup-instructions "1.4.0")
(my-eval-after-load "magit"
  ;; magit-statusでcをおしたときすべてをcommitする
  (setq magit-commit-all-when-nothing-staged t)
  (setq magit-diff-refine-hunk t)
  (setq magit-push-always-verify nil)
  (setq magit-restore-window-configuration t)
  ;; modeline quitでbranch名update
  (defun my/magit-mode-line-update (&rest _)
    (when (magit-get-current-branch)
      (let ((root-dir (f-parent (magit-git-dir))))
        (dolist (b (buffer-list))
          (with-current-buffer b
            (when-let (bfn (and vc-mode
                                (buffer-file-name (current-buffer))))
              (when (f-descendant-of\? bfn root-dir)
                ;; mode-lineのbranch名 update
                (vc-file-setprop bfn 'vc-git-symbolic-ref nil)
                (vc-after-save)
                (when git-gutter-mode
                  (git-gutter-mode))
                (force-mode-line-update))))))))
  (add-hook 'magit-post-refresh-hook 'my/magit-mode-line-update))
(magit-auto-revert-mode -1)

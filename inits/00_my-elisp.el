;; -*- lexical-binding: t -*-
;; utility functions or macros
(eval-when-compile (require 'cl-lib))
;; (require 'my-utilities)
;; replace regxepの変更
;; (require 'my-replace)

;; (require 'japanese-color)
;; ;; TeX関係の自作elispコマンド
;; (require 'my-TeX-commands)
;; ;; anything-exuberant-ctagsをgrepで
;; (require 'anything-exuberant-ctags-lang-c)
;; ;; ee outline改造
;; (require 'my-ee-outline)
;; ;; tex newcommand snippet
;; (require 'tex-newcommand-load-yasnippet)
;; ;; tex error message
;; (require 'my-tex-error-message)
;; autoloads
(require 'my-elisp-autoloads)

;; some yasnippet commands
;; my-commands
;; (require 'my-commands)
;; anything grep lang=c
;; (require 'anything-grep-lang-c)

;;; conflictの解決につかう
;; (require 'cl)
;; (defvar my-init-files nil)
;; (setq my-init-files (cl-loop for i in (directory-files (locate-user-emacs-file "inits"))
;;                           if (string-match (rx bol (= 2 num)) i)
;;                           collect i))
;; (defvar my-init-ignore-files nil)
;; (setq my-init-ignore-files nil)
;; (mapc (lambda (f) (unless (member f my-init-files)
;;                 (load (concat (locate-user-emacs-file "inits/") f)))) my-init-files)
(autoload 'helm-pdf-manuscripts "helm-pdf-manuscripts" nil t)

(autoload 'xmodmap-mode "xmodmap.el" nil t)
(push (cons (rx ".Xmodmap" eol) 'xmodmap-mode) auto-mode-alist)

(defmacro add-elements (sequence  &rest elmts)
  (declare (indent 1))
  "Add ELMTS to the head of SEQUENCE and set SEQUENCE the result. "
  `(set ,sequence (append (list ,@elmts) (symbol-value ,sequence))))

(defmacro my-add-hook1 (hook &rest forms)
  "Define a function my-'hook-name'-function1
whose body is forms, and add this function to hook."
  (declare (indent 1))
  `(my-add-hook0  ,hook "function1" ,@forms))

(defmacro my-add-hook (hook &rest forms)
  "Define a function my-'hook-name'-function
whose body is forms, and add this function to hook."
  (declare (indent 1))
  `(my-add-hook0  ,hook "function" ,@forms))


(defmacro my-add-hook0 (hook suffix-name  &rest forms)
  "Define a function my-'hook-name'-'suffix-name'
whose body is forms, and add this function to hook."
  (declare (indent 2))
  (let ((funcsymbol (intern
                     (format "my-%s-%s" (symbol-name hook) suffix-name))))
    `(progn
       (defun ,funcsymbol  () ,@forms)
       (add-hook ',hook (function ,funcsymbol)))))

(defmacro my-named-add-hook (hook &rest forms)
  "Define a function my-'hook-name'-'suffix-name'
whose body is forms, and add this function to hook."
  (declare (indent 1))
  (let* ((suffix-name (cond ((symbolp (car forms))
                             (prog1
                                 (symbol-name (car forms))
                                (setq forms (cdr forms))))
                            ((stringp (car forms))
                             (prog1
                                 (car forms)
                               (setq forms (cdr forms))))
                            (t "function")))
         (funcsymbol (intern
                      (format "my-%s-%s" (symbol-name hook) suffix-name))))
    `(progn
       (defun ,funcsymbol  () ,@forms)
       (add-hook ',hook (function ,funcsymbol)))))

(defmacro my-eval-after-load (file &rest forms)
  "same as eval-after-load"
  (declare (indent 1))
  `(eval-after-load ,file
     '(progn ,@forms)))


(defmacro my-define-keys (keymap &rest defs)
  (declare (indent 1))
  (append (list 'progn)
          (cl-loop for i from 0 to (1- (/ (length defs) 2))
                collect
                `(define-key
                   ,keymap
                   (kbd ,(nth (* 2 i) defs))
                   ,(nth (1+ (* 2 i)) defs)))))

(defmacro my-keychord-define (keymap &rest defs)
  (declare (indent 1))
  (append (list 'progn)
          (cl-loop for i from 0 to (1- (/ (length defs) 2))
                collect
                `(key-chord-define ,keymap
                                   ,(nth (* 2 i) defs)
                                   ,(nth (1+ (* 2 i)) defs)))))
(defun my-delete-elc-only-files ()
  (interactive)
  (cl-loop for f in (f-files default-directory
                          (lambda (x) (string-match "elc$" x))
                          t)
        unless (f-exists? (concat (f-no-ext f) ".el"))
        do (f-delete f)))

(defun my:command-prefix-func (f g)
  `(lambda (arg)
     (interactive "p")
     (call-interactively
      (if (= arg 1)
          #',f
        #',g))))

(defun my:defface (face spec)
  (unless (facep face)
    (face-spec-set face spec)))

(defmacro my/with-eval-after-load-prog-mode (name &rest forms)
  (declare (indent 1))
  `(my-named-add-hook prog-mode-hook ,name
                      ,@(append forms
                                `((remove-hook
                                   'prog-mode-hook
                                   #',(intern (format "my-prog-mode-hook-%s" name)))))))

(defmacro my/lazy-progn (&rest body)
  (let ((timer-sym (cl-gensym))
        (time (or (plist-get body :time) 0.3)))
    `(let ((,timer-sym nil))
       (setq ,timer-sym
             (run-with-idle-timer
              ,time t
              (lambda ()
                (unless (minibufferp)
                  (unwind-protect
                      (progn
                        ,@(if (plist-get body :time)
                              (cddr body)
                            body))
                    (cancel-timer ,timer-sym)))))))))

;; -*- lexical-binding: t -*-

;; (my:defface 'helm-lisp-show-completion
;;             '((((class color) (background dark))
;;                (:background "grey"))
;;               (t (:background "navy"
;;                               :foreground "white"))))
(my:defface 'helm-candidate-number
            '((t (:background nil :foreground nil))))
(my:defface 'helm-selection
            `((((class color) (background dark))
               (:background "grey30"))))
;; (my:defface 'helm-source-header
;;             '((((class color) (background dark))
;;                (:background nil :foreground "grey"))))
(my:defface 'helm-separator
            '((((class color) (background dark))
               (:foreground "azure2"))
              (t (:foreground "black"))))

(setq helm-for-files-preferred-list
  '(helm-source-buffers-list
    helm-source-recentf
    ;; helm-source-files-in-current-dir
    helm-source-file-cache))
;; (my-define-keys global-map
;;   "<f1>a"  'helm-apropos)
;;; helm-for-filesはbufferを集めるのにidoを使う．

(my/lazy-progn
 (require 'ido)
 (setq ido-ignore-buffers (append (list (rx "*anything" )
                                        (rx "*helm" )
                                        (rx "*Google Translate*")
                                        (rx "*epc con")
                                        (rx "*anaconda-mode*")
                                        (rx "*magit-" alnum))
                                  ido-ignore-buffers))
 (setq helm-boring-buffer-regexp-list ido-ignore-buffers))

(my-define-keys global-map
  "<f1>a"  'helm-apropos
  "M-x"    'helm-M-x
  "C-b"    'helm-for-files
  "M-b"    'helm-buffers-list
  "M-y"    'helm-show-kill-ring
  "C-c i"  'helm-imenu
  "M-s"    'helm-occur
  "H-s"    'helm-ag
  "C-c b"  'helm-bookmarks
  "H-s"    'helm-ag
  "C-:"    'helm-resume
  "C-c f"  'helm-recentf)


(my-eval-after-load "helm"

  (my-define-keys helm-map
    "C-M-l" 'forward-char
    "C-M-h" 'backward-char
    "C-n" 'delete-backward-char
    "C-j" 'helm-next-line
    "C-k" 'helm-previous-line
    "C-l" 'helm-next-source
    "C-h" 'helm-previous-source
    "C-p" nil)
  (setq helm-echo-input-in-header-line t)
  (defun spacemacs//helm-hide-minibuffer-maybe ()
    "Hide minibuffer in Helm session if we use the header line as input field."
    (when (with-helm-buffer helm-echo-input-in-header-line)
      (let ((ov (make-overlay (point-min) (point-max) nil nil t)))
        (overlay-put ov 'window (selected-window))
        (overlay-put ov 'face
                     (let ((bg-color (face-background 'default nil)))
                       `(:background ,bg-color :foreground ,bg-color)))
        (setq-local cursor-type nil))))
  (add-hook 'helm-minibuffer-set-up-hook
            'spacemacs//helm-hide-minibuffer-maybe))

;; helm descbinds
(global-set-key (kbd "<f1> b") 'helm-descbinds)
(setq helm-descbinds-window-style 'split-window)

(defun my/helm-etags-select (arg)
  (interactive "P")
  (require 'helm-tags)
  (let ((tag  (helm-etags-get-tag-file))
        (helm-execute-action-at-once-if-one helm-etags-execute-action-at-once-if-one))
    (when (or (equal arg '(4))
              (and helm-etags-mtime-alist
                   (helm-etags-file-modified-p tag)))
      (remhash tag helm-etags-cache))
    (if (and tag (file-exists-p tag))
        (helm :sources 'helm-source-etags-select :keymap helm-etags-map
              :input (let ((tap (thing-at-point 'symbol)))
                       (if (s-blank? tap) ""
                           (concat "\\_<" (thing-at-point 'symbol)  "\\_>" " ")))
              :buffer "*helm etags*"
              :default (concat "\\_<" (thing-at-point 'symbol) "\\_>"))
      (message "Error: No tag file found, please create one with etags shell command."))))

(global-set-key (kbd "M-.") 'helm-etags-select)
;;; replace completing-read by helm
(defmacro my:with-helm-mode (&rest body)
  `(unwind-protect
       (progn (helm-mode 1)
              ,@body)
     (helm-mode -1)))
(defmacro my:helm-cmd (cmd)
  `(lambda ()
     (interactive) (my:with-helm-mode (call-interactively ',cmd))))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (helm-rg flycheck-mypy stream idris-mode gruvbox-theme diminish origami auto-highlight-symbol ripgrep gotham-theme dumb-jump rainbow-delimiters dracula-theme doom-themes dired-collapse atom-one-dark-theme clang-format fontawesome perspeen ediprolog psession spaceline powerline crux evil-iedit-state pcmpl-args swift-mode zerodark-theme lice spacemacs-theme c-eldoc shackle git-gutter-fringe golden-ratio py-isort company-jedi dockerfile-mode yaml-mode skewer-mode paradox ddskk pcre2el use-package org highlight-parentheses flycheck-cask diff-hl company-anaconda ob-sagemath recentf-ext quelpa init-loader persp-mode auto-yasnippet which-key top-mode window-numbering anchored-transpose ace-window ag anaphora async auctex auto-complete auto-complete-c-headers auto-complete-sage avy avy-zap bind-key caml cl-lib clj-refactor clojure-mode coffee-mode color-theme-sanityinc-tomorrow company company-math company-racer concurrent csv-mode ctags-update dash dash-functional deferred dired-filter dired-k edit-server edn epl ess etags-select evil evil-easymotion evil-leader evil-nerd-commenter evil-paredit evil-surround f flycheck flycheck-haskell flycheck-ocaml flycheck-package flycheck-pyflakes flycheck-rust fold-this fringe-helper fuzzy gh git-commit gmail-message-mode go-mode google-this google-translate goto-chg goto-last-change grep-a-lot haskell-mode helm helm-ag helm-c-yasnippet helm-core helm-descbinds helm-projectile helm-sage html-to-markdown htmlize hydra imenu-anywhere inf-clojure inf-ruby js2-mode jss key-chord magit magit-popup magma-mode markdown-mode material-theme merlin mmm-mode monokai-theme multiple-cursors noflet open-junk-file org-bullets package-build paredit pkg-info popup popwin pos-tip py-autopep8 python-environment pyvenv quickrun racer rainbow-mode redo+ rust-mode scala-mode sequential-command shut-up smartrep smex sx toc-org tuareg undo-tree visual-regexp wgrep wgrep-ag window-layout with-editor wolfram-mode yasnippet zenburn-theme cython-mode)))
 '(persp-keymap-prefix (kbd "M-c"))
 '(safe-local-variable-values
   (quote
    ((flycheck-cargo-rustc-args "--features" "clippy")
     (pdflatex-engine . "xelatex")
     (python-shell-virtualenv-root . "~/.virtualenvs/mydrive")
     (eval when
           (and
            (buffer-file-name)
            (file-regular-p
             (buffer-file-name))
            (string-match-p "^[^.]"
                            (buffer-file-name)))
           (emacs-lisp-mode)
           (when
               (fboundp
                (quote flycheck-mode))
             (flycheck-mode -1))
           (unless
               (featurep
                (quote package-build))
             (let
                 ((load-path
                   (cons ".." load-path)))
               (require
                (quote package-build))))
           (package-build-minor-mode)
           (set
            (make-local-variable
             (quote package-build-working-dir))
            (expand-file-name "../working/"))
           (set
            (make-local-variable
             (quote package-build-archive-dir))
            (expand-file-name "../packages/"))
           (set
            (make-local-variable
             (quote package-build-recipes-dir))
            default-directory))
     (encoding . utf-8)
     (flycheck-rust-args "--extern" "rand=/home/sho/app/guessing_game/target/debug/deps/librand-204b49f864ff4762.rlib")
     (require-final-newline . t)))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(put 'erase-buffer 'disabled nil)
;; (dolist (f (butlast (f-entries (locate-user-emacs-file "inits"))))
;;   (insert (concat "
;; ;; "
;;                   (substring (f-filename f) 3)
;;                   "
;; "
;;                   (with-temp-buffer
;;                     (insert-file-contents f)
;;                     (buffer-string))))
;;   (newline))
(put 'narrow-to-region 'disabled nil)

(require 'yasnippet)
(require 'my-utilities)
(require 'tex)
(defun TeX-newcommand-search-buf-file-name ( &optional path-tex-sty-folder)
  ;; option path-tex-sty-folder="~/texmf/tex/latex"
  "search-buf-file-name-listを返す関数.
   search-buf-file-name-listは探すべきbuffer-file-nameからなる alist.
   "
  (let (search-buf-file-name-list TeX-master-file-name file-name TeX-file-buf-name)
    ;; master fileを探してそこに移動しsearch-buf-file-name-listを定義
    (cond
     ((eq TeX-master t)
      (setq search-buf-file-name-list (list (buffer-file-name))))
     ((stringp TeX-master)
      (setq search-buf-file-name-list (list (concat
                                             default-directory
                                             TeX-master
                                             ".tex"
                                             )))))
    ;; option変数path-tex-sty-folderまたは同フォルダ内にある.styファイル，同フォルダ内にある子ファイル
    (when (nth 0 search-buf-file-name-list) ;ediff-mergeのため
      (with-temp-buffer
        (insert-file-contents (nth 0 search-buf-file-name-list))
        (goto-char (point-min))
        (while (re-search-forward "\\\\\\(usepackage\\|RequirePackage\\){\\([^}]+\\)}" (point-max) t 1)
          (setq file-name (buffer-substring-no-properties (match-beginning 2) (match-end 2)))
          (when (file-exists-p (concat default-directory file-name ".sty"))
            (setq TeX-file-buf-name
                  (expand-file-name (concat default-directory file-name ".sty")))
            (push TeX-file-buf-name search-buf-file-name-list))
          (when (and
                 path-tex-sty-folder
                 (file-exists-p (concat path-tex-sty-folder "/"  file-name ".sty")))
            (setq TeX-file-buf-name (expand-file-name (concat path-tex-sty-folder "/"  file-name ".sty")))
            (push TeX-file-buf-name search-buf-file-name-list)
            ))
        (goto-char (point-min))
        (while (re-search-forward "\\\\\\(input\\|include\\){\\([^}]+\\)}" (point-max) t 1)
          (setq file-name (buffer-substring-no-properties (match-beginning 2) (match-end 2) ))
          (when (file-exists-p (concat default-directory file-name ".tex"))
            (setq TeX-file-buf-name (expand-file-name (concat default-directory file-name ".tex")))
            (push TeX-file-buf-name search-buf-file-name-list)))
        search-buf-file-name-list))))

(defun TeX-newcommand-create-snippet-in-the-current-buffer ( expand-file-list )
  ;; expand-file-list expandするべきfile名(full path)のlist
  (let (name number bool (arg-list nil) q comment-end (expand-file-list-string "")
             creationp)
    ;; nameはnewocmmandの名前(string) numberはnewcommandの引数の個数(整数) boolはoptionのあるなし(t or nil)
    ;; arg-listは引数の名前からなるlist
    (goto-char (point-min))
    (while  (re-search-forward "\\\\newcommand{\\\\\\([a-zA-Z@]+\\)}" (point-max) t 1)
      (setq creationp t)
      (setq q (point))
      ;; name number boolの定義
      (setq name (buffer-substring-no-properties (match-beginning 1) (match-end 1) ))
      (if (looking-at "\\[")
          (progn
            (re-search-forward "[1-9]+]")
            (setq number (string-to-number (buffer-substring-no-properties (match-beginning 0) (- (match-end 0) 1))))
            (if (looking-at "\\[")
                (setq bool t)
              (setq bool nil)))
        (setq number 0)
        (setq bool nil))
      (if (progn (beginning-of-line) (looking-at-p "[^\\]*%"))
          (setq creationp nil)) ;comment outされていたらsnippetはつくられない
      ;; arg-listの定義
      (end-of-line)
      (setq comment-end (point))
      (goto-char q)
      (if (re-search-forward "%" comment-end t 1)
          (save-restriction
            (narrow-to-region (point) comment-end)
            (let ((i number) pt)
              (while (>= i 1)
                (goto-char (point-min))
                (if (re-search-forward (concat "#" (number-to-string i)) (point-max) t 1)
                    (progn
                      (setq pt (point))
                      (if (and (re-search-forward "{\\([^}]*\\)}" (point-max) t 1)
                               (not (string-match "#[1-9]" (buffer-substring-no-properties pt (match-beginning 1)))))
                          (progn
                            (goto-char (- (match-beginning 1) 1))
                            (forward-list)
                            (setq arg-list (cons (buffer-substring-no-properties (match-beginning 1) (- (point) 1)) arg-list))
                            )
                        (setq arg-list (cons (concat "arg" (number-to-string i)) arg-list))
                        ))
                  (setq arg-list (cons (concat "arg" (number-to-string i)) arg-list)))
                (setq i (- i 1))
                )))
        (let ((i number))
          (while (>= i 1)
            (setq arg-list (cons (concat "arg" (number-to-string i)) arg-list))
            (setq i (- i 1)))))
      ;; snippetの定義
      (if creationp
          (progn (let ((i 0))
                   (while (< i (length expand-file-list))
                     (setq expand-file-list-string (concat expand-file-list-string " " "\"" (nth i expand-file-list) "\""))
                     (setq i (+ i 1))
                     ))
                 (let (KEY TEMPLATE NAME CONDITION GROUP VARS FILE KEYBINDING snippet-list)
                   (setq KEY name NAME name)
                   (setq GROUP nil VARS nil FILE nil KEYBINDING nil)
                   (setq CONDITION (concat "(and (catch 'bool (progn (backward-char) (while (not (bobp)) (if (looking-at-p \"[a-zA-Z@]\") (backward-char) (throw 'bool (looking-at-p \"\\\\\\\\\")))))) (member (buffer-file-name) (list " expand-file-list-string ")))"))
                   (if bool
                       (setq TEMPLATE  (concat name "${1:" ":opt " (car arg-list) "}"))
                     (setq TEMPLATE (concat name "{${1:" (car arg-list) "}}"))
                     )
                   (if (> number 0)
                       (let ((i 2))
                         (while (<= i number)
                           (setq TEMPLATE (concat TEMPLATE "{$" "{" (number-to-string i) ":" (nth (- i 1) arg-list) "}}"))
                           (setq i (+ i 1))
                           ))
                     (setq TEMPLATE name))
                   (setq TEMPLATE (concat TEMPLATE "$0"))
                   (setq snippet-list (list (list KEY TEMPLATE NAME CONDITION GROUP VARS FILE KEYBINDING)))
                   (yas/define-snippets 'latex-mode snippet-list)
                   )))
      (forward-line 1))))

;; TODO
;; 環境変数texmfの.styファイルをよみこむ
;;;###autoload
(defun TeX-newcommand-yas-reload ( &optional path-tex-sty-folder)
  (interactive)
  (when (aand (buffer-file-name)
              (equal (file-name-extension it) "tex"))
      (save-excursion
        (let (search-buf-file-name-list tex-file-list)
          (setq search-buf-file-name-list (TeX-newcommand-search-buf-file-name path-tex-sty-folder))
          (dolist (x search-buf-file-name-list)
            (when (equal (file-name-extension x) "tex")
              (push x tex-file-list)))
          ;; snippetをつくる
          (dolist (x search-buf-file-name-list)
            (with-temp-buffer
              (insert-file-contents x)
              (TeX-newcommand-create-snippet-in-the-current-buffer tex-file-list)))))))

(provide 'tex-newcommand-load-yasnippet)
;; Local Variables:
;; coding: utf-8
;; End:

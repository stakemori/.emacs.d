;; (unless (package-installed-p 'quelpa)
;;   (with-temp-buffer
;;     (url-insert-file-contents "https://raw.github.com/quelpa/quelpa/master/bootstrap.el")
;;     (eval-buffer)))

;; (defmacro my:quelpa (recipe &rest plist)
;;   (if plist
;;       `(quelpa ',recipe ,@plist)
;;     `(unless (package-installed-p ',(if (listp recipe) (car recipe) recipe))
;;        (quelpa ',recipe))))
;; (byte-compile 'my:quelpa)

;; (my:quelpa (cython-mode
;;             :repo "cython/cython" :fetcher github
;;             :files ("Tools/cython-mode.el")))

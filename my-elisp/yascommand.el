(require 'yasnippet)

;;;###autoload
(defun insert-norm ()
  (interactive)
  (yas/expand-snippet
   "\\left\|$1\\right\|$0"))
;;;###autoload
(defun insert-set ()
  (interactive)
  (yas/expand-snippet
   "\\\\set{$1}$0"))
;;;###autoload
(defun insert-get ()
  (interactive)
  (yas/expand-snippet
   "\\\\get{$1}$0"))
;;;###autoload
(defun insert-bars ()
  (interactive)
  (yas/expand-snippet
   "|$1|$0"))
(provide 'yascommand)

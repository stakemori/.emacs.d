(eval-when-compile (require 'cl))
(require 'anything)
(require 'my-utilities)
(defvar my-file-buffers-alist nil)
(defun my-save-file-buffers (arg)
  (interactive "p")
  (cond ((equal arg 1) (my-push-file-buffers "default"))
        (t (my-push-file-buffers (read-from-minibuffer "name: ")))))

(defun my-push-file-buffers (car)
  (let ((bufs (cl-loop for buf in (buffer-list)
                    if (buffer-file-name buf)
                    collect (buffer-file-name buf))))
    (while (assoc car my-file-buffers-alist)
      (ansetq my-file-buffers-alist
              (delete (assoc car my-file-buffers-alist) it)))
    (push (cons car (list bufs (if (memq (buffer-file-name) bufs)
                                   (buffer-file-name) nil)))
          my-file-buffers-alist)))

(defun my-resume-file-buffers ()
  (interactive)
  (anything-other-buffer
   '(my-anything-c-source-resume-file-buffers)
   "*anything buffers groups*"))


(defun my-open-file-buffers (car)
  (let ((buf-cdr (cdr (assoc car my-file-buffers-alist))))
    (dolist (file (car buf-cdr))
      (when (file-exists-p file)
        (find-file file)))
    (awhen (cadr buf-cdr) (find-file it))))
(defvar my-anything-c-source-resume-file-buffers
  '((candidates . (lambda () (cl-loop for x in my-file-buffers-alist
                               collect (car x))))
    (name . "anything buffer groups")
    (action . (("resume" . my-open-file-buffers)
               ("delete" . my-delete-from-file-buffers-alist)))))

(defun my-delete-from-file-buffers-alist (can)
  (ansetq my-file-buffers-alist (delete (assoc can it) it)))

(defvar my-clear-file-buffers-hook nil)
(defun my-clear-file-buffers ()
  (interactive)
  (cl-loop for buf in (buffer-list)
        if (buffer-file-name buf)
        do
        (kill-buffer buf)
        (run-hooks 'my-clear-file-buffers-hook)))
(provide 'my-file-buffers)

;; -*- lexical-binding: t -*-
;; evil
(require 'evil-common)
(with-eval-after-load "evil-easymotion"
  (require 'avy))
(eval-when-compile (require 'evil-easymotion))
(autoload 'evilem--jump "evil-easymotion")
(autoload 'evilem--collect "evil-easymotion")
(autoload 'evilem-make-motion "evil-easymotion")
(global-set-key (kbd "M-RET")
                (lambda () (interactive)
                  (end-of-line)
                  (evil-insert-state)
                  (newline-and-indent)))
(use-package evil
  :init
  (progn
    (evil-mode 1)
    (setq evil-cross-lines nil)
    (setq evil-want-fine-undo t)
    ;; vimに近いundo
    (setq evil-in-single-undo t))
  :config
  (progn
    (global-undo-tree-mode -1)
    (remove-hook 'evil-local-mode-hook #'evil-turn-on-undo-tree-mode)
    (setq-default evil-symbol-word-search t)
    (define-key evil-normal-state-map (kbd "C-t") 'my-ace-window-cmd)
    ;; (define-key evil-normal-state-map (kbd "b") 'origami-recursively-toggle-node)
    ;; (define-key evil-normal-state-map (kbd "B") 'origami-close-all-nodes)
    (define-key evil-insert-state-map (kbd "C-t") 'my-ace-window-cmd)
    (my-define-keys evil-insert-state-map
      "C-e" nil
      "C-k" nil
      "C-j" nil
      "C-h" nil
      "C-v" nil
      "C-n" nil
      "C-y" nil
      "C-o" nil
      "C-r" nil
      "C-a" nil)
    (my-define-keys evil-motion-state-map
      "C-e" nil
      "TAB" nil
      "C-b" nil
      "," 'avy-goto-char-timer
      "t" (evilem-create
           #'evil-repeat-find-char
           :name
           'evilem--motion-evil-find-char-to
           :pre-hook
           (save-excursion (setq evil-this-type 'inclusive)
                           (call-interactively #'evil-find-char-to))
           :bind
           ((evil-cross-lines nil)))
      "f" (evilem-create
           #'evil-repeat-find-char
           :name
           'evilem--motion-evil-find-char
           :pre-hook
           (save-excursion (setq evil-this-type 'inclusive)
                           (call-interactively #'evil-find-char))
           :bind
           ((evil-ross-lines nil)))
      "u" 'undo)
    (global-set-key (kbd "C-/") 'undo)
    (my-define-keys evil-normal-state-map
      "m"    'evil-jump-item
      "s"    'save-buffer
      "M-."  nil
      "C-r"  nil
      "C-."  nil
      "q"    nil)
    ;; (key-chord-define evil-insert-state-map "jj" 'evil-normal-state)
    (cl-loop for (mode . state) in '((magit-popup-mode . emacs)
                                     (eww-mode . emacs)
                                     (ag-mode . normal)
                                     (grep-mode . emacs)
                                     (shell-mode . emacs)
                                     (magit-branch-manager-mode . emacs)
                                     (magit-revision-mode . emacs)
                                     (magit-popup-sequence-mode . emacs)
                                     (magit-popup-help-mode . emacs)
                                     (dired-mode . emacs)
                                     (wdired-mode . normal)
                                     (sage-shell-mode . emacs)
                                     (inf-clojure-mode . emacs)
                                     (term-mode . emacs)
                                     (inferior-python-mode . emacs)
                                     (cider-repl-mode . emacs)
                                     (moccur-mode . emacs)
                                     (sbt-mode . emacs)
                                     (eshell-mode . emacs)
                                     (top-mode . emacs)
                                     (paradox-menu-mode . emacs)
                                     (idris-repl-mode . emacs))
             do (evil-set-initial-state mode state))

;;; evil word syntax table
    (defvar my/evil-word-chars-alist `((emacs-lisp-mode ?-)
                                       (python-mode ?_)
                                       (rust-mode ?_)
                                       (c-mode ?_)))
    (defun my/forward-evil-word-around (fun &rest args)
      (with-syntax-table (syntax-table)
        (cl-loop for (m . chrs) in my/evil-word-chars-alist
                 if (derived-mode-p m)
                 do (dolist (c chrs)
                      (modify-syntax-entry c "w")))
        (apply fun args)))
    (advice-add 'forward-evil-word :around #'my/forward-evil-word-around)

    (defvar c-z-map (make-sparse-keymap))
    (define-key evil-normal-state-map (kbd "n") c-z-map)
    (smartrep-define-key evil-normal-state-map "n"
      '(("n" . 'mc/mark-next-symbol-like-this)
        ("p" . 'mc/mark-previous-symbol-like-this)
        ("j" . 'mc/mark-next-like-this)
        ("k" . 'mc/mark-previous-like-this)
        ("m" . 'mc/mark-more-like-this-extended)
        ("u" . 'mc/unmark-next-like-this)
        ("U" . 'mc/unmark-previous-like-this)
        ("s" . 'mc/skip-to-next-like-this)
        ("S" . 'mc/skip-to-previous-like-this)
        ("*" . 'mc/mark-all-like-this)
        ("d" . 'mc/mark-all-like-this-dwim)
        ("i" . 'evil-insert)
        ("o" . 'mc/sort-regions)
        ("O" . 'mc/reverse-regions)))))

;;; leader
(global-evil-leader-mode)
(evil-leader/set-leader "<SPC>")
(evil-leader/set-key
  "r" 'helm-recentf
  "b" 'helm-buffers-list
  "," 'avy-goto-char-timer
  "s" 'save-buffer
  "g" 'magit-status
  "p" 'projectile-command-map
  ;; "pp" 'helm-projectile-switch-project
  ;; "pf" 'helm-projectile-find-file
  ;; "pb" 'helm-projectile-switch-to-buffer
  ;; "pk" 'projectile-kill-buffers
  "i" 'helm-imenu
  "I" 'helm-imenu-anywhere
  "q" 'kill-this-buffer
  "h" 'async-shell-command
  "ys" 'copy-sexp)
(evil-leader/set-key-for-mode 'latex-mode
  "eu" (lambda () (interactive) (LaTeX-environment 1))
  "cc" 'TeX-command-master)

;;; evil paredit
(evil-define-key 'normal evil-paredit-mode-map
  (kbd "D") 'paredit-kill)
(add-hook 'emacs-lisp-mode-hook 'evil-paredit-mode)
(add-hook 'clojure-mode-hook 'evil-paredit-mode)

;;; surround
(require 'evil-surround)
(global-evil-surround-mode 1)
(setq-default evil-surround-pairs-alist
              (append '((?\( . ("(" . ")"))
                        (?\[ . ("[" . "]"))
                        (?\{ . ("{" . "}"))
                        evil-surround-pairs-alist)))

;; nerd commenter
(evil-leader/set-key
  "ci" 'evilnc-comment-or-uncomment-lines
  "cp" 'evilnc-comment-or-uncomment-paragraphs)

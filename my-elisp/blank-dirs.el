(eval-when-compile (require 'cl))
(require 'package)
(defvar my-directory-ignores-regexps (rx (or (and (or "." "..") eol)
                                             ".elc")))
(cl-defun my-delete-blank-dirs (&optional (dir default-directory))
  (cl-loop for d in (my-dir-ess-empty-dirs dir)
        do (shell-command (format "rm -rf %s" d))))

;;;###autoload
(defun delete-empty-package-dirs ()
  (interactive)
  (my-delete-blank-dirs package-user-dir))

(defun my-directory-ess-empty-p (dir)
  (not (cl-loop for f in (directory-files dir)
             unless (string-match my-directory-ignores-regexps f)
             collect f)))

(defun my-dir-ess-empty-dirs (dir)
  (cl-loop for f in (with-no-warnings
                   (remove-if
                    (lambda (x) (or (string= x ".")
                                    (string= x "..")))
                    (directory-files dir)))
        for abs-path = (expand-file-name f dir)
        if (and
            (file-directory-p abs-path)
            (my-directory-ess-empty-p abs-path))
      collect abs-path))

;; (package-generate-autoloads "my-elisp" default-directory)

(require 'tex)
(require 'popwin)
(require 'my-utilities)
(eval-when-compile (require 'cl))
(defun my-tex-error-message-list ()
  (save-excursion
    (let (s1 tex-master-buf-name (lst nil))
      (setq tex-master-buf-name (my-tex-error-message-master-buf-name))
      (with-temp-buffer
        (insert-file-contents (concat default-directory tex-master-buf-name ".log"))
        (unwind-protect
            (progn
              (goto-char (point-min))
              (if (not (re-search-forward "Emergency stop" (point-max) t 1))
                  (progn
                    (setq lst
                          (list (my-tex-error-message-error-list tex-master-buf-name)
                                (my-tex-error-message-reference-warnings-list tex-master-buf-name)
                                (my-tex-error-message-bad-boxes-list tex-master-buf-name))))
                (goto-char (point-min))
                (while (re-search-forward "\\(^!.+\\)" (point-max) t 1)
                  (unless (string-match-p "Emergency stop" (match-string 1))
                    (setq s1 (buffer-substring-no-properties (match-beginning 1) (match-end 1)))
                    (setq lst (cons (list s1 nil nil) lst))
                    (forward-line 1)))
                (setq lst (reverse lst))
                (setq lst (list lst nil nil))))
          (kill-buffer nil)
          (eval 'lst))))))


(defun my-tex-error-only-error-message-list ()
  (save-excursion
    (let (s1 tex-master-buf-name (lst nil))
      (setq tex-master-buf-name (my-tex-error-message-master-buf-name))
      (with-temp-buffer
        (insert-file-contents (concat default-directory tex-master-buf-name ".log"))
        (unwind-protect
            (progn
              (goto-char (point-min))
              (if (not (re-search-forward "Emergency stop" (point-max) t 1))
                  (progn
                    (setq lst  (list (my-tex-error-message-error-list tex-master-buf-name) nil nil)))
                (goto-char (point-min))
                (while (and (re-search-forward "\\(^!.+\\)" (point-max) t 1)
                            (not (string-match-p "Emergency stop"
                                                 (match-string 1))))
                  (setq s1 (buffer-substring-no-properties (match-beginning 1) (match-end 1)))
                  (setq lst (cons (list s1 nil nil) lst))
                  (forward-line 1))
                (setq lst (reverse lst))
                (setq lst (list lst nil nil))
                ))
          (kill-buffer nil)
          (eval 'lst))))))

(defun my-tex-error-message-master-buf-name ()
  (let ( name )
    (if (stringp TeX-master)
          (setq name TeX-master)
        (setq name (file-name-sans-extension
                    (file-name-nondirectory(buffer-file-name))))
        )))



(defun my-tex-error-message-error-list (tex-master-buf-name)
  (let (s1 err-buf-name
           err-line output-err-point
           (lst nil)) ;lstはs1(errorメッセージ),err-buf-name,err-lineを要素にもつリストのリスト
    (goto-char (point-min))
    (while (re-search-forward "^!" (point-max) t 1)
      (setq output-err-point (point))
      ;; errorのあるbufferをきめる
      (if (re-search-backward "(\\./\\(.+?\\)\\.\\(tex\\|sty\\)" (point-min) t 1)
          (progn
            (forward-char 1)
            (let ((stk 1))
              (while (and (> stk 0) (not (eobp)))
                (cond
                 ((looking-at-p "(") (setq stk (1+ stk)))
                 ((looking-at-p ")") (setq stk (1- stk))))
                (forward-char)))
            (cond
             ((and (> (point) output-err-point) (not (eobp)))
              (setq err-buf-name
                    (concat
                     (buffer-substring-no-properties
                      (match-beginning 1)  (match-end 1))
                     "."
                     (buffer-substring-no-properties
                      (match-beginning 2)  (match-end 2)))))
             (t
              (setq err-buf-name (concat tex-master-buf-name ".tex")))))
        (setq err-buf-name tex-master-buf-name))
      ;; error行の検索
      (goto-char output-err-point)
      (backward-char)
      (re-search-forward "\\(^!.+\\)")
      (setq s1 (buffer-substring-no-properties (match-beginning 1) (match-end 1)))
      (forward-line 1)
      (re-search-forward "\\(^.+\\)")
      (setq s1 (concat s1 " " (buffer-substring-no-properties (match-beginning 1) (match-end 1))))
      (forward-line -1)
      (setq err-line (string-to-number
                      (buffer-substring-no-properties
                       (re-search-forward "^l\\." nil 1 1)
                       (- (re-search-forward "[0-9] ") 1))))
      ;; (re-search-forward "\\(^.+\\)")
      (setq s1 (concat err-buf-name "|line " (number-to-string err-line) "|" s1 ))
      (add-to-list 'lst (list s1 err-buf-name err-line))
      (forward-line 1)
      )
    (setq lst (reverse lst))
    )
  )

(defun my-tex-error-message-reference-warnings-list (tex-master-buf-name)
  (let (s1 err-buf-name
           err-line output-err-point
           (lst nil))
    (goto-char (point-min))
    (while (re-search-forward "\\(LaTeX Warning:\\) \\(Citation\\|Reference\\)" (point-max) t 1)
      (setq output-err-point (match-beginning 1))
      ;; errorのあるbufferをきめる
      (if (re-search-backward "(\\./\\(.+?\\)\\.tex" (point-min) t 1)
          (progn
            (forward-char 1)
            (let ((stk 1))
              (while (and (> stk 0) (not (eobp)))
                (cond
                 ((looking-at-p "(") (setq stk (1+ stk)))
                 ((looking-at-p ")") (setq stk (1- stk)))
                 )
                (forward-char)
                ))
            (cond
             ((and (> (point) output-err-point) (not (eobp)))
              (setq err-buf-name (concat
                                  (buffer-substring-no-properties (match-beginning 1)  (match-end 1))
                                  ".tex")))
             (t
              (setq err-buf-name (concat tex-master-buf-name ".tex")))
             )
            )
        (setq err-buf-name (concat tex-master-buf-name ".tex"))
        )
      ;; error行の検索
      (goto-char output-err-point)
      (backward-char)
      (re-search-forward "\\(LaTeX Warning:\\) \\(Citation\\|Reference\\)")
      (save-excursion
        (let (st en)
          (when (and (progn (end-of-line) (forward-char -1) t)
                     (not (looking-at-p "\\.")))
            (setq st (point))
            (setq en (search-forward "." nil t))
            (cl-loop for i from 1 to (1- (count-lines st en))
                  do
                  (beginning-of-line) (delete-char -1))
            ))
        (beginning-of-line)
        (re-search-forward "\\(LaTeX Warning:\\) \\(Citation\\|Reference\\).*line \\([0-9]+\\).*\\.")
        )
      (setq s1 (buffer-substring-no-properties (match-beginning 1) (match-end 3)))
      (setq err-line (string-to-number (buffer-substring-no-properties (match-beginning 3) (match-end 3))))
      (setq s1 (concat err-buf-name "|line " (number-to-string err-line) "|" s1 ))
      (add-to-list 'lst (list s1 err-buf-name err-line))
      (forward-line 1)
      )
    (setq lst (reverse lst))
    )
  )

(defun my-tex-error-message-bad-boxes-list (tex-master-buf-name)
  (let (s1 err-buf-name
           err-line output-err-point
           (lst nil))
    (goto-char (point-min))
    (while (re-search-forward "\\(Over\\|Under\\)full.+at lines \\([0-9]+\\)--\\([0-9]+\\)" (point-max) t 1)
      (setq output-err-point (match-beginning 1))
      ;; errorのあるbufferをきめる
      (if  (re-search-backward "(\\./\\(.+?\\)\\.tex" (point-min) t 1)
          (progn
            (forward-char 1)
            (let ((stk 1))
              (while (and (> stk 0) (not (eobp)))
                (cond
                 ((looking-at-p "(") (setq stk (1+ stk)))
                 ((looking-at-p ")") (setq stk (1- stk)))
                 )
                (forward-char)
                ))
            (cond
             ((and (> (point) output-err-point) (not (eobp)))
              (setq err-buf-name (concat
                                  (buffer-substring-no-properties (match-beginning 1)  (match-end 1))
                                  ".tex")))
             (t
              (setq err-buf-name (concat
                                  tex-master-buf-name
                                  ".tex")))
             )
            )
        (setq err-buf-name (concat tex-master-buf-name ".tex"))
        )
      ;; error行の検索
      (goto-char output-err-point)
      (backward-char)
      (re-search-forward "\\(Over\\|Under\\)full.+at lines \\([0-9]+\\)--\\([0-9]+\\)")
      (setq s1 (buffer-substring-no-properties (match-beginning 1) (match-end 3)))
      (setq err-line
            ;; (/ (+ (string-to-number (buffer-substring-no-properties (match-beginning 2) (match-end 2)))
            ;;       (string-to-number (buffer-substring-no-properties (match-beginning 3) (match-end 3))))
            ;;    2)
            (string-to-number (buffer-substring-no-properties (match-beginning 3) (match-end 3)))
            )
      (setq s1 (concat err-buf-name "|line " (number-to-string err-line) "|" s1 ))
      (if (not (member (list s1 err-buf-name err-line) lst))
          (setq lst (cons (list s1 err-buf-name err-line) lst))
        )
      (forward-line 1)
      )
    (setq lst (reverse lst))
    ))

(defvar my-tex-error-message-default-window)
(make-local-variable 'my-tex-error-message-default-window)

;;;###autoload
(defun my-TeX-error-message-create-buffer ()
  (interactive)
  (let (lst lst1 (i 0))
    (setq lst1 (my-tex-error-only-error-message-list))
    (setq lst (nth 0 lst1))
    (if lst
        (progn
          (get-buffer-create "*LaTeX-error-message*")
          (pop-to-buffer "*LaTeX-error-message*")
          (setq my-tex-error-message-default-window
        popwin:selected-window)
          (my-TeX-error-message-mode)
          (erase-buffer)
          (insert (format "%s."
                          (my-tex-error-message-plural (length (nth 0 lst1)) "error")
                          ))
          (newline)
          (while (< i (length lst))
            (insert (nth 0 (nth i lst)))
            (insert "\n")
            (setq i (+ i 1)))
          (goto-char (point-min))
          (forward-line 1)
          )
      (when (called-interactively-p 'interactive)
        (message (format "%s." "no error"))))))

;;;###autoload
(defun my-tex-error-message-warning-create-buffer ()
  (interactive)
  (let (err war bad lst1 (i 0))
    (setq lst1 (my-tex-error-message-list))
    (setq err (nth 0 lst1))
    (setq war (nth 1 lst1))
    (setq bad (nth 2 lst1))
    (get-buffer-create "*LaTeX-warnings-bad-boxes-message*")
    (pop-to-buffer "*LaTeX-warnings-bad-boxes-message*")
    (setq my-tex-error-message-default-window
        popwin:selected-window)
    (my-TeX-error-message-mode)
    (setq mode-line-format '("%e"))
    (erase-buffer)
    (insert (format "%s, %s, %s."
                    (my-tex-error-message-plural (length (nth 0 lst1)) "error")
                    (my-tex-error-message-plural (length (nth 1 lst1)) "undefined reference")
                    (my-tex-error-message-plural (length (nth 2 lst1)) "bad hbox")
                          ))
    (newline)
    (while (< i (+ (length err)))
      (insert (nth 0 (nth i err)))
      (insert "\n")
      (setq i (+ i 1))
      )
    (setq i 0)
    (while (< i (+ (length war)))
      (insert (nth 0 (nth i war)))
      (insert "\n")
      (setq i (+ i 1))
      )
    (setq i 0)
    (while (< i (+ (length bad)))
      (insert (nth 0 (nth i bad)))
      (insert "\n")
      (setq i (+ i 1))
      )
    (goto-char (point-min))
    (forward-line 1)))

(defun my-tex-error-message-number-to-string (num)
  (cond
   ((equal num 0) (eval "no"))
   ((equal num 1) (eval "a"))
   ((> num 1) (number-to-string num))))

(defun my-tex-error-message-plural (n w)
  (cond
   ((and (equal w "bad hbox") (> n 1)) (concat (my-tex-error-message-number-to-string n) " " w "es"))
   ((> n 1) (concat (my-tex-error-message-number-to-string n) " " w "s"))
   ((and (string-match "[aiueo]" (substring w 0 1)) (equal "a" (my-tex-error-message-number-to-string n)))
    (concat (my-tex-error-message-number-to-string n) "n" " " w))
   (t (concat (my-tex-error-message-number-to-string n) " " w))))

;;;###autoload
(defun my-tex-error-message-pop-error-buffer ()
  (interactive)
  (let (buffer-name err-line dir (win (selected-window)))
    (save-excursion
      (beginning-of-line)
      (if (re-search-forward "\\(.+\\.\\(sty\|tex\\)\\)|line \\([0-9]+\\)" (point-max) t 1)
          (progn
            (setq dir default-directory)
            (setq buffer-name (buffer-substring-no-properties (match-beginning 1) (match-end 1)))
            (setq err-line (string-to-number (buffer-substring-no-properties (match-beginning 3) (match-end 3))))
            (select-window my-tex-error-message-default-window)
            (let* ((file (concat dir buffer-name)))
              (when (file-exists-p file)
                (find-file file)))
            (push-mark)
            (save-restriction
              (widen)
              (goto-char (point-min))
              (forward-line (1- err-line)))
            (recenter (/ (frame-height) 2))
            (select-window win))))))

;;;###autoload
(defun my-tex-error-message-pop-error-buffer-other-window ()
  (interactive)
  (let (buffer-name err-line dir)
    (save-excursion
      (beginning-of-line)
      (if (re-search-forward "\\(.+\\.\\(sty\\|tex\\)\\)|line \\([0-9]+\\)" (point-max) t 1)
        (progn
          (setq dir default-directory)
          (setq buffer-name (buffer-substring-no-properties (match-beginning 1) (match-end 1)))
          (setq err-line (string-to-number (buffer-substring-no-properties (match-beginning 3) (match-end 3))))
          (select-window my-tex-error-message-default-window)
          (let* ((file (concat dir buffer-name)))
            (when (file-exists-p file)
              (find-file file)))
          (push-mark)
          (save-restriction
            (widen)
            (goto-char (point-min))
            (forward-line (1- err-line)))
          (recenter (/ (frame-height) 2)))))))
;;;###autoload
(defun my-tex-error-message-kill-buffer-and-window ()
  (interactive)
  (kill-buffer-and-window)
  (select-window my-tex-error-message-default-window))
(define-derived-mode my-TeX-error-message-mode
  nil "LaTeX-error-message-mode")
(my-define-keys my-TeX-error-message-mode-map
  "m"    'my-tex-error-message-pop-error-buffer
  "SPC"  'my-tex-error-message-pop-error-buffer
  "j"    'next-line
  "k"    'previous-line
  "q"    'my-tex-error-message-kill-buffer-and-window
  "o"    'my-tex-error-message-pop-error-buffer-other-window
  "C-m"  'my-tex-error-message-pop-error-buffer-other-window)



(provide 'my-tex-error-message)
;; Local Variables:
;; coding: utf-8
;; End:

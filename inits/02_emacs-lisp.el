(require 'bind-key)
;; auto-byte-compile
;; (require 'auto-async-byte-compile)
;; (setq auto-async-byte-compile-exclude-files-regexp
;;       (rx (or "junk" "init.el" "inits" "env_dep" "themes"
;;               ".loaddefs.el"
;;               ".status.el"
;;               "el-get"
;;               "cache"
;;               "elpa")))
;; (add-hook 'emacs-lisp-mode-hook 'enable-auto-async-byte-compile-mode)
;; 16進数の列などに色をつける．
(add-hook 'emacs-lisp-mode-hook 'rainbow-mode)
;; paredit
(require 'paredit)
(add-hook 'emacs-lisp-mode-hook 'enable-paredit-mode)
(add-hook 'lisp-interaction-mode-hook 'enable-paredit-mode)
(add-hook 'lisp-mode-hook 'enable-paredit-mode)
(my-define-keys paredit-mode-map
  "C-n"      #'paredit-backward-delete
  "C-j"      #'next-line
  "C-k"      #'previous-line
  "M-s"      nil
  "C-v"      #'paredit-newline
  "C-p"      #'paredit-kill
  "C-q C-p"  #'paredit-split-sexp
  "C-q C-f"  #'paredit-wrap-round
  "C-q C-c"  #'paredit-convolute-sexp
  "C-q C-t"  #'paredit-split-sexp
  "C-q C-j"  #'paredit-join-sexps
  "C-q C-d"  #'paredit-splice-sexp
  "C-M-l"    #'paredit-forward
  "C-M-h"    #'paredit-backward
  "C-M-n"    #'scroll-other-window
  "C-M-p"    #'scroll-other-window-down
  ";"        nil
  "M-r"      nil)
(defun my:paredit-down (arg)
  (interactive "p")
  (unless (ignore-errors
            (paredit-forward-down arg)
            (when (eq (char-after) ?\()
              (skip-chars-forward "( \n"))
            t)
    (paredit-forward-up arg)
    (when (eq (char-after) ?\))
      (skip-chars-forward ") \n"))))

(defun my:paredit-up (arg)
  (interactive "p")
  (unless (ignore-errors
            (paredit-backward-down arg)
            (when (eq (char-before) ?\))
              (skip-chars-backward ") \n"))
            t)
    (paredit-backward-up arg)
    (when (eq (char-before) ?\()
      (skip-chars-backward "( \n"))))

(smartrep-define-key paredit-mode-map "C-q"
  '(("C-k" . #'paredit-forward-up)
    ("k" . #'paredit-forward-up)
    ("C-j" . #'paredit-forward-down)
    ("j" . #'paredit-forward-down)
    ("p" . #'paredit-backward-up)
    ("n" . #'paredit-backward-down)
    ("l" . #'paredit-forward)
    ("C-l" . #'paredit-forward)
    ("C-M-l" . #'paredit-forward)
    ("C-M-h" . #'paredit-backward)
    ("C-h" . #'paredit-backward)
    ("h" . #'paredit-backward)
    ("C-;" . #'recenter-top-bottom)
    (";" . #'recenter-top-bottom)))
;; eldoc
(add-hook 'emacs-lisp-mode-hook 'turn-on-eldoc-mode)
(add-hook 'lisp-interaction-mode 'turn-on-eldoc-mode)
(setq eldoc-minor-mode-string "")

;; 関数のかきかえ
;; (defun ac-emacs-lisp-mode-setup ()
;;   (setq ac-sources
;;         (append '(ac-source-features
;;                   ac-source-yasnippet
;;                   ac-source-functions
;;                   ac-source-variables
;;                   ac-source-symbols)
;;                 ac-sources)))
(my-add-hook emacs-lisp-mode-hook
  ;; syntax table : を wordに
  (modify-syntax-entry ?: "w")
  (setq-local company-backends
              '((company-capf company-dabbrev-code company-gtags company-etags company-keywords)
                company-files)))

(my-add-hook lisp-interaction-mode-hook
  ;; syntax table : を wordに
  (modify-syntax-entry ?: "w"))

;; scratch buffer 評価結果が省略されないように
(setq eval-expression-print-length nil)
(key-chord-define emacs-lisp-mode-map "ev" 'eval-buffer)
(key-chord-define lisp-interaction-mode-map "ev" 'eval-buffer)
(key-chord-define emacs-lisp-mode-map
                  "rj" (lambda () (interactive) (insert "#'")))
(key-chord-define lisp-interaction-mode-map
                  "rj" (lambda () (interactive) (insert "#'")))
(define-key lisp-interaction-mode-map (kbd "C-j") 'next-line)
(define-key lisp-interaction-mode-map (kbd "C-m") 'eval-print-last-sexp)
(define-key emacs-lisp-mode-map (kbd "C-M-i")
  'helm-lisp-completion-at-point)
(define-key lisp-interaction-mode-map (kbd "C-M-i")
  'helm-lisp-completion-at-point)

;; (add-hook 'emacs-lisp-mode-hook #'elisp-slime-nav-mode)
;; (use-package elisp-slime-nav
;;   :bind (("C-c C-l d" . elisp-slime-nav-describe-elisp-thing-at-point)
;;          ("C-c C-l C-d" . elisp-slime-nav-describe-elisp-thing-at-point)
;;          :map elisp-slime-nav-mode-map)
;;   :config
;;   (define-key elisp-slime-nav-mode-map
;;     (kbd "C-c C-d") nil))
;; ;; unit test
;; (require 'el-expectations)
;; (require 'el-mock)

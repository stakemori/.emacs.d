;; hjklでウィンドウサイズ変更
(defun window-resizer ()
  "Control window size and position."
  (interactive)
  (let ((window-obj (selected-window))
        (current-width (window-width))
        (current-height (window-height))
        (dx (if (= (nth 0 (window-edges)) 0) 1
              -1))
        (dy (if (= (nth 1 (window-edges)) 0) 1
              -1))
        c)
    (catch 'end-flag
      (while t
        (message "size[%dx%d]"
                 (window-width) (window-height))
        (setq c (read-char))
        (cond ((= c ?l)
               (enlarge-window-horizontally dx))
              ((= c ?h)
               (shrink-window-horizontally dx))
              ((= c ?j)
               (enlarge-window dy))
              ((= c ?k)
               (shrink-window dy))
              ;; otherwise
              (t
               (message "Quit")
               (throw 'end-flag t)))))))

;; scratch buffer消さない
(defun my-make-scratch ()
  ;; "*scratch*" を作成して buffer-list に放り込む
  (set-buffer (get-buffer-create "*scratch*"))
  (funcall initial-major-mode)
  (erase-buffer)
  (awhen (and (not inhibit-startup-message)
             initial-scratch-message)
    (insert it)))
(defun my-buffer-name-list ()
  (mapcar (function buffer-name) (buffer-list)))
;; *scratch* バッファで kill-buffer したら内容を消去するだけにする
(add-hook 'kill-buffer-query-functions 'scratch-buffer-kill-query-function)
(defun scratch-buffer-kill-query-function ()
  (if (string= "*scratch*" (buffer-name))
      (progn (my-make-scratch) nil)
    t))

;; startup messageださない．scratch bufferのmessageださない
;; (setq inhibit-startup-message t)
(setq initial-scratch-message nil)

;; scratch buffer fileに保存
(defvar my-sractch-cache-file (locate-user-emacs-file ".scratch"))
(defun my-save-scratch ()
  (awhen (get-buffer "*scratch*")
    (set-buffer it)
    (write-region nil nil my-sractch-cache-file nil 'silent)))
(add-hook 'kill-emacs-hook 'my-save-scratch)
(defun my-read-scratch ()
  (awhen (and (file-exists-p my-sractch-cache-file)
              (get-buffer "*scratch*"))
    (with-current-buffer it
      (insert-file-contents my-sractch-cache-file)
      (setq default-directory "~/"))))
(add-hook 'after-init-hook 'my-read-scratch)

;; minibufferの入力C-gでキャンセルしたとき，履歴にのこす.M-pでみれる
(defadvice abort-recursive-edit (before minibuffer-save activate)
  (when (eq (selected-window) (active-minibuffer-window))
    (add-to-history minibuffer-history-variable (minibuffer-contents))))

;; kill-comment-region
(defun kill-comment-region (bgn end)
  (interactive "r")
  (save-excursion
    (if (not (mark t))
        (kill-comment 1)
      (goto-char bgn)
      (kill-comment (count-lines bgn end)))))

(defun my-desktop-clear (arg)
  (interactive "p")
  (let ((regexp-lst
         (list
          "\\*grep\\*"
          (rx "*" "Sage help" "*")
          (rx "*ag" (1+ anything) "*")
          (rx "*" (1+ anything) ".git*")
          (rx "*Egg" (zero-or-more anything) "*")
          (rx "*magit" (zero-or-more anything) "*")
          (rx "*woman" (zero-or-more anything) "*")
          (rx "*moccur" (zero-or-more anything) "*")
          (rx "*ediff-" (zero-or-more anything) "*")
          (rx "*Completions" (zero-or-more anything) "*")
          (rx "*Compile-log" (zero-or-more anything) "*")
          (rx "*Async Shell" (zero-or-more anything) "*")
          (rx "*" (zero-or-more anything) "output*")
          (rx "*vc" (zero-or-more anything) "*")
          (rx "*Man" (zero-or-more anything) "*")
          (rx "*Ibuffer" (zero-or-more anything) "*")
          (rx "*Occur" (zero-or-more anything) "*")
          (rx "*Help*")
          (rx "*Directory for" (zero-or-more anything))
          (rx "*LaTeX-warnings-bad-boxes-message*")
          (rx "*compilation*")
          (rx "*python-pep8*")
          (rx (1+ nonl) "TeX" (0+ nonl) "output*")
          (rx "magit" (1+ nonl))
          (rx "TAGS")))
        (major-mode-list '(dired-mode fundamental-mode shell-mode))
        (not-erase-regexp-list
         (list " \\*" "anything" "\\*Messages\\*"
               "\\*nrepl-server\\*"
               "\\*nrepl-connection\\*"
               "\\*nrepl\\*"
               "\\*GNU Emacs\\*"
               "anaconda")))

    (mapc (lambda (b)
            (when (and
                   (cl-loop for r in not-erase-regexp-list
                         never (string-match r (buffer-name b)))
                   (or (cl-loop for r in regexp-lst
                             thereis (string-match r (buffer-name b)))
                       (cl-loop for m in major-mode-list
                             thereis (and (get-buffer b)
                                          (with-current-buffer b
                                            (equal major-mode m)))))
                   (let ((win (get-buffer-window b)))
                     (or (and (null win) (equal arg 4))
                         (not (window-parameter win 'no-delete-other-window)))))
              (kill-buffer b))) (buffer-list))))

;; command
(defun copy-line** ()
  ;; bgn : back-to-indentation, end : end-of-line
  (interactive)
  (let ((bgn 0) (end 0))
    (save-excursion
      (back-to-indentation)
      (setq bgn (point))
      (end-of-line)
      (setq end (point))
      (kill-ring-save bgn end))))

;; save時にdelete-trairing-whitespace
(defun my-before-save-function ()
  (save-excursion
    (save-restriction
      (narrow-to-region
       (point-min)
       (progn (forward-line -1) (line-end-position)))
      (delete-trailing-whitespace)))
  (save-excursion
    (save-restriction
      (narrow-to-region
       (progn (forward-line 1) (point)) (point-max))
      (delete-trailing-whitespace)
      (widen)
      ;; (untabify (point-min) (point-max))
      )))
(my-named-add-hook before-save-hook
  (when (derived-mode-p 'prog-mode)
    (my-before-save-function)))

(defun sort-by (lst fn)
  (sort lst (lambda (x y) (< (funcall fn x) (funcall fn y)))))

(defun my:set-mykey! (mp)
  (cl-loop for (s1 . s2) in
           '(("\C-j" . "\C-v") ("\C-k" . "\C-p"))
           with f = (lambda (s) (string-to-char s))
           for c1 = (funcall f s1)
           for c2 = (funcall f s2)
           if (assoc-default c1 mp)
           do
           (define-key mp s1 nil)
           (define-key mp s2 it)))

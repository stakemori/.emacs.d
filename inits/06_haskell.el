(add-to-list 'auto-mode-alist '("\\.hs$" . haskell-mode))
(add-to-list 'auto-mode-alist '("\\.lhs$" . literate-haskell-mode))
(add-to-list 'auto-mode-alist '("\\.cabal\\'" . haskell-cabal-mode))
(add-to-list 'interpreter-mode-alist '("runghc" . haskell-mode))
;; ;; ghc-modはaptでいれた
;; (autoload 'ghc-init "ghc" nil t)

;; (my-add-hook haskell-mode-hook
;;   )

;; sage-modeの関数かきなおし
(defun my-sage-pcomplete-or-help ()
  "If point is after ?, describe preceding symbol; otherwise, pcomplete."
  (interactive)
  (let ((help-bufn "*SAGE-Help*"))
    (if (not (looking-back "[^\\?]\\?"))
        (pcomplete)
      (let ((word (save-excursion (backward-char) (python-current-word))))
        (when word
          (get-buffer-create help-bufn)
          (with-current-buffer (get-buffer help-bufn)
            (view-mode 0)
            (erase-buffer))
          (comint-redirect-send-command-to-process
           (concat word "?") help-bufn (current-buffer) nil t)
          (save-excursion
            (set-buffer help-bufn)
            (while (bobp)
              (sit-for 0.3))
            (goto-char (point-min)))
          (pop-to-buffer help-bufn)
          )))))

(provide 'my-inferior-sage)
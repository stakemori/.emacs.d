(setq package-user-dir
      (let ((elpa-dir-name (format "elpa_%s" emacs-major-version))) ;default = ~/.emacs.d/elpa/
        (file-name-as-directory
         (expand-file-name elpa-dir-name user-emacs-directory))))

(tool-bar-mode -1)
(menu-bar-mode -1)

;; scroll bar off
(scroll-bar-mode -1)
(when (fboundp 'horizontal-scroll-bar-mode)
  (horizontal-scroll-bar-mode -1))
(setq inhibit-startup-screen t)

(setq package-quickstart t)

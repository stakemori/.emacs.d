;; yasnippet
(my/with-eval-after-load-prog-mode yas
  (yas-global-mode 1))

(global-set-key (kbd "C-x y") #'aya-create)
(global-set-key (kbd "C-x C-y") #'aya-expand)
(push (cons "\\.yasnippet" 'snippet-mode) auto-mode-alist)
(global-set-key (kbd "C-c y") 'yas-insert-snippet)
(setq yas-snippet-dirs '("~/.emacs.d/snippets"))

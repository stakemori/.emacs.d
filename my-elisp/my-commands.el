(require 'my-utilities)
(require 'f)
(eval-when-compile (require 'cl))

;;;###autoload
(defun insert-_ ()
  (interactive)
  (insert "_{}")
  (forward-char -1))

;;;###autoload
(defun insert-hat ()
  (interactive)
  (insert "^{}")
  (forward-char -1))

;;;###autoload
(defun insert-equation* ()
  (interactive)
  (if (region-active-p)
      (let ((beg (min (region-beginning) (region-end)))
            (end (max (region-beginning) (region-end))))
        (save-excursion
          (goto-char beg) (insert "\\begin{equation*}") (newline)
          (setq end (+ end (length "\\begin{equation*}") 1))
          (goto-char end) (newline) (insert "\\end{equation*}")
          (setq end (+ end (length "\\end{equation*}")))
          (indent-region beg end)
          ))
      (let (p)
        (insert "\\begin{equation*}")
        (newline-and-indent)
        (setq p (point))
        (newline)
        (insert "\\end{equation*}")
        (indent-for-tab-command)
        (goto-char p))))

;;;###autoload
(defun insert-displaymath ()
  (interactive)
  (if (region-active-p)
      (let ((beg (min (region-beginning) (region-end)))
            (end (max (region-beginning) (region-end))))
        (save-excursion
          (goto-char beg) (insert "\\begin{displaymath}") (newline)
          (setq end (+ end (length "\\begin{displaymath}") 1))
          (goto-char end) (newline) (insert "\\end{displaymath}")
          (setq end (+ end (length "\\end{displaymath}")))
          (indent-region beg end)
          ))
      (let (p)
        (insert "\\begin{displaymath}")
        (newline-and-indent)
        (setq p (point))
        (newline)
        (insert "\\end{displaymath}")
        (indent-for-tab-command)
        (goto-char p))))

;;;###autoload
(defun insert-doll ()
  (interactive)
  (if (region-active-p)
      (let ((beg (min (region-beginning) (region-end)))
            (end (max (region-beginning) (region-end))))
        (save-excursion
          (goto-char end) (insert "$")
          (goto-char beg) (insert "$")))
    (save-excursion
      (insert "$")
      (insert "$"))
    (forward-char 1)))

;; 括弧類の挿入
;;;###autoload
(defun insert-braces ()
  (interactive)
  (if (region-active-p)
      (let ((beg (min (region-beginning) (region-end)))
            (end (max (region-beginning) (region-end))))
        (save-excursion
          (goto-char end) (insert "}")
          (goto-char beg) (insert "{")))
    (save-excursion
      (insert "{")
      (insert "}"))
    (forward-char 1)))

;;;###autoload
(defun insert-left-right-braces ()
  (interactive)
  (if (region-active-p)
      (let ((beg (min (region-beginning) (region-end)))
            (end (max (region-beginning) (region-end))))
        (save-excursion
          (goto-char end) (insert " \\right\\}")
          (goto-char beg) (insert "\\left\\{ ")))
    (save-excursion
      (insert "\\left\\{")
      (insert "\\right\\}")
      )
    (forward-char 7)))
;;;###autoload
(defun insert-left-right-paren ()
  (interactive)
  (if (region-active-p)
      (let ((beg (min (region-beginning) (region-end)))
            (end (max (region-beginning) (region-end))))
        (save-excursion
          (goto-char end) (insert " \\right)")
          (goto-char beg) (insert "\\left( ")))
    (save-excursion
      (insert "\\left(")
      (insert "\\right)")
      )
    (forward-char 6)))


;;;###autoload
(defun insert-paren ()
  (interactive)
  (if (region-active-p)
      (let ((beg (min (region-beginning) (region-end)))
            (end (max (region-beginning) (region-end))))
        (save-excursion
          (goto-char end) (insert ")")
          (goto-char beg) (insert "(")))
    (save-excursion
      (insert "(")
      (insert ")"))
    (forward-char 1)
    ))
;;;###autoload
(defun insert-brackets ()
  (interactive)
  (if (region-active-p)
      (let ((beg (min (region-beginning) (region-end)))
            (end (max (region-beginning) (region-end))))
        (save-excursion
          (goto-char end) (insert "]")
          (goto-char beg) (insert "[")))
    (save-excursion
      (insert "[")
      (insert "]"))
    (forward-char 1)
    ))

;;;insert \
;;;###autoload
(defun insert-yen nil
  (interactive)
  (insert "\\"))
;;;###autoload
(defun insert-spc nil
  (interactive )
  (insert " "))
;;;###autoload
(defun insert-| nil
  (interactive )
  (insert "\|"))
;;;###autoload
(defun insert-and nil
  (interactive )
  (insert "\&"))
;;;###autoload
(defun insert-= nil
  (interactive )
  (insert "="))
;;;###autoload
(defun insert-prime nil
  (interactive )
  (insert "'"))
;;;###autoload
(defun insert-plus nil
  (interactive )
  (insert "+"))
;;;###autoload
(defun insert-minus nil
  (interactive )
  (insert "-"))
;;;###autoload
(defun insert-star nil
  (interactive )
  (insert "*"))
;;;###autoload
(defun insert-backslash nil
  (interactive )
  (insert "/"))
;;;###autoload
(defun insert-bigm| nil
  (interactive )
  (insert "\\bigm |"))

;; (defun insert-hat nil
;;   (interactive )
;;   (insert "^"))
;; (defun insert-_ nil
;;   (interactive )
;;   (insert "_"))

;;;###autoload
(defun my-mark-tex-math-inner ()
  (interactive)
  (let ((s nil) p q)
    (save-excursion
      (setq p (point))
      (search-backward "\\begin{document}" (point-min) 1 1)
      (while (< (point) p)
        (cond
         ((and (looking-at-p "\\$") (not s)) (setq s (point)))
         ((and (looking-at-p "\\$") s) (setq s nil)))
        (forward-char)))
    (if (and s (re-search-forward "\\$" (point-max) t 1))
          (progn
            (setq q (- (point) 1))
            (goto-char (+ s 1))
            (set-mark q)
            ))))
;;;###autoload
(defun my-kill-tex-math ()
  (interactive)
  (let ( (s nil) p q)
    (save-excursion
      (setq p (point))
      (search-backward "\\begin{document}" (point-min) 1 1)
      (while (< (point) p)
        (cond
         ((and (looking-at-p "\\$") (not s)) (setq s (point)))
         ((and (looking-at-p "\\$") s) (setq s nil)))
        (forward-char)))
  (if (and s (re-search-forward "\\$" (point-max) t 1))
          (progn
            (kill-region (point)  s)
            ))))

;;;###autoload
(defun my-kill-ring-save-tex-math-inner ()
  (interactive)
  (let ( (s nil) p q)
    (save-excursion
      (setq p (point))
      (search-backward "\\begin{document}" (point-min) 1 1)
      (while (< (point) p)
        (cond
         ((and (looking-at-p "\\$") (not s)) (setq s (point)))
         ((and (looking-at-p "\\$") s) (setq s nil)))
        (forward-char))
      (if (and s (re-search-forward "\\$" (point-max) t 1))
          (progn
            (kill-ring-save ( - (point) 1) (+ s 1)))))))

;; shell のfind簡易版
;;;###autoload
(defun my-shell-command-find-file (flname)
  (interactive
   (list (read-from-minibuffer "file name (whole match): ")))
  (shell-command (format "find . -name '%s'&" flname)))

;;;###autoload
(defun my-collect-key-defines (beg end)
  (interactive "r")
  (save-excursion
    (save-restriction
      (narrow-to-region beg end)
      (goto-char (point-min))
      (let (key cmd lst)
        (while (re-search-forward
                (rx "\"" (group (1+ (not (any "\"")))) "\"") nil t)
          (setq key (match-string 1))
          (re-search-forward (rx (group "'" (1+ (not whitespace))) ")") nil t)
          (setq cmd (match-string 1))
          (push (cons key cmd) lst))
        (widen)
        (with-temp-buffer
          (cl-loop for (key . cmd) in (nreverse lst)
                do
                (insert (format "\"%s\" " key))
                (insert cmd)
                (newline))
          (copy-region-as-kill (point-min) (point-max)))))))

;;;###autoload
(defun insert-%-- ()
  (interactive)
  (let* ((col (current-column))
         (str (concat "% " (make-string (- 73 col) ?-))))
    (insert str)))

(defun my-add-info-path (&rest paths)
  (cl-loop for p in paths
        do
        (add-to-list 'Info-default-directory-list
                     (expand-file-name p))
        finally return Info-default-directory-list))

;;;###autoload
(defun my:replace-symbol-at-point (rpl)
  (interactive
   (list (query-replace-read-to
          (my:-replace-symbol-regexp-at-point)
          "Replace" t)))
  (let ((case-fold-search nil))
    (save-excursion
      (with-no-warnings
        (replace-regexp
         (my:-replace-symbol-regexp-at-point)
         rpl
         nil
         (save-excursion
           (beginning-of-defun)
           (point))
         (save-excursion
           (end-of-defun)
           (point)))))))

(defun my:-replace-symbol-regexp-at-point ()
  (concat "\\_<" (thing-at-point 'symbol) "\\_>"))

;;; my:copy-file-name
;;;###autoload
(defun my:copy-file-name (a)
  (interactive "P")
  (aif (buffer-file-name (current-buffer))
      (if a
          (kill-new (f-parent it))
        (kill-new it))))
(global-set-key (kbd "C-c w") 'my:copy-file-name)

;;;###autoload
(defun open-in-atom (f)
  (interactive
   (list (read-file-name
          "File: " nil
          (aif (buffer-file-name) it))))
  (call-process
   (or (executable-find "atom")
       (executable-find "atom-beta"))
   nil nil nil
                (file-name-nondirectory f)))

;;;###autoload
(defun open-in-vs-code (f)
  (interactive
   (list (read-file-name
          "File: " nil
          (aif (buffer-file-name) it))))
  (deferred:process "code"
    (file-name-nondirectory f)))


;; register
(defvar clipboard-register ?@)

(defadvice kill-region (before clipboard-cut activate)
  (when (eq last-command this-command)
    (set-register clipboard-register (car kill-ring))
    (message "Copy to clipboard")))

(defadvice copy-region-as-kill (before clipboard-copy activate)
  (when (eq last-command this-command)
    (set-register clipboard-register (car kill-ring))
    (message "Copy to clipboard")))

;;;###autoload
(defun clipboard-paste ()
  (interactive)
  (insert-register clipboard-register))

;; recenter 0
;;;###autoload
(defun line-to-top-of-window ()
  (interactive) (recenter 0))


;;;###autoload
(defun my-open-text-buffer ()
  (interactive)
  (switch-to-buffer (get-buffer-create "*text*"))
  (text-mode))

;; other-window
;;;###autoload
(defun other-window-or-split ()
  (interactive)
  (when (one-window-p)
    (split-window-horizontally))
  (other-window 1))

(provide 'my-commands)
;; Local Variables:
;; coding: utf-8
;; End:

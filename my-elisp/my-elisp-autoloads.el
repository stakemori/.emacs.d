;;; foo-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "blank-dirs" "blank-dirs.el" (21882 14317 455066
;;;;;;  933000))
;;; Generated autoloads from blank-dirs.el

(autoload 'delete-empty-package-dirs "blank-dirs" "\


\(fn)" t nil)

;;;***

;;;### (autoloads nil "my-TeX-commands" "my-TeX-commands.el" (22009
;;;;;;  5385 731939 746000))
;;; Generated autoloads from my-TeX-commands.el

(autoload 'tex-looking-back-command "my-TeX-commands" "\


\(fn)" nil nil)

(autoload 'guess-TeX-master "my-TeX-commands" "\
Guess the master file for FILENAME from currently open .tex files.

\(fn FILENAME)" nil nil)

(autoload 'my-TeX-open-master-file "my-TeX-commands" "\


\(fn)" t nil)

(autoload 'my-TeX-parse-package "my-TeX-commands" "\


\(fn FILE)" nil nil)

(autoload 'my-TeX-parse-environment "my-TeX-commands" "\


\(fn FILE)" nil nil)

(autoload 'my-TeX-auto-parse "my-TeX-commands" "\


\(fn)" t nil)

(autoload 'my-TeX-replace-hankaku-commas-in-this-buffer "my-TeX-commands" "\


\(fn BEG END)" t nil)

(autoload 'my-TeX-replace-hankaku-commas-dired "my-TeX-commands" "\


\(fn)" t nil)

(autoload 'my-TeX-replace-hankaku-commas "my-TeX-commands" "\


\(fn FILE &optional B E SILENT)" nil nil)

(autoload 'my-TeX-inside-label-or-ref-p "my-TeX-commands" "\


\(fn)" t nil)

(autoload 'my-TeX-inside-dai-kakko-p "my-TeX-commands" "\


\(fn)" t nil)

(autoload 'my-TeX-comment-out-preamble "my-TeX-commands" "\


\(fn &optional BUF-F-N)" t nil)

(autoload 'my-TeX-comment-out-preamble-dired-marked-files "my-TeX-commands" "\


\(fn)" t nil)

(autoload 'my-TeX-investigate-multiply-defined-labels-write "my-TeX-commands" "\


\(fn)" t nil)

(autoload 'my-TeX-investigate-multiply-defined-labels-list-funct "my-TeX-commands" "\


\(fn)" nil nil)

(autoload 'my-TeX-investigate-multiply-defined-labels-in-a-file "my-TeX-commands" "\


\(fn LABEL &optional BUFFLNM)" nil nil)

(autoload 'my-TeX-katakana-search "my-TeX-commands" "\


\(fn)" t nil)

(autoload 'my-TeX-clean "my-TeX-commands" "\
my-TeX-clean:
Delete generated files associated with current master and region files.
If prefix ARG is non-nil, not only remove intermediate but also
output files.

\(fn &optional ARG)" t nil)

(autoload 'mt:check-label-referred "my-TeX-commands" "\


\(fn)" t nil)

;;;***

;;;### (autoloads nil "my-commands" "my-commands.el" (22175 9374
;;;;;;  683569 7000))
;;; Generated autoloads from my-commands.el

(autoload 'insert-_ "my-commands" "\


\(fn)" t nil)

(autoload 'insert-hat "my-commands" "\


\(fn)" t nil)

(autoload 'insert-equation* "my-commands" "\


\(fn)" t nil)

(autoload 'insert-displaymath "my-commands" "\


\(fn)" t nil)

(autoload 'insert-doll "my-commands" "\


\(fn)" t nil)

(autoload 'insert-braces "my-commands" "\


\(fn)" t nil)

(autoload 'insert-left-right-braces "my-commands" "\


\(fn)" t nil)

(autoload 'insert-left-right-paren "my-commands" "\


\(fn)" t nil)

(autoload 'insert-paren "my-commands" "\


\(fn)" t nil)

(autoload 'insert-brackets "my-commands" "\


\(fn)" t nil)

(autoload 'insert-yen "my-commands" "\


\(fn)" t nil)

(autoload 'insert-spc "my-commands" "\


\(fn)" t nil)

(autoload 'insert-| "my-commands" "\


\(fn)" t nil)

(autoload 'insert-and "my-commands" "\


\(fn)" t nil)

(autoload 'insert-= "my-commands" "\


\(fn)" t nil)

(autoload 'insert-prime "my-commands" "\


\(fn)" t nil)

(autoload 'insert-plus "my-commands" "\


\(fn)" t nil)

(autoload 'insert-minus "my-commands" "\


\(fn)" t nil)

(autoload 'insert-star "my-commands" "\


\(fn)" t nil)

(autoload 'insert-backslash "my-commands" "\


\(fn)" t nil)

(autoload 'insert-bigm| "my-commands" "\


\(fn)" t nil)

(autoload 'my-mark-tex-math-inner "my-commands" "\


\(fn)" t nil)

(autoload 'my-kill-tex-math "my-commands" "\


\(fn)" t nil)

(autoload 'my-kill-ring-save-tex-math-inner "my-commands" "\


\(fn)" t nil)

(autoload 'my-shell-command-find-file "my-commands" "\


\(fn FLNAME)" t nil)

(autoload 'my-collect-key-defines "my-commands" "\


\(fn BEG END)" t nil)

(autoload 'insert-%-- "my-commands" "\


\(fn)" t nil)

(autoload 'my:replace-symbol-at-point "my-commands" "\


\(fn RPL)" t nil)

(autoload 'my:copy-file-name "my-commands" "\


\(fn A)" t nil)

(autoload 'open-in-atom "my-commands" "\


\(fn F)" t nil)

(autoload 'open-in-vs-code "my-commands" "\


\(fn F)" t nil)

(autoload 'clipboard-paste "my-commands" "\


\(fn)" t nil)

(autoload 'line-to-top-of-window "my-commands" "\


\(fn)" t nil)

(autoload 'my-open-text-buffer "my-commands" "\


\(fn)" t nil)

(autoload 'other-window-or-split "my-commands" "\


\(fn)" t nil)

;;;***

;;;### (autoloads nil "my-tex-error-message" "my-tex-error-message.el"
;;;;;;  (21882 14317 459067 6000))
;;; Generated autoloads from my-tex-error-message.el

(autoload 'my-TeX-error-message-create-buffer "my-tex-error-message" "\


\(fn)" t nil)

(autoload 'my-tex-error-message-warning-create-buffer "my-tex-error-message" "\


\(fn)" t nil)

(autoload 'my-tex-error-message-pop-error-buffer "my-tex-error-message" "\


\(fn)" t nil)

(autoload 'my-tex-error-message-pop-error-buffer-other-window "my-tex-error-message" "\


\(fn)" t nil)

(autoload 'my-tex-error-message-kill-buffer-and-window "my-tex-error-message" "\


\(fn)" t nil)

;;;***

;;;### (autoloads nil "my-utilities" "my-utilities.el" (22009 5385
;;;;;;  731939 746000))
;;; Generated autoloads from my-utilities.el

(autoload 'append-elements "my-utilities" "\
Add ELMTS to the tail of ORIGSEQ and set ORIGSEQ the result.

\(fn ORIGSEQ &rest ELMTS)" nil t)

(function-put 'append-elements 'lisp-indent-function '1)

(autoload 'macexpand "my-utilities" "\


\(fn EXPR)" nil t)

(autoload 'ansetq "my-utilities" "\
Anaphoric setq. REST is a list of sym val sym1 val1... `it' is the value of last sym

\(fn &rest REST)" nil t)

(autoload 'group "my-utilities" "\


\(fn L &optional N)" nil nil)

(autoload 'my-composite-function "my-utilities" "\
When funcs is '('f1 'f2 ... 'fn), returns a composite function x |-> fn . fn-1 . ... . f1(x).

\(fn &rest FUNCS)" nil t)

(autoload 'my-add-to-list "my-utilities" "\


\(fn LIST &rest ELEMENTS)" nil t)

(function-put 'my-add-to-list 'lisp-indent-function '1)

(autoload 'case* "my-utilities" "\
Like case, but compared by `eqaul'

\(fn EXPR &rest CLAUSES)" nil t)

(function-put 'case* 'lisp-indent-function '1)

(defsubst list-prod (&rest lists) (reduce 'list-prod2 lists :from-end t :initial-value '(nil)))

(defsubst list-prod-n (list n) (let ((lists (make-list n list))) (apply 'list-prod lists)))

(autoload 'get-value "my-utilities" "\


\(fn FUNC-OR-VAL &rest ARGS)" nil nil)

(autoload 'file:write "my-utilities" "\


\(fn CTS FILE)" nil nil)

(autoload 'file:read "my-utilities" "\


\(fn FILE)" nil nil)

(autoload 'my:not-required-packages "my-utilities" "\


\(fn)" nil nil)

(autoload 'cart-prod "my-utilities" "\


\(fn &rest ARGS)" nil nil)

(autoload 'some->> "my-utilities" "\


\(fn X FORM &rest FORMS)" nil t)

(autoload 'my:lazy-do "my-utilities" "\


\(fn &rest BODY)" nil t)

(function-put 'my:lazy-do 'lisp-indent-function '0)

(autoload 'my:mouse-keys-non-prefix "my-utilities" "\


\(fn)" nil nil)

(autoload 'my:mouse-keys "my-utilities" "\


\(fn)" nil nil)

(autoload 'my:shell-aysnc "my-utilities" "\


\(fn NAME CMD OUTPUT-BUF)" nil nil)

;;;***

;;;### (autoloads nil "tex-newcommand-load-yasnippet" "tex-newcommand-load-yasnippet.el"
;;;;;;  (21882 14317 459067 6000))
;;; Generated autoloads from tex-newcommand-load-yasnippet.el

(autoload 'TeX-newcommand-yas-reload "tex-newcommand-load-yasnippet" "\


\(fn &optional PATH-TEX-STY-FOLDER)" t nil)

;;;***

;;;### (autoloads nil "yascommand" "yascommand.el" (22008 14873 272117
;;;;;;  575000))
;;; Generated autoloads from yascommand.el

(autoload 'insert-norm "yascommand" "\


\(fn)" t nil)

(autoload 'insert-set "yascommand" "\


\(fn)" t nil)

(autoload 'insert-get "yascommand" "\


\(fn)" t nil)

(autoload 'insert-bars "yascommand" "\


\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("anything-exuberant-ctags-lang-c.el" "anything-grep-lang-c.el"
;;;;;;  "anything-magma.el" "helm-pdf-manuscripts.el" "japanese-color.el"
;;;;;;  "my-file-buffers.el" "my-git-repos.el" "my-imenu-tree.el"
;;;;;;  "my-inferior-sage.el" "my-ispell-buffer.el" "my-modeline.el"
;;;;;;  "my-monokai.el" "my-windows.el" "py-imenu.el") (22125 52988
;;;;;;  811399 448000))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
(provide 'my-elisp-autoloads)
;;; foo-autoloads.el ends here

;; ibuffer
(autoload 'ibuffer "ibuffer" "List buffers." t)
(require 'ibuffer)
(setq ibuffer-never-show-predicates
      (list "\*anything.+"  "\*Pymacs"  "\*sdic"
            "\*vc" "\*buffer" "\*.*TeX.*output\\*"
            "\*Compile-Log" "*Shell Command" "\*Messages"
            "\*Completions" "\*GNU Emacs"
            (rx "*" (or "helm" "Helm"))
            (rx "*magit")
            (rx "*epc con")))
(my-add-hook ibuffer-mode-hook
  (define-key ibuffer-mode-map (kbd "k") 'ibuffer-backward-line)
  (define-key ibuffer-mode-map (kbd "j") 'ibuffer-forward-line)
  (define-key ibuffer-mode-map (kbd "C-j") 'ibuffer-forward-line)
  (define-key ibuffer-mode-map (kbd "K") 'ibuffer-do-kill-lines)
  (define-key ibuffer-mode-map (kbd "C-k") 'ibuffer-backward-line)
  (define-key ibuffer-mode-map (kbd "C-p") 'ibuffer-kill-line)
  (define-key ibuffer-mode-map (kbd "*.") 'ibuffer-mark-by-file-extention)
  (define-key ibuffer-mode-map "T" 'ibuffer-do-convert-coding-system)
  (define-key ibuffer-mode-map (kbd "o")
    'ibuffer-visit-buffer-other-window-noselect)
  (define-key ibuffer-mode-map (kbd "U")
    (lambda ()
      (interactive)
      (ibuffer-unmark-all (ibuffer-count-marked-lines t))))
  (define-key ibuffer-mode-map (kbd "C-o")
    'ibuffer-visit-buffer-other-window)
  (define-key ibuffer-mode-map "c" 'ibuffer-category))
(global-set-key (kbd "C-x C-b") 'ibuffer)
(defun ibuffer-mark-by-file-extention ()
  (interactive)
  (let ((c (read-string "file extention: ")))
    (ibuffer-mark-by-file-name-regexp (format "\.%s$" c))))

;; ibufferで文字コード変換
(defun ibuffer-do-convert-coding-system
  (coding-system &optional arg)
  "Convert file (s) in specified coding system."
  (interactive
   (list
    (let ((default (or
                    (and (boundp 'dired-default-file-coding-system)
                         dired-default-file-coding-system)
                    buffer-file-coding-system)))
      (read-coding-system
       (format
        "Coding system for converting file (s) (default, %s): "
        default)
       default))
    current-prefix-arg))
  (if (= (ibuffer-count-marked-lines) 0)
      (message
       "No buffers marked; use 'm' to mark a buffer")
    (let ((ibuffer-do-bufs nil))
      (ibuffer-map-marked-lines
       (lambda (buf mark)
         (push buf ibuffer-do-bufs)))
      (while ibuffer-do-bufs
        (set-buffer (car ibuffer-do-bufs))
        (set-buffer-file-coding-system coding-system arg)
        (if (buffer-file-name)
            (let ((coding-system-for-write coding-system))
              (save-buffer arg)))
        (setq ibuffer-do-bufs (cdr ibuffer-do-bufs))))))


(define-key isearch-mode-map (kbd "C-M-o") 'isearch-occur)

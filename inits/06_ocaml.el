(add-to-list 'exec-path "~/.opam/system/bin")
(my-eval-after-load "tuareg"
  (my-define-keys tuareg-interactive-mode-map
    ";" 'self-insert-command))
(my-add-hook tuareg-mode-hook
  (electric-indent-local-mode -1)
  (my-define-keys tuareg-mode-map
    "C-M-h" nil
    "C-c m" 'tuareg-mark-phrase))

(when (executable-find "opam")
  (dolist (var (car (read-from-string
                     (shell-command-to-string "opam config env --sexp"))))
    (setenv (car var) (cadr var)))
  ;; Update the emacs path
  (setq exec-path (split-string (getenv "PATH") path-separator))

  ;; Update the emacs load path
  (push (concat (getenv "OCAML_TOPLEVEL_PATH") "/../../share/emacs/site-lisp")
        load-path)

  ;; Automatically load utop.el
  (autoload 'utop "utop" "Toplevel for OCaml" t)

  (my-eval-after-load "utop"
    (my-define-keys utop-mode-map
      ";" 'self-insert-command))

  ;; ocaml flycheck
  (when (executable-find "ocamlmerlin")
    (with-eval-after-load 'merlin
     ;; Start merlin on ocaml files
     (add-hook 'tuareg-mode-hook 'merlin-mode t)
     (add-hook 'caml-mode-hook 'merlin-mode t)
     ;; Enable auto-complete
     (setq merlin-use-auto-complete-mode t)
     ;; Use opam switch to lookup ocamlmerlin binary
     (setq merlin-command 'opam)

     ;; Disable Merlin's own error checking
      (setq merlin-error-after-save nil)

      ;; Enable Flycheck checker
      (flycheck-ocaml-setup))

    (add-hook 'tuareg-mode-hook #'merlin-mode)
    (add-hook 'tuareg-mode-hook #'flycheck-mode)))

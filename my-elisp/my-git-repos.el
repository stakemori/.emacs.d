(require 'my-utilities)
(eval-when-compile (require 'cl))
(defvar my-git-repo-alist-file nil)
(setq my-git-repo-alist-file "~/Dropbox/.emacs.d/my-git-repos-alist.el")
(defvar my-git-repos nil)
(defvar my-git-repos-alist nil)

(defvar anything-c-source-my-git-repos nil)
(setq anything-c-source-my-git-repos
      '((name . "Git separated repositories working directory")
        (type . my-git-repo)
        (candidates . (lambda ()
                        (setq my-git-repos
                              (cl-loop for x in my-git-repos-alist
                                    collect (car x)))))
        (init . (lambda ()
                  (with-temp-buffer
                    (insert-file-contents my-git-repo-alist-file)
                    (setq my-git-repos-alist (read (current-buffer))))
                  (my-git-repo-confirm-installation)))))

(defvar my-git-repo-installed-p nil)
(defun my-git-repo-confirm-installation ()
  (if (or my-git-repo-installed-p
          (if (string-match (rx "git version " num ".")
                            (shell-command-to-string "git --version"))
              (setq my-git-repo-installed-p t)))
      t
    (error "command git not found.")))

(defun my-git-repo-reset-hard (dir)
  (when (yes-or-no-p "Discard all uncommitted changes?")
    (my-git-repo-reset-hard-noconfirm dir)))

(defun my-git-repo-init (dir)
  (when (yes-or-no-p "Create working tree from separated repository?")
    (my-git-repo-init-noconfirm dir)))

(defun my-git-repo-reset-hard-noconfirm (dir)
  (let ((default-directory default-directory)
         status)
    (setq dir (expand-file-name dir))
    (unless (string-match "/" dir (1- (length dir)))
      (setq dir (concat dir "/")))
    (cd dir)
    (setq status (shell-command-to-string "git status"))
    (unless (string-match "nothing to commit (working directory clean)"
                          status)
      (shell-command-to-string "git reset --hard HEAD")
      (cl-loop for buf in (buffer-list)
            if (aand (buffer-file-name buf)
                     (equal (file-name-directory it) dir))
            do (with-current-buffer buf
                 (revert-buffer nil t))))))

(defun my-git-repo-init-noconfirm (dir)
  (let ((sep-git-repo (expand-file-name
                       (assoc-default dir my-git-repos-alist)))
        (default-directory default-directory)
        ans)
    (unless (and (file-exists-p dir) (setq ans t))
      (when (setq ans (yes-or-no-p
                       (format "Directory %s does not exist. Create it ?"
                               dir)))
        (make-directory dir t)))
    (cd dir)
    (when ans
      (setq dir (expand-file-name dir))
      (my-git-repo-write-git-file dir sep-git-repo))))

(defun my-git-repo-write-git-file (dir sep-git-repo)
  (let ((default-directory default-directory))
    (cd dir)
    (write-region (format "gitdir: %s"
                          (expand-file-name sep-git-repo))
                  nil ".git" nil 'silent)
    (write-region (format "[core]\n        worktree = %s\n"
                          (expand-file-name dir)) nil
                  ".gitconfig" nil 'silent)))

(defun my-git-repo-write-alist ()
  (write-region (format "%S" my-git-repos-alist)
                nil my-git-repo-alist-file nil 'silent))

(defun my-git-repo-delete-separated-repo (dir)
  (let* ((sep-repo (assoc-default dir my-git-repos-alist))
        (prompt (format "the separated repository %s?" sep-repo)))
    (when (if (fboundp 'yes-or-no-p-orig)
              (and (yes-or-no-p-orig (concat "Delete " prompt))
                   (yes-or-no-p-orig (concat "Really delete " prompt)))
            (and (yes-or-no-p (concat "Delete " prompt))
                 (yes-or-no-p (concat "Really delete " prompt))))
      (delete-directory sep-repo t)
      (ansetq my-git-repos-alist (delete (assoc dir it) it))
      (my-git-repo-write-alist))))

(defun my-git-repo-mv-separated-repo (dir)
  (let* ((sep-repo (assoc-default dir my-git-repos-alist))
         (sep-repo-mv (read-file-name "move to: "sep-repo nil
                                      nil nil sep-repo)))
    (when (yes-or-no-p (format "Rename to %s" sep-repo-mv))
      (shell-command-to-string (format "mv %s %s" sep-repo sep-repo-mv))
      (my-git-repo-write-git-file dir sep-repo-mv)
      (ansetq sep-repo-mv (concat "~/" (file-relative-name it "~/")))
      (ansetq my-git-repos-alist
              (cons (cons dir sep-repo-mv)
                    (delete (assoc dir it) it)))
      (my-git-repo-write-alist))))

(defun my-git-repo-remove-from-alist (dir)
  (when (yes-or-no-p "Remove this directory from this list? ")
    (ansetq my-git-repos-alist (delete (assoc dir it) it))
    (my-git-repo-write-alist)))

(my-eval-after-load "anything-config"
  (defun anything-bookmarks ()
    "Preconfigured `anything' for bookmarks."
    (interactive)
    (anything-other-buffer '(anything-c-source-bookmarks
                             anything-c-source-my-git-repos)
                           "*anything bookmarks*"))

  (define-anything-type-attribute 'my-git-repo
    '((action
       ("Magit status" . my-git-repo-magit-status)
       ("Reset working tree" . my-git-repo-reset-hard)
       ("Create working tree from separated repository" . my-git-repo-init)
       ("Remove from this list" . my-git-repo-remove-from-alist)
       ("Reset working tree for each marked directory" .
        my-git-repo-reset-hard-marked-cans)
       ("Create working tree from separated repository for each marked directory" .
        my-git-repo-init-marked-cans)
       ("Move separated repository" . my-git-repo-mv-separated-repo)
       ("Delete separated repository" . my-git-repo-delete-separated-repo))))

  (defun my-git-repo-magit-status (dir)
    (unless (file-exists-p dir)
      (my-git-repo-init-noconfirm dir))
    (when (file-exists-p dir)
      (dired dir) (magit-status dir)))

  (defun my-git-repo-reset-hard-marked-cans (dir)
    (when (yes-or-no-p
           "Discard all uncommitted changes for marked directories?")
      (mapc 'my-git-repo-reset-hard (anything-marked-candidates))))

  (defun my-git-repo-init-marked-cans (dir)
    (when (yes-or-no-p
           (concat "Create working tree from separated repository"
                   " for marked directories?"))
      (mapc 'my-git-repo-init (anything-marked-candidates)))))


;;; git init --separate-git-dir=
(defun my-git-repo-init-create-separate-git-dir (sep-dir)
  "Create separated git repo by git init --separate-git-dir=. I assuem sep-dir in HOME directory."
  (interactive
   (list (read-file-name "repo directory: " "~/Dropbox/git-repos/"
                         "~/Dropbox/git-repos/")))
  (when (and (yes-or-no-p (concat (format "Create a git repository in %s"
                                          default-directory)
                                  (format " with separated directory %s"
                                          sep-dir)))
             (my-git-repo-confirm-installation))
    (when (file-exists-p sep-dir)
      (error (format "Directory %s already exists."
                     sep-dir)))
    (make-directory sep-dir t)
    (ansetq sep-dir (expand-file-name it))
    (shell-command-to-string (format "git init --separate-git-dir=%s" sep-dir))
    (my-git-repo-write-git-file default-directory sep-dir)
    (add-to-list 'my-git-repos-alist
                 (cons default-directory
                       (concat "~/" (file-relative-name sep-dir "~/"))))
    (my-git-repo-write-alist)))

(provide 'my-git-repos)

;; -*- lexical-binding: t -*-
;; recentf-ext
(my/lazy-progn
 (recentf-mode 1)
 (require 'recentf-ext)
 (setq recentf-max-saved-items nil)
 (setq recentf-exclude '("/TAGS$" "/var/tmp/")))

(defun copy-sexp ()
  (interactive)
  (kill-new (thing-at-point 'sexp)))
(defun kill-thing* ()
  (interactive)
  (apply 'kill-region (bounds-of-thing-at-point 'sexp)))

;; latexmkrc 色付け
(add-to-list 'auto-mode-alist '("\\.latexmkrc" . shell-script-mode))

;; open junk file
(autoload 'open-junk-file "open-junk-file" nil t)
(setq open-junk-file-format "~/Dropbox/scripts/%Y/%m/%d-%H%M%S.")

;; desktopみたいなもの
(autoload 'my-clear-file-buffers "my-file-buffers")
(my-define-keys global-map
  "C-c d" 'my-clear-file-buffers)

;; buffer名がかっぶたときにbuffer名を分かりやすくする．
(setq uniquify-buffer-name-style 'post-forward-angle-brackets)
(setq uniquify-ignore-buffers-re "*[^*]+*")

;; minor mode confilct
(autoload 'show-minor-mode-map-priority "minor-mode-hack" nil t)

;; redo+
(autoload 'redo "redo+" nil t)
(global-set-key (kbd "C-?") 'redo)

;; c-c c-eでdefvarをeval
(my/lazy-progn
 (defun my-eval-defvar ()
   (interactive)
   (require 'pp)
   (let* ((sexp (pp-last-sexp))
          (sexp1 (if (equal (length sexp) 3)
                     sexp
                   (butlast sexp))))
     (when (eq (car sexp1) 'defvar)
       (setq sexp1 (cons 'setq (cdr sexp1)))
       (eval sexp1)))))
(global-set-key (kbd "C-c C-e") 'my-eval-defvar)

(smartrep-define-key global-map "M-g"
  '(("i" . 'hi-lock-face-symbol-at-point)
    ("s" . 'my:replace-symbol-at-point)
    ("r" . (lambda ()
             (interactive)
             (hi-lock-unface-buffer t)))
    ("l" . 'hi-lock-line-face-buffer)))

;; ag
(setq ag-highlight-search t)
(setq ag-group-matches nil)               ; workaround for wgrep
(with-eval-after-load "ag"
  (define-key ag-mode-map (kbd "r") 'wgrep-change-to-wgrep-mode)
  ;; ignore tag file
  (add-to-list 'ag-arguments "--ignore=TAGS"))
;; (global-set-key (kbd "<f7>r") (my:command-prefix-func
;;                                'ag-regexp
;;                                'ag-project-regexp))
(defun ripgrep-regexp-no-ignore (regexp directory)
  (interactive
   (list (read-from-minibuffer "Ripgrep search (no-ignore-vsc) for: " (thing-at-point 'symbol))
         (read-directory-name "Directory: ")))
  (ripgrep-regexp regexp directory '("--no-ignore-vcs")))
(global-set-key (kbd "<f7>g") #'ripgrep-regexp)
(global-set-key (kbd "<f7>r") #'ripgrep-regexp-no-ignore)

(with-eval-after-load 'ripgrep
  (define-key ripgrep-search-mode-map (kbd "C-i") 'compilation-next-error))
(global-set-key (kbd "<f7>f") 'helm-locate)
;; (global-set-key (kbd "<f7>d") (my:command-prefix-func
;;                                'ag-dired
;;                                'ag-dired-regexp))


;; markdown-mode
(add-to-list 'auto-mode-alist '("\\.md$" . markdown-mode))
(my-add-hook markdown-mode-hook
  (flyspell-mode 1)
  (toggle-truncate-lines -1)
  (toggle-word-wrap))
(with-eval-after-load "flyspell"
  (define-key flyspell-mode-map (kbd "C-;") nil))

;; imenu ながさの制限をなくす
(setq imenu-max-item-length nil)
(setq imenu-auto-rescan t)

;; align ,
(add-to-list 'align-rules-list
             '(camma-assignment
               (regexp . ",\\( *\\)")
               (repeat . t)              ; 複数回適用を有効に
               (modes  . '(perl-mode python-mode sage-mode))))

;; google translate
(autoload 'google-translate-translate "google-translate")
(global-set-key (kbd "C-x C-t") 'google-translate-at-point-dwim)
(defun google-translate-at-point-dwim ()
  (interactive)
  (let* ((text
         (cond ((use-region-p)
                (buffer-substring-no-properties (region-beginning)
                                                (region-end)))
               (t (or (current-word t)
                      (error "No word at point.")))))
        (lan-ls (cond ((string-match (rx (category japanese)) text)
                       '("ja" "en"))
                      (t '("en" "ja")))))
    (apply 'google-translate-translate (append lan-ls (list text)))))

(defun google-translate-at-point-dwim-german (arg)
  (interactive "p")
  (let* ((text
          (cond ((use-region-p)
                 (buffer-substring-no-properties (region-beginning)
                                                 (region-end)))
                (t (buffer-string))))
         (lan-ls (cond ((= arg 1)
                        '("de" "en"))
                       (t '("en" "de")))))
    (apply 'google-translate-translate (append lan-ls (list text)))))
(global-set-key (kbd "<f10>") 'google-translate-at-point-dwim-german)

;; aspell personal dictionary
(setq ispell-personal-dictionary
      (expand-file-name "~/Dropbox/personal-dictionary-files/.aspell.en.pws")
      my-ispell-personal-dictionary
      ispell-personal-dictionary)

(with-eval-after-load 'ispell
  (defun my-after-ispell-change-dictionary (dict &rest r)
    (cond ((string-match-p (rx bol "en") dict)
           (setq ispell-personal-dictionary my-ispell-personal-dictionary
                 ispell-local-pdict my-ispell-personal-dictionary))
          (t (setq ispell-personal-dictionary nil
                   ispell-local-pdict nil))))
  (advice-add 'ispell-change-dictionary :after
              'my-after-ispell-change-dictionary))

;; diff hl
(my/lazy-progn
 ;; (add-hook 'dired-mode-hook #'diff-hl-dired-mode)
 ;; (global-diff-hl-mode 1)
 ;; (progn
 ;;   (face-spec-set 'diff-hl-insert
 ;;                  '((default :inherit diff-added)
 ;;                    (((class color) (min-colors 88) (background dark))
 ;;                     :background "#335533":foreground "green4")
 ;;                    (((class color) (min-colors 88) (background light))
 ;;                     :background "#ddffdd" :foreground "green4")))
 ;;   (face-spec-set 'diff-hl-delete
 ;;                  '((default :inherit diff-removed)
 ;;                    (((class color) (min-colors 88) (background dark))
 ;;                     :background "#553333" :foreground "red3")
 ;;                    (((class color) (min-colors 88) (background light))
 ;;                     :background "#ffdddd" :foreground "red3")))
 ;;   (face-spec-set 'diff-hl-change
 ;;                  '((default :foreground "DeepSkyBlue")
 ;;                    (((class color) (min-colors 88) (background light))
 ;;                     :background "#ddddff")
 ;;                    (((class color) (min-colors 88) (background dark))
 ;;                     :background "#333355"))))
 ;; (let ((l '(("n" . 'diff-hl-next-hunk)
 ;;            ("p" . 'diff-hl-previous-hunk)
 ;;            ("r" . 'diff-hl-revert-hunk))))
 ;;   (smartrep-define-key global-map (kbd "C-q")
 ;;     l)
 ;;   (with-eval-after-load 'paredit
 ;;     (smartrep-define-key paredit-mode-map (kbd "C-q")
 ;;       l)))

 ;; (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh)
 ;; (diff-hl-flydiff-mode 1)
 )
(cond ((window-system)
       (require 'git-gutter-fringe)
       (set-fringe-style '(8 . 0)))
      (t (require 'git-gutter)))
(global-git-gutter-mode)

;; visual basic mode
(autoload 'visual-basic-mode "visual-basic-mode" "Visual Basic mode." t)
(setq auto-mode-alist (append '(("\\.\\(frm\\|bas\\)$" .
                                 visual-basic-mode)) auto-mode-alist))
(my-eval-after-load "visual-basic-mode"
  (define-key visual-basic-mode-map (kbd "C-M-j") 'forward-paragraph))

;; magma mode
(autoload 'magma-mode "magma-mode" nil t)
(add-to-list 'auto-mode-alist
             `(,(rx "." (or "magma" "mgm") eol) . magma-mode))
(my-eval-after-load "sage-shell"
  (require 'magma-mode)
  (defun sage:-magma-free-send-base (code obj-type)
    (sage-edit:exec-command-base
     :command (format "magma_free('%s')"
                      (replace-regexp-in-string "\n" "\\\\n" code))
     :display-function 'display-buffer
     :pre-message
     (format "Sending the %s to Magma Calculator..." obj-type)
     :post-message
     (format "Sending the %s to Magma Calculator... Done." obj-type)))

  (defun sage:magma-free-send-region (beg end)
    "This command evaluates Magma code in the region and display
the result to the process buffer by the function magma_free. It
requires the internet connection."
    (interactive "r")
    (sage:-magma-free-send-base (buffer-substring beg end) "region"))

  (defun sage:magma-free-send-buffer ()
    "This command evaluates Magma code in the current buffer and
display the result to the process buffer by the function
magma_free. It requires the internet connection."
    (interactive)
    (sage:-magma-free-send-base (buffer-string) "current buffer"))

  (my-define-keys magma-mode-map
    "C-c C-c" 'sage:magma-free-send-buffer))

;; coding system
(cl-case system-type
  (gnu/linux (prefer-coding-system 'utf-8)))

;; my imenu tree
(autoload 'imtr:imenu-tree "my-imenu-tree" nil t)

(my-eval-after-load "my-imenu-tree"
  (defadvice imtr:imenu-alist (around py-imenu activate)
    (let ((py-imenu-show-methods-at-toplevel nil))
      ad-do-it)))

;; smartrep mode lineの色を変えない
(setq smartrep-mode-line-active-bg (face-background 'mode-line))
(setq smartrep-mode-line-string-activated nil)

;; fold this

(my/lazy-progn
 (make-face 'fold-this-face)
 (face-spec-set 'fold-this-face
                '((((class color) (background dark))
                   (:background "dim gray" :foreground "azure2"))))

 (defun fold-this (beg end)
   (interactive "r")
   (let ((o (make-overlay beg end nil t nil)))
     (overlay-put o 'type 'fold-this)
     (overlay-put o 'invisible t)
     (overlay-put o 'keymap fold-this-keymap)
     (overlay-put o 'modification-hooks '(fold-this--unfold-overlay))
     (overlay-put o 'display "....")
     (overlay-put o 'evaporate t)
     (overlay-put o 'face 'fold-this-face))
   (deactivate-mark))

 (defun my-fold-this (beg end)
   (interactive "r")
   (let ((a 10))
     (if (< (+ beg a) end)
         (fold-this (+ beg a) end)
       (fold-this beg end))))
 ;; (global-set-key (kbd "C-q C-f") 'my-fold-this)
 )

;; helm-ag
(setq helm-ag-insert-at-point 'symbol)

;; fill column indicater
(setq fci-rule-width 1)
(setq fci-rule-color "grey30")
(setq-default fci-rule-column 80)

;; modeline face nil
(setq smartrep-mode-line-active-bg (face-background 'mode-line))

;; anchored-transpose
(autoload 'anchored-transpose "anchored-transpose" nil t)
(global-set-key (kbd "C-x t") 'anchored-transpose)

;; visual regexp
(my-define-keys global-map
  "C-z r"   'vr/mc-mark)

;;; projectile
(add-hook 'prog-mode-hook 'projectile-mode)
(add-hook 'text-mode-hook 'projectile-mode)
(global-set-key (kbd "C-c p p") #'helm-projectile-switch-project)
(global-set-key (kbd "C-c p f") #'helm-projectile-find-file)

(my/lazy-progn
 (with-eval-after-load "projectile"
   (setq projectile-globally-ignored-file-suffixes
         (append '(".sobj" ".p")
                 projectile-globally-ignored-file-suffixes))
   (helm-projectile-on)
   (setq projectile-completion-system 'helm)
   (add-elements 'projectile-globally-ignored-modes
     "magit-status-mode" "compilation-mode" "dired-mode" "magit-process-mode"
     "fundamental-mode" "sage-shell:help-mode")))

;; helm yas
(my-define-keys global-map "C-c y" 'helm-yas-complete)


;; openline indent
(my/lazy-progn
 (defun open-line-and-indent ()
   (interactive)
   (let ((newline-and-indent-funct
          (lambda ()
            (cond
             ((derived-mode-p 'python-mode)
              (if (fboundp 'py-newline-and-indent)
                  (py-newline-and-indent)
                (newline-and-indent)))
             (t (newline-and-indent)))))
         (indent-for-tab-cmd
          (lambda ()
            (cond ((eq major-mode 'text-mode))
                  (t (indent-for-tab-command))))))
     (beginning-of-line)
     (funcall newline-and-indent-funct)
     (forward-line -1)
     (funcall indent-for-tab-cmd)))
 (defun my:display-this-buffer-other-window ()
   (interactive)
   (when (one-window-p)
     (split-window-horizontally))
   (let ((buf (current-buffer))
         (win (select-window (next-window))))
     (switch-to-buffer buf)))
 (defun my-ace-window-cmd (arg)
   (interactive "p")
   (require 'ace-window)
   (if (= (length (aw-window-list)) 1)
       (split-window-right)
     (ace-window arg))))

(global-set-key (kbd "M-t") 'ace-swap-window)
(with-eval-after-load 'ace-window
  (defvar my-aw-ignore-side-windows t)
  (defun my-aw-window-filter (win-list)
    (cond (my-aw-ignore-side-windows
           (cl-remove-if
            (lambda (win) (window-parameter win 'no-other-window))
            win-list))
          (t win-list)))
  (advice-add 'aw-window-list :filter-return #'my-aw-window-filter))

(my-define-keys global-map "C-o" 'open-line-and-indent)
(my-define-keys global-map "C-c o" 'my:display-this-buffer-other-window)
(my-define-keys global-map "C-t" #'my-ace-window-cmd)
(setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))

;;; Auto update packages
;; (run-with-timer 10 (* 3600 24) #'my:update-packages)
;; org-toc
(autoload #'toc-org-insert-toc "toc-org" nil t)

;; imenu-anywhere
(global-set-key (kbd "C-.") 'helm-imenu-anywhere)

;; avy
(global-set-key (kbd "C-,") 'avy-goto-char-timer)
(global-set-key (kbd "C-.") 'avy-goto-char)
(setq avy-keys (append (number-sequence ?1 ?9) '(?0)))
;; avy zap-to char
(my-define-keys global-map
  "M-z" 'avy-zap-to-char
  "M-Z" 'avy-zap-up-to-char)
(setq avy-zap-forward-only t)

;;; edit server
(setq edit-server-new-frame nil)
(edit-server-start)
;;; use github flavor markdown for editing gmail.
(setq ham-mode-markdown-to-html-command
      `(,(executable-find "pandoc")
        "--from" "markdown_github"
        "--to" "html" "--email-obfuscation" "none" file))

;; mathematica imenu
(my-named-add-hook wolfram-mode-hook "imenu"
  (add-to-list 'imenu-generic-expression
               (list nil (rx bol (group (1+ nonl)) ":=") 1)))


;; ctags-auto-update
(setq ctags-update-command "ctags-exuberant")

(my/lazy-progn :time 1
 (which-key-mode 1))

;; cursor を含む括弧を強調
(use-package highlight-parentheses
  :defer t
  :init
  (progn
    (setq hl-paren-delay 0.2)
    (setq hl-paren-colors '("Springgreen3" "IndianRed1" "IndianRed3" "IndianRed4"))
    (add-hook 'emacs-lisp-mode-hook #'highlight-parentheses-mode))
  :config
  (set-face-attribute 'hl-paren-face nil :weight 'ultra-bold))


(use-package paradox
  :init
  (setq paradox-github-token "075f5eb7b6d595329033ceba6e6356477fe4a91f")
  (setq paradox-display-download-count t)
  (setq paradox-automatically-star nil)
  (setq paradox-execute-asynchronously t)
  :config
  (my-define-keys paradox-menu-mode-map
    "j" 'paradox-next-entry
    "l" nil
    "k" 'paradox-previous-entry
    "s" nil
    "S" 'paradox-menu-mark-star-unstar))

;; stick-shift like
(setq my/sticky-alist:ja
      '(("1" . "!")
        ("2" . "\"")
        ("3" . "#")
        ("4" . "$")
        ("5" . "%")
        ("6" . "&")
        ("7" . "'")
        ("8" . "(")
        ("9" . ")")
        ("0" . "~")
        ("@" . "`")
        ("[" . "{")
        ("]" . "}")
        ("-" . "=")
        ("^" . "~")
        ("," . "<")
        ("." . ">")
        ("/" . "?")
        (":" . "*")
        (";" . "+")
        ("\\" . "_")))
(defun my/shift-last-char ()
  (interactive)
  (let ((pt (point))
        (m (make-marker)))
    (set-marker m pt)
    (skip-chars-backward (s-join "" (-map #'car my/sticky-alist:ja)))
    (let ((chars (-map #'char-to-string (buffer-substring (point) pt))))
      (delete-region (point) pt)
      (dolist (c chars)
        (execute-kbd-macro (assoc-default c my/sticky-alist:ja))))))
(bind-key* "C-M-u" #'my/shift-last-char)


(use-package golden-ratio
  :disabled
  :defer 0.1
  :init
  (progn
    (golden-ratio-mode 1)
    (remove-hook 'post-command-hook 'golden-ratio--post-command-hook)
    (defun my/golden-ratio-mlt-args (&rest args)
      (golden-ratio)))
  :config
  (progn
    (require 'seq)
    (setq golden-ratio-exclude-modes
          '(ediff-mode eshell-mode dired-mode compilation-mode help-mode gud-mode
                       gdb-locals-mode gdb-inferior-io-mode gdb-frames-mode gdb-breakpoints-mode
                       sage-shell-mode))
    (setq golden-ratio-extra-commands (seq-uniq
                                       (append `(my-ace-window-cmd
                                                 quit-window
                                                 ,@(--map (intern (format "select-window-%d" it))
                                                          (number-sequence 1 10)))
                                               golden-ratio-extra-commands)))
    (setq golden-ratio-inhibit-functions
          '(my/gdb-running-p))
    (defun my/gdb-running-p ()
      (and
       (derived-mode-p 'c-mode)
       (bound-and-true-p gud-comint-buffer)
       (buffer-name gud-comint-buffer)
       (get-buffer-process gud-comint-buffer)))
    (dolist (s golden-ratio-extra-commands)
      (advice-add s :after 'my/golden-ratio-mlt-args))))

(use-package proof-site
  :commands (proofgeneral))

(setq shackle-rules
      '(("*helm" :regexp t :align below :ratio 0.4)
        ("*jedi:doc*" :popup t))
        ;; ;; 上部に表示
        ;; ("foo" :align above)
        ;; ;; 別フレームで表示
        ;; ("bar" :frame t)
        ;; ;; 同じウィンドウで表示
        ;; ("baz" :same t)
        ;; ;; ポップアップで表示
        ;; ("hoge" :popup t)
        ;; ;; 選択する
        ;; ("abc" :select t)
      )

(shackle-mode 1)

(use-package evil-iedit-state
  :bind (("C-:" . evil-iedit-state/iedit-mode))
  :config (define-key evil-iedit-state-map "s" nil))

(use-package crux
  :commands (crux-indent-rigidly-and-copy-to-clipboard))

;; (require 'spaceline-config)
;; (spaceline-toggle-which-function)
;; (spaceline-define-segment
;;     buffer-encoding-abbrev
;;   "The full `buffer-file-coding-system'."
;;   (format-mode-line "%Z"))
;; (spaceline-spacemacs-theme)

(require 'diminish)

(defmacro my-diminish (m name &optional to)
  `(with-eval-after-load ',name
     (diminish ',m ,to)))

(defmacro my-diminish-loop (alist)
  (append '(progn)
          (cl-loop for (m . name) in alist
                   collect
                   `(my-diminish ,m ,name))))

(my-diminish-loop ((racer-mode . racer)
                   (company-mode . company)
                   (projectile-mode . projectile)
                   (which-key-mode . which-key)
                   (rainbow-mode . rainbow-mode)
                   (highlight-parentheses-mode . highlight-parentheses)
                   (paredit-mode . paredit)
                   (yas-minor-mode . yasnippet)
                   (global-whitespace-mode . whitespace)
                   (git-gutter-mode . git-gutter)))

(my-diminish flyspell-mode flyspell "FlySp")

(column-number-mode)
(psession-mode 1)
(push '(shell-command-history . "shell-command-history.el")
      psession-object-to-save-alist)
(push '(compile-history . "compile-history.el")
      psession-object-to-save-alist)

;; dumb-jump
(setq dumb-jump-selector 'helm)
(setq dumb-jump-default-project "~/work")
(setq dumb-jump-force-searcher 'rg)
(dumb-jump-mode)
(defun my-dj-before (&rest _args)
  (push-mark))
(advice-add 'dumb-jump-go :before #'my-dj-before)
(push '(:type "from-import" :supports ("ag" "grep" "rg" "git-grep") :language "python"
              :regex "from [^ \\n]+ import\\s*\\bJJJ\\b" :tests ("from foo import test")
              :not ("from foo import testx"))
      dumb-jump-find-rules)
(push '(:type "import" :supports ("ag" "grep" "rg" "git-grep") :language "python"
              :regex "import\\s*\\bJJJ\\b" :tests ("from foo import test")
              :not ("from foo import testx"))
      dumb-jump-find-rules)

;; (load "/media/sho/hdd2tb/repos/all-the-icons.el/all-the-icons.elc")
;; (all-the-icons-load-cache)
;; (add-hook 'dired-mode-hook 'all-the-icons-dired-mode)

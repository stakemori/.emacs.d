(my-add-hook clojure-mode-hook
  (paredit-mode 1)
  (clj-refactor-mode 1)
  ;; (define-key nrepl-interaction-mode-map (kbd "C-c M-j") 'cider-jack-in)
  ;; (define-key nrepl-interaction-mode-map (kbd "C-c C-q") 'cider-quit)
  )
(my-eval-after-load "clojure-mode"
  (cljr-add-keybindings-with-prefix "C-c m")
  (my-keychord-define clojure-mode-map
    "rk" (lambda () (interactive) (insert "#"))
    "rl" (lambda () (interactive) (insert "%"))
    "rj" (lambda () (interactive)
           (insert "(->> )")
           (forward-char -1))
    "rh" (lambda () (interactive) (insert "(-> )")
           (forward-char -1))))
(setq nrepl-hide-special-buffers t)

(defadvice clojure-load-file (around save-window-excursion activate)
  (save-window-excursion
    ad-do-it))
(setq cljr-suppress-middleware-warnings t)

(with-eval-after-load 'cider-repl
  (define-key cider-repl-mode-map (kbd "C-c M-o") 'cider-repl-clear-buffer))
;; (my-add-hook nrepl-repl-mode-hook
;;   ;; (nrepl-turn-on-eldoc-mode)
;;   (define-key nrepl-repl-mode-map (kbd "C-c C-q") 'cider-quit)
;;   (paredit-mode 1))

;; (my-add-hook cider-repl-mode-hook
;;   (paredit-mode 1))

;; ;; ;; Use clojure.repl in nrepl buffer.
;; (add-hook 'nrepl-mode-hook 'paredit-mode)

;; quickrun jark
(my-eval-after-load "quickrun"
  (push '("clojure/jark" . ((:command . "jark")
                            (:exec "%c -e < %s")
                            (:description . "Run Clojure file with jark")))
        quickrun/language-alist))

;; kibit
(with-eval-after-load "clojure-mode"
  (require 'compile)
  (add-to-list 'compilation-error-regexp-alist-alist
               '(kibit "^At \\([^:]+\\):\\([[:digit:]]+\\):" 1 2 nil 0))
  (add-to-list 'compilation-error-regexp-alist 'kibit)

  ;; A convenient command to run "lein kibit" in the project to which
  ;; the current emacs buffer belongs to.
  (defun kibit ()
    "Run kibit on the current project.
Display the results in a hyperlinked *compilation* buffer."
    (interactive)
    (compile "lein kibit"))

  (defun kibit-current-file ()
    "Run kibit on the current file.
Display the results in a hyperlinked *compilation* buffer."
    (interactive)
    (compile (concat "lein kibit " buffer-file-name))))

(my-named-add-hook inf-clojure-mode-hook
  (company-mode -1)
  (local-set-key (kbd "TAB") 'completion-at-point))

(add-hook 'clojure-mode-hook #'eldoc-mode)
(add-hook 'inf-clojure-mode-hook #'eldoc-mode)
(add-hook 'clojure-mode-hook #'inf-clojure-minor-mode)
